uniform mat4 Projection;
uniform mat4 ModelView;
attribute vec4 vPosition;
attribute vec2 vTexCoord0;
varying vec2 texture0_coord;
void main()
{
	gl_Position = Projection * (ModelView * vPosition);
	texture0_coord = vTexCoord0;
}