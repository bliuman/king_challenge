uniform sampler2D texture0;
varying vec2 tex_coord0;
void main()
{
	gl_FragColor = texture2D(texture0, tex_coord0) * gl_Color;
}