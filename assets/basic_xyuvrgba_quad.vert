uniform mat4 Projection;
uniform mat4 ModelView;
attribute vec4 vPosition;
attribute vec4 vColor;
attribute vec2 vTexCoord0;
varying vec2 tex_coord0;
void main()
{
	gl_Position = Projection * (ModelView * vPosition);
	gl_FrontColor = vColor / 255.0;
	tex_coord0 = vTexCoord0;
}