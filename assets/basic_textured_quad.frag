varying vec4 color;
uniform sampler2D texture0;
varying vec2 texture0_coord;
void main()
{
	gl_FragColor = texture2D(texture0, texture0_coord);
}