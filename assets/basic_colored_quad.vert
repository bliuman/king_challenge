uniform mat4 Projection;
uniform mat4 ModelView;
attribute vec4 vPosition;
attribute vec4 vColor;
varying vec4 color;
void main()
{
	gl_Position = Projection * (ModelView * vPosition);
	vec4 temp = vColor;
	color = gl_Color;
}