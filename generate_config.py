import os
import os.path
import sys

def cmakeGroupName(dir):
	head, tail = os.path.split(dir)
	if not head:
		return dir

	groupName = tail
	while head:
		head, tail = os.path.split(head)
		groupName = tail + "\\\\" + groupName
	return groupName
	
if len(sys.argv) < 4:
	raise Exception("Too few parameters")

rootDir = sys.argv[1]
projectName = sys.argv[2]
platform = sys.argv[3]
validPlatforms = ['Windows', 'MacOSX', 'Linux', 'iOS', 'Android']
if not platform in validPlatforms:
	raise Exception("Unsuported platform")
excludePlatforms = [p for p in validPlatforms if p != platform]

validHeaders = ['.h']
validSources = ['.cpp', '.mm']
groups = {}
headers = []
sources = []
for dirName, subdirs, files in os.walk(rootDir):
	lst = []
	for fname in files:
		if fname.startswith('.'):
			continue
		filePath = dirName + "\\" + fname
		filePath = filePath.replace("\\", "/")
		lst.append(filePath)
		if fname.endswith((".cpp", ".mm")):
			sources.append(filePath)
		elif fname.endswith((".h")):
			headers.append(filePath)
	groups[cmakeGroupName(dirName)] = lst

underscore = '_'
headerFilter = [underscore + prefix + header for header in validHeaders for prefix in excludePlatforms]
headers = [header for header in headers if not header.endswith(tuple(headerFilter))]
sourceFilter = [underscore + prefix + source for source in validSources for prefix in excludePlatforms]
sources = [source for source in sources if not source.endswith(tuple(sourceFilter))]

file = open("Config.cmake", "w")
file.write("set(" + projectName + "_header_files\n")
for header in headers:
	file.write("\t\t" + header + "\n")
file.write(")\n\n")
	
file.write("set(" + projectName + "_source_files\n")
for source in sources:
	file.write("\t\t" + source + "\n")
file.write(")\n\n")
	
for key, value in groups.iteritems():
	file.write("source_group(" + key + " FILES\n")
	values = [val for val in value if not val.endswith(tuple(headerFilter)) and not val.endswith(tuple(sourceFilter))]
	for v in values:
		file.write("\t\t" + v + "\n")
	file.write(")\n\n")
file.close()