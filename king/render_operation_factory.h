//
//  render_operation_factory.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#ifndef RENDER_OPERATION_FACTORY_H
#define RENDER_OPERATION_FACTORY_H

#include <map>
#include <string>
#include <Poco/SharedPtr.h>
#include "king/base_render_operation.h"
#include "king/gpu_program.h"
#include "king/uniform_params_container.h"

namespace king {

class RenderOperationFactory {
  public:
    typedef Poco::SharedPtr<RenderOperationFactory> SharedPtr;
  
  private:
    typedef king::BaseRenderOperation::SharedPtr (RenderOperationFactory::*FactoryFunction)() const;
    typedef std::map<std::string, FactoryFunction> FactoryFunctionMap;
  
  public:
    RenderOperationFactory(const king::UniformParamsContainer::SharedPtr& uniform_params_container);
    ~RenderOperationFactory();
    king::BaseRenderOperation::SharedPtr Create(const std::string& name) const;
  
  private:
    king::GPUProgram::SharedPtr LoadGpuProgram(const std::string& name) const;
    king::BaseRenderOperation::SharedPtr BasicTexturedQuad() const;
    king::BaseRenderOperation::SharedPtr BasicColoredQuad() const;
    king::BaseRenderOperation::SharedPtr QuadXYUVRGBA() const;
    king::BaseRenderOperation::SharedPtr GuiContext() const;
  
    king::UniformParamsContainer::SharedPtr uniform_params_container_;
    FactoryFunctionMap factory_functions_;
  
};

} // namespace king

#endif  // RENDER_OPERATION_FACTORY_H
