//
//  blend_mode_switch.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#ifndef KING_BLEND_MODE_SWITCH_H
#define KING_BLEND_MODE_SWITCH_H

#include "king/render_state_switch.h"
#include "king/opengl.h"

namespace king {

class BlendModeSwitch: public king::RenderStateSwitch {
  public:
    BlendModeSwitch(GLint source, GLint destination);
    virtual ~BlendModeSwitch();
    virtual void TurnOn();
    virtual void TurnOff();
  
  private:
    bool on_;
    GLint blend_source_;
    GLint blend_destination_;
    GLboolean original_on_;
    GLint original_source_;
    GLint original_destination_;
  
};

} // namespace king

#endif  // KING_BLEND_MODE_SWITCH_H
