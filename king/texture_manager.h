//
//  Texture_manager.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#ifndef KING_TEXTURE_MANAGER_H
#define KING_TEXTURE_MANAGER_H

#include <string>
#include <map>
#include <Poco/Path.h>
#include <Poco/SharedPtr.h>
#include "king/texture.h"

namespace king {

class TextureManager {
  public:
    typedef Poco::SharedPtr<TextureManager> SharedPtr;
    typedef std::map<std::string, king::Texture::SharedPtr> TextureMap;
  
  public:
    TextureManager(const Poco::Path& path);
    ~TextureManager();
    king::Texture::SharedPtr GetTexture(const std::string& file_path);
  
  private:
    Poco::Path base_directory_;
    TextureMap textures_;
    
};

} // namespace king

#endif  // KING_TEXTURE_MANAGER_H
