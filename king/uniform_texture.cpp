//
//  uniform_texture.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#include "king/uniform_texture.h"

namespace king {

UniformTexture::UniformTexture(GLuint data)
  :king::UniformParameter<GLuint>(data)
{
}

UniformTexture::~UniformTexture()
{
}

void UniformTexture::Bind(GLuint location) const
{
  glUniform1i(location, Get());
}

} // namespace king
