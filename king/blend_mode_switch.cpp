//
//  blend_mode_switch.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#include "king/blend_mode_switch.h"
#include <assert.h>

namespace king {

BlendModeSwitch::BlendModeSwitch(GLint source, GLint destination)
  :on_(false)
  ,blend_source_(source)
  ,blend_destination_(destination)
  ,original_on_(false)
  ,original_source_(source)
  ,original_destination_(destination)
{
}

BlendModeSwitch::~BlendModeSwitch()
{
  assert(!on_);
}

void BlendModeSwitch::TurnOn()
{
  assert(!on_);
  glGetIntegerv(GL_BLEND_SRC, &original_source_);
  glGetIntegerv(GL_BLEND_DST, &original_destination_);
  glGetBooleanv(GL_BLEND, &original_on_);
  if (!original_on_)
  {
    glEnable(GL_BLEND);
  }
  glBlendFunc(blend_source_, blend_destination_);
  on_ = true;
}

void BlendModeSwitch::TurnOff()
{
  assert(on_);
  glBlendFunc(original_source_, original_destination_);
  if (!original_on_)
  {
    glDisable(GL_BLEND);
  }
  on_ = false;
}

} // namespace king
