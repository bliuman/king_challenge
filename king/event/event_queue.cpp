//
//  event_queue.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 28/10/13.
//
//

#include "king/event/event_queue.h"

namespace king {
namespace event {

EventQueue::EventQueue()
  :events_()
{
  events_.reserve(kDefaultEventQueueSize);
}

EventQueue::~EventQueue()
{
}

} // namespace event
} // namespace king
