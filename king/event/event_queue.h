//
//  event_queue.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 28/10/13.
//
//

#ifndef KING_EVENT_QUEUE_H
#define KING_EVENT_QUEUE_H

#include <vector>
#include <SDL_events.h>

namespace king {
namespace event {

class EventQueue {
  private:
    static const int kDefaultEventQueueSize = 10;
    
  public:
    typedef std::pair<SDL_Event, bool> EventEntry;
    typedef std::vector<EventEntry> Events;
    
  public:
    EventQueue();
    ~EventQueue();
    
    void PushEvent(const SDL_Event& evt) {events_.push_back(EventEntry(evt, false));}
    Events& GetEvents() {return events_;}
    void FlushQueue() {events_.clear();}
    
  private:
    Events events_;
    
};

} // namespace event
} // namespace king

#endif  // KING_EVENT_QUEUE_H
