//
//  uniform_mat4.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#ifndef KING_UNIFORM_MAT4_H
#define KING_UNIFORM_MAT4_H

#include <glm/glm.hpp>
#include "king/opengl.h"
#include "king/uniform_parameter.h"

namespace king {

class UniformMat4: public king::UniformParameter<glm::mat4> {
  public:
    UniformMat4(const glm::mat4& data);
    virtual ~UniformMat4();
    virtual void Bind(GLuint location) const;
};

} // namespace king

#endif  // KING_UNIFORM_MAT4_H
