//
//  vertex_xyuvrgba.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/25/13.
//
//

#include "king/core/frame.h"
#include "king/vertex_XYUVRGBA.h"
#include <assert.h>
#include <glm/glm.hpp>
#include "king/core/game_object.h"
#include "king/gpu_program_param_info.h"
#include "king/render_state.h"

namespace king {

void VertexXYUVRGBA::BindVertexAttributes(const king::GPUProgram::SharedPtr &gpu_program, AttributeList &attribute_list)
{
  const int attrib_count = 3;
  AttribBindInfo attrib_bind_info[attrib_count] = {
    {"vPosition", 2, GL_FLOAT, GL_FALSE, 0},
    {"vTexCoord0", 2, GL_FLOAT, GL_FALSE, (const GLvoid*)(2 * sizeof(GL_FLOAT))},
    {"vColor", 4, GL_UNSIGNED_BYTE, GL_FALSE, (const GLvoid*)(4 * sizeof(GL_FLOAT))}
  };
  
  GLsizei stride = sizeof(VertexXYUVRGBA);
  const king::GPUProgram::Parameters& attrib_parameters = gpu_program->GetAttributes().GetParameters();
  king::GPUProgram::Parameters::const_iterator iter = attrib_parameters.end();
  for (int i = 0; i < attrib_count; ++i)
  {
    const AttribBindInfo& info = attrib_bind_info[i];
    iter = attrib_parameters.find(info.name_);
    assert(attrib_parameters.end() != iter);
    glVertexAttribPointer(iter->second, info.size_, info.type_, info.normalize_, stride, info.offset_);
    glEnableVertexAttribArray(iter->second);
    attribute_list.push_back(iter->second);
  }
}

void VertexXYUVRGBA::UnbindVertexAttributes(const AttributeList &attribute_list)
{
  for (AttributeList::const_iterator iter = attribute_list.begin(); attribute_list.end() != iter; ++iter)
  {
    glDisableVertexAttribArray((*iter));
  }
}

int VertexXYUVRGBA::UpdateData(BufferType &buffer, const king::BaseRenderOperation::RenderList& render_objects)
{
  int vertex_count_ = 0;
  int vertex_per_object = 6;
  if (buffer.size() < render_objects.size() * vertex_per_object)
  {
    buffer.resize(buffer.size() * 2, false);
  }
  
  const glm::vec2* position = NULL;
  const glm::vec2* dimension = NULL;
  const glm::vec2* min_uv = NULL;
  const glm::vec2* max_uv = NULL;
  const glm::ivec4* color = NULL;
  king::core::Frame* frame = NULL;
  king::RenderState* render_state = NULL;
  king::Texture::SharedPtr texture = NULL;
  for (king::BaseRenderOperation::RenderList::const_iterator iter = render_objects.begin(); render_objects.end() != iter; ++iter)
  {
    frame = (*iter)->GetFrame();
    position = &frame->GetPosition();
    dimension = &frame->GetDimension();
    
    render_state = (*iter)->GetRenderState();
    assert(1 == render_state->GetTextures().size());
    texture  = render_state->GetTextures().front();
    min_uv = &texture->GetMinUV();
    max_uv = &texture->GetMaxUV();
    
    color = &render_state->GetColor();
    
    // top left vertex
    buffer[vertex_count_].position[0] = position->x;
    buffer[vertex_count_].position[1] = position->y;
    buffer[vertex_count_].uv[0] = min_uv->x;
    buffer[vertex_count_].uv[1] = min_uv->y;
    buffer[vertex_count_].color[0] = color->r;
    buffer[vertex_count_].color[1] = color->g;
    buffer[vertex_count_].color[2] = color->b;
    buffer[vertex_count_].color[3] = color->a;
    
    // bottom left vertex
    buffer[vertex_count_ + 1].position[0] = position->x;
    buffer[vertex_count_ + 1].position[1] = position->y + dimension->y;
    buffer[vertex_count_ + 1].uv[0] = min_uv->x;
    buffer[vertex_count_ + 1].uv[1] = max_uv->y;
    buffer[vertex_count_ + 1].color[0] = color->r;
    buffer[vertex_count_ + 1].color[1] = color->g;
    buffer[vertex_count_ + 1].color[2] = color->b;
    buffer[vertex_count_ + 1].color[3] = color->a;
    
    // bottom right vertex
    buffer[vertex_count_ + 2].position[0] = position->x + dimension->x;
    buffer[vertex_count_ + 2].position[1] = position->y + dimension->y;
    buffer[vertex_count_ + 2].uv[0] = max_uv->x;
    buffer[vertex_count_ + 2].uv[1] = max_uv->y;
    buffer[vertex_count_ + 2].color[0] = color->r;
    buffer[vertex_count_ + 2].color[1] = color->g;
    buffer[vertex_count_ + 2].color[2] = color->b;
    buffer[vertex_count_ + 2].color[3] = color->a;
    
    // bottom right vertex
    buffer[vertex_count_ + 3].position[0] = position->x + dimension->x;
    buffer[vertex_count_ + 3].position[1] = position->y + dimension->y;
    buffer[vertex_count_ + 3].uv[0] = max_uv->x;
    buffer[vertex_count_ + 3].uv[1] = max_uv->y;
    buffer[vertex_count_ + 3].color[0] = color->r;
    buffer[vertex_count_ + 3].color[1] = color->g;
    buffer[vertex_count_ + 3].color[2] = color->b;
    buffer[vertex_count_ + 3].color[3] = color->a;
    
    // top right vertex
    buffer[vertex_count_ + 4].position[0] = position->x + dimension->x;
    buffer[vertex_count_ + 4].position[1] = position->y;
    buffer[vertex_count_ + 4].uv[0] = max_uv->x;
    buffer[vertex_count_ + 4].uv[1] = min_uv->y;
    buffer[vertex_count_ + 4].color[0] = color->r;
    buffer[vertex_count_ + 4].color[1] = color->g;
    buffer[vertex_count_ + 4].color[2] = color->b;
    buffer[vertex_count_ + 4].color[3] = color->a;
    
    // top left vertex
    buffer[vertex_count_ + 5].position[0] = position->x;
    buffer[vertex_count_ + 5].position[1] = position->y;
    buffer[vertex_count_ + 5].uv[0] = min_uv->x;
    buffer[vertex_count_ + 5].uv[1] = min_uv->y;
    buffer[vertex_count_ + 5].color[0] = color->r;
    buffer[vertex_count_ + 5].color[1] = color->g;
    buffer[vertex_count_ + 5].color[2] = color->b;
    buffer[vertex_count_ + 5].color[3] = color->a;
    
    vertex_count_ += vertex_per_object;
  }
  return vertex_count_;
}

} // namespace king
