//
//  render_queue.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 25/10/13.
//
//

#ifndef KING_RENDER_QUEUE_H
#define KING_RENDER_QUEUE_H

#include <map>
#include <string>
#include <vector>
#include <Poco/SharedPtr.h>
#include "king/render_batch.h"
#include "king/render_batch_factory.h"

namespace king {

class RenderQueue;
class RenderBatchFactory;

} // namespace king

namespace king {

class RenderQueue {
  public:
    typedef Poco::SharedPtr<RenderQueue> SharedPtr;
    typedef std::vector<king::RenderBatch*> RenderBatchList;
    typedef std::map<std::string, king::RenderBatch*> RenderBatchIdMap;
  
  public:
    RenderQueue(const king::RenderBatchFactory::SharedPtr& batch_factory);
    ~RenderQueue();
    void AddGameObject(king::core::GameObject* game_object);
    void RemoveGameObject(king::core::GameObject* game_object);
    void ReinsertGameObject(king::core::GameObject* game_object, const std::string& old_state_hash);
    void Render();
  
  private:
    void RemoveGameObject(king::core::GameObject* game_object, const std::string& state_hash);
    static bool BatchCompareFunc(king::RenderBatch* lhs, king::RenderBatch* rhs);
  
    king::RenderBatchFactory::SharedPtr batch_factory_;
    RenderBatchList batch_list_;
    RenderBatchIdMap batch_id_map_;
    bool sort_;
  
};

} // namespace king

#endif  // KING_RENDER_QUEUE_H
