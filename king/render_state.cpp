//
//  render_state.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#include "king/render_state.h"
#include <assert.h>
#include <set>
#include <Poco/MD5Engine.h>
#include <Poco/Format.h>
#include "king/texture_unit.h"

namespace king {

RenderState::RenderState()
  :render_op_name_("")
  ,depth_(0)
  ,texture_list_()
  ,color_()
  ,hash_("")
{
}

RenderState::RenderState(const std::string& render_op_name, int depth, const TextureList& texture_list, const glm::ivec4& color)
  :render_op_name_(render_op_name)
  ,depth_(depth)
  ,texture_list_(texture_list)
  ,color_(color)
  ,hash_()
{
  CalculateObjectHash();
}

RenderState::~RenderState()
{
}

void RenderState::CalculateObjectHash()
{
  Poco::MD5Engine md5_engine;
  md5_engine.update(render_op_name_);
  md5_engine.update(Poco::format("%i", depth_));
  std::set<int> unique_texture_units;
  for (TextureList::const_iterator iter = texture_list_.begin(); texture_list_.end() != iter; ++iter)
  {
    int texture_unit_id = (*iter)->GetTextureUnit()->GetTextureUnitId();
    if (0 == unique_texture_units.count(texture_unit_id))
    {
      md5_engine.update(Poco::format("%i", texture_unit_id));
      unique_texture_units.insert(texture_unit_id);
    }
  }
  for (int i = 0; i < 4; ++i)
  {
    md5_engine.update(Poco::format("%i", color_[i]));
  }
  hash_ = md5_engine.digestToHex(md5_engine.digest());
}

} // namespace king
