//
//  uniform_mat4.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#include "king/uniform_mat4.h"

namespace king {

UniformMat4::UniformMat4(const glm::mat4& data)
  :king::UniformParameter<glm::mat4>(data)
{
}

UniformMat4::~UniformMat4()
{
}

void UniformMat4::Bind(GLuint location) const
{
  glUniformMatrix4fv(location, 1, GL_FALSE, &Get()[0][0]);
}

} // namespace king
