//
//  uniform_parameter.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 25/10/13.
//
//

#ifndef KING_UNIFORM_PARAMETER_H
#define KING_UNIFORM_PARAMETER_H

#include <Poco/SharedPtr.h>
#include "base_uniform_parameter.h"

namespace king {

template<typename Data>
class UniformParameter: public king::BaseUniformParameter {
  public:
    typedef Poco::SharedPtr<UniformParameter> SharedPtr;
  
  public:
    virtual ~UniformParameter();
    void Set(const Data& data) {data_ = data;}
    const Data& Get() const {return data_;}
    Data& Get() {return data_;}
  
  protected:
    UniformParameter(const Data& data);
  
  private:
    Data data_;
  
};

template<typename Data>
UniformParameter<Data>::UniformParameter(const Data& data)
  :data_(data)
{
}

template<typename Data>
UniformParameter<Data>::~UniformParameter()
{
}

} // namespace king

#endif  // KING_UNIFORM_PARAMETER_H
