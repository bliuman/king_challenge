//
//  game_object.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#ifndef KING_CORE_GAME_OBJECT_H
#define KING_CORE_GAME_OBJECT_H

#include <map>
#include <string>
#include <vector>
#include <Poco/BasicEvent.h>

namespace king {
namespace core {

class Frame;

} // namespace core

class RenderState;

} // namespace king

namespace king {
namespace core {

class GameObject {
  public:
    typedef std::map<std::string, king::RenderState*> RenderStateMap;
    typedef std::vector<GameObject*> GameObjectList;
    typedef Poco::BasicEvent<const std::string> StateChangedEvent;

  public:
    GameObject(king::core::Frame* frame);
    virtual ~GameObject();
    king::core::Frame* GetFrame() const {return frame_;}
    void AddRenderState(const std::string& render_state_name, king::RenderState* render_state);
    king::RenderState* GetRenderState() const {return render_state_;}
    void SwitchRenderState(const std::string& render_state_name);
    StateChangedEvent OnStateChangedEvent;
  
  private:
    king::core::Frame* frame_;
    RenderStateMap render_state_map_;
    king::RenderState* render_state_;
        
};

} // namespace core
} // namespace king

#endif  // KING_CORE_GAME_OBJECT_H
