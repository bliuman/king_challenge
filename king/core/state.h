//
//  state.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/22/13.
//
//

#ifndef KING_CORE_STATE_H
#define KING_CORE_STATE_H

#include "king/animation/animation_system.h"
#include "king/core/factory.h"
#include "king/gui/gui_manager.h"
#include "king/render_queue.h"
#include "king/texture_manager.h"

namespace king {
namespace core {

class StateContainer;

} // namespace core
  
namespace event {

class EventQueue;

} // namespace king
} // namespace king

namespace king {
namespace core {
    
class State {
  public:
    typedef king::core::Factory<State, std::string> Factory;
  
  public:
    enum Status
    {
      kActive = 0,
      kEntering,
      kRunning,
      kPaused,
      kLeaving,
      kInactive,
      kDestroy,
      kIdle
    };
  
  public:
    virtual ~State();
    Status GetStatus() const {return status_;}
    void SetStatus(const Status status);
    virtual void Activate(StateContainer* state_container) = 0;
    virtual void Enter() = 0;
    virtual void Pause() = 0;
    virtual void ProcessEvents(king::event::EventQueue& event_queue) = 0;
    virtual void Update(float delta_frame) = 0;
    virtual void Resume() = 0;
    virtual void Leave() = 0;
    virtual void Deactivate() = 0;
  
  protected:
    State(const king::RenderQueue::SharedPtr& render_queue, const king::TextureManager::SharedPtr& texture_manager, const king::animation::AnimationSystem::SharedPtr& animation_system, const king::gui::GuiManager::SharedPtr& gui_manager);
    StateContainer* GetStateContainer() const;
    king::RenderQueue::SharedPtr GetRenderQueue() {return render_queue_;}
    king::TextureManager::SharedPtr GetTextureManager() {return texture_manager_;}
    king::animation::AnimationSystem::SharedPtr GetAnimationSystem() {return animation_system_;}
    king::gui::GuiManager::SharedPtr GetGuiManager() {return gui_manager_;}
  
  private:
    //
    // prevent copying
    //
    State(const State& other) {}
    State& operator = (const State& other) {return *this;}
  
    StateContainer* state_container_;
    Status status_;
    king::RenderQueue::SharedPtr render_queue_;
    king::TextureManager::SharedPtr texture_manager_;
    king::animation::AnimationSystem::SharedPtr animation_system_;
    king::gui::GuiManager::SharedPtr gui_manager_;
  
};
  
} // namespace core
} // namespace king

#endif  // KING_CORE_STATE_H
