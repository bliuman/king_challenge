//
//  factory.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/01/13.
//
//

#ifndef KING_CORE_FACTORY_H
#define KING_CORE_FACTORY_H

#include <Poco/SharedPtr.h>

namespace king {
namespace core {

template<typename Result, typename Parameter>
class Factory {
  public:
    typedef Result ResultType;
    typedef Parameter ParameterType;
    typedef Poco::SharedPtr<Factory<Result, Parameter> > SharedPtr;
    
  public:
    Factory() {}
    virtual ~Factory() {}
    virtual Result* Create(const Parameter& parameter) = 0;
    virtual void Destroy(Result* object) = 0;
};

} // namespace core
} // namespace king

#endif  // KING_CORE_FACTORY_H