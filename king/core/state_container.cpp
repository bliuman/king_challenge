//
//  state_container.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/22/13.
//
//

#include "king/core/state_container.h"
#include <assert.h>
#include <algorithm>
#include "king/core/state.h"
#include "king/game/state_factory.h"

namespace king {
namespace core {
    
StateContainer::StateContainer(king::game::StateFactory* factory)
  :state_factory_(factory)
  ,states_()
  ,next_state_("", NULL)
  ,variable_container_(new king::core::VariableContainer())
{
  assert(NULL != state_factory_);
  states_.reserve(kDefaultStateListSize);
}

StateContainer::~StateContainer()
{
  for (TStateList::iterator iter = states_.begin(); states_.end() != iter; ++iter)
  {
    state_factory_->Destroy(iter->second);
  }
  states_.clear();
  state_factory_->Destroy(next_state_.second);
  delete state_factory_;
}

void StateContainer::PushState(const std::string &name)
{
  assert(NULL == next_state_.second);
  king::core::State* state = state_factory_->Create(name);
  assert(NULL != state);
  next_state_ = TStateListEntry(name, state);
  assert(states_.end() == std::find(states_.begin(), states_.end(), next_state_));
}

void StateContainer::PopState(const std::string &name)
{
  TStateList::reverse_iterator position = states_.rbegin();
  while (states_.rend() != position && name != position->first)
  {
    ++position;
  }

  if (states_.rend() != position)
  {
    std::for_each((position + 1).base(), states_.end(), StateContainer::PopStateFunc);
  }
}

void StateContainer::ProcessEvents(king::event::EventQueue &event_queue)
{
  for (TStateList::const_reverse_iterator iter = states_.rbegin(); states_.rend() != iter; ++iter)
  {
    if (king::core::State::kRunning == iter->second->GetStatus())
    {
        iter->second->ProcessEvents(event_queue);
    }
  }
}

void StateContainer::Update(float delta_frame)
{
  if (NULL != next_state_.second)
  {
    if (!states_.empty())
    {
      states_.back().second->Pause();
    }
    states_.push_back(next_state_);
    next_state_.second = NULL;
  }
  
  if (!states_.empty())
  {
    PerformStateAction(states_.back());
    for (TStateList::const_reverse_iterator iter = states_.rbegin(); states_.rend() != iter; ++iter)
    {
      UpdateState((*iter), delta_frame);
    }
  }
}

void StateContainer::PopStateFunc(const TStateListEntry &entry)
{
  assert(NULL != entry.second);
  entry.second->SetStatus(king::core::State::kLeaving);
}

void StateContainer::PerformStateAction(const TStateListEntry &entry)
{
  king::core::State* state = entry.second;
  switch (state->GetStatus())
  {
    case king::core::State::kEntering:
    {
      state->Enter();
    } break;
    
    case king::core::State::kLeaving:
    {
      state->Leave();
    } break;
    
    case king::core::State::kRunning:
    case king::core::State::kIdle:
    case king::core::State::kPaused:
    {
    } break;
    
    case king::core::State::kInactive:
    {
      state->Deactivate();
    } break;
    
    case king::core::State::kDestroy:
    {
      state_factory_->Destroy(state);
      states_.pop_back();
      if (!states_.empty() && king::core::State::kPaused == states_.back().second->GetStatus())
      {
        states_.back().second->Resume();
      }
    } break;
    
    case king::core::State::kActive:
    {
      state->Activate(this);
    } break;
    
    default:
    {
      assert(false);
    } break;
  };
}

void StateContainer::UpdateState(const TStateListEntry &entry, float delta_frame) const
{
  king::core::State* state = entry.second;
  if (king::core::State::kRunning == state->GetStatus())
  {
    state->Update(delta_frame);
  }
}

} // namespace core
} // namespace king
