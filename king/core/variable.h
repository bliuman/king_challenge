//
//  variable.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/4/13.
//
//

#ifndef KING_CORE_VARIABLE_H
#define KING_CORE_VARIABLE_H

#include <string>
#include <Poco/SharedPtr.h>
#include <Poco/BasicEvent.h>

namespace king {
namespace core {

template<typename DataType>
class Variable {
  public:
    typedef Poco::SharedPtr<Variable<DataType> > SharedPtr;
    typedef Poco::BasicEvent<const DataType> VariableChangedEvent;
  
  public:
    Variable(const DataType& data);
    ~Variable();
    void SetData(const DataType& data);
    const DataType& GetData() const;
    DataType& GetData();
    VariableChangedEvent OnVariableChangedEvent;
  
  private:
    DataType data_;
    
};

template<typename DataType>
Variable<DataType>::Variable(const DataType& data)
  :data_(data)
{
}

template<typename DataType>
Variable<DataType>::~Variable()
{
}

template<typename DataType>
void Variable<DataType>::SetData(const DataType& data)
{
  data_ = data;
  OnVariableChangedEvent(this, data_);
}

template<typename DataType>
const DataType& Variable<DataType>::GetData() const
{
  return data_;
}

template<typename DataType>
DataType& Variable<DataType>::GetData()
{
  return data_;
}

} // namespace core
} // namespace king

#endif  // KING_CORE_VARIABLE_H
