//
//  frame.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#include "king/core/frame.h"
#include <assert.h>
#include <Poco/BasicEvent.h>

namespace king {
namespace core {

Frame::Frame()
  :position_(.0f, .0f)
  ,dimension_(.0f, .0f)
  ,game_object_(NULL)
{
}

Frame::Frame(const glm::vec2& position, const glm::vec2& dimension)
  :position_(position)
  ,dimension_(dimension)
{
}

Frame::Frame(const Frame& frame)
  :position_(frame.position_)
  ,dimension_(frame.dimension_)
{
}

Frame::~Frame()
{
}

void Frame::SetGameObject(king::core::GameObject *game_object)
{
  assert(NULL != game_object);
  game_object_ = game_object;
}

void Frame::SetPosition(const glm::vec2 &position)
{
  position_ = position;
  OnFrameChangedEvent(this, Poco::EventArgs());
}

void Frame::SetDimension(const glm::vec2 &dimension)
{
  dimension_ = dimension;
  OnFrameChangedEvent(this, Poco::EventArgs());
}

} // namespace core
} // namespace king
