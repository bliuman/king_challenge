//
//  game_object.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#include "king/core/game_object.h"
#include <assert.h>
#include "king/core/frame.h"
#include "king/render_state.h"

namespace king {
namespace core {

GameObject::GameObject(king::core::Frame* frame)
  :frame_(frame)
  ,render_state_map_()
  ,render_state_(NULL)
{
  assert(NULL != frame_);
  frame_->SetGameObject(this);
}

GameObject::~GameObject()
{
  delete frame_;
  for (RenderStateMap::const_iterator iter = render_state_map_.begin(); render_state_map_.end() != iter; ++iter)
  {
    delete iter->second;
  }
  render_state_map_.clear();
}

void GameObject::AddRenderState(const std::string &render_state_name, king::RenderState *render_state)
{
  assert(!render_state_name.empty());
  std::pair<RenderStateMap::iterator, bool> result = render_state_map_.insert(RenderStateMap::value_type(render_state_name, render_state));
  assert(result.second);
}

void GameObject::SwitchRenderState(const std::string &render_state_name)
{
  RenderStateMap::const_iterator iter = render_state_map_.find(render_state_name);
  assert(render_state_map_.end() != iter);
  std::string hash = (NULL != render_state_ ? render_state_->GetObjectHash() : "");
  render_state_ = iter->second;
  OnStateChangedEvent(this, hash);
}

} // namespace core
} // namespace king
