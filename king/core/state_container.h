//
//  state_container.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/22/13.
//
//

#ifndef KING_CORE_STATE_CONTAINER_H
#define KING_CORE_STATE_CONTAINER_H

#include <string>
#include <vector>
#include "king/core/variable_container.h"

namespace king {
namespace core {

class State;

} // namespace core

namespace game {

class StateFactory;

} // namespace game

namespace event {

class EventQueue;

} // namespace event
} // namespace king

namespace king {
namespace core {
    
class StateContainer {
  private:
    typedef std::pair<std::string, king::core::State*> TStateListEntry;
    typedef std::vector<TStateListEntry> TStateList;
    static const TStateList::size_type kDefaultStateListSize = 10;
  
  public:
    StateContainer(king::game::StateFactory* factory);
    ~StateContainer();
    bool IsEmpty() const {return states_.empty();}
    void PushState(const std::string& name);
    void PopState(const std::string& name);
    void ProcessEvents(king::event::EventQueue& event_queue);
    void Update(float delta_frame);
    king::core::VariableContainer::SharedPtr& GetVariableContainer() {return variable_container_;}
    const king::core::VariableContainer::SharedPtr& GetVariableContainer() const {return variable_container_;}
  
  private:
    void PerformStateAction(const TStateListEntry& entry);
    void UpdateState(const TStateListEntry& entry, float delta_frame) const;
    static void PopStateFunc(const TStateListEntry& entry);
  
    king::game::StateFactory* state_factory_;
    TStateList states_;
    TStateListEntry next_state_;
    king::core::VariableContainer::SharedPtr variable_container_;
    
};
  
} // namespace core
} // namespace king

#endif  // KING_CORE_STATE_CONTAINER_H
