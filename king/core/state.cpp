//
//  state.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/22/13.
//
//

#include "king/core/state.h"

#include <assert.h>

namespace king {
namespace core {
    
State::State(const king::RenderQueue::SharedPtr& render_queue, const king::TextureManager::SharedPtr& texture_manager, const king::animation::AnimationSystem::SharedPtr& animation_system, const king::gui::GuiManager::SharedPtr& gui_manager)
  :state_container_(NULL)
  ,status_(kActive)
  ,render_queue_(render_queue)
  ,texture_manager_(texture_manager)
  ,animation_system_(animation_system)
  ,gui_manager_(gui_manager)
{
    
}

State::~State()
{
    
}

void State::Activate(StateContainer* state_container)
{
  assert(NULL != state_container);
  assert(NULL == state_container_);
  state_container_ = state_container;
  SetStatus(kEntering);
}

void State::Enter()
{
  SetStatus(kRunning);
}

void State::Pause()
{
  SetStatus(kPaused);
}

void State::ProcessEvents(king::event::EventQueue &event_queue)
{
}

void State::Update(float delta_frame)
{
}

void State::Resume()
{
  SetStatus(kRunning);
}

void State::Leave()
{
  SetStatus(kInactive);
}

void State::Deactivate()
{
  SetStatus(kDestroy);
}

void State::SetStatus(const Status status)
{
  status_ = status;
}

StateContainer* State::GetStateContainer() const
{
  assert(NULL != state_container_);
  return state_container_;
}
  
} // namespace core
} // namespace king
