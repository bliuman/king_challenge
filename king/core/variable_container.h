//
//  variable_container.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/4/13.
//
//

#ifndef KING_CORE_VARIABLE_CONTAINER_H
#define KING_CORE_VARIABLE_CONTAINER_H

#include <map>
#include <string>
#include <assert.h>
#include <Poco/Any.h>
#include "king/core/variable.h"

namespace king {
namespace core {

class VariableContainer {
  public:
    typedef Poco::SharedPtr<VariableContainer> SharedPtr;
  
  private:
    typedef std::map<std::string, Poco::Any> VariablesMap;
  
  public:
    VariableContainer() {}
    ~VariableContainer() {}
  
    template<typename Data>
    void Add(const std::string& name, const typename king::core::Variable<Data>::SharedPtr& variable);
  
    template<typename Data>
    void Remove(const std::string& name);
  
    template<typename Data>
    typename king::core::Variable<Data>::SharedPtr Get(const std::string& name) const;
  
  private:
    VariablesMap variables_;
  
};

template<typename Data>
void VariableContainer::Add(const std::string& name, const typename king::core::Variable<Data>::SharedPtr& variable)
{
  assert(!name.empty());
  std::pair<VariablesMap::iterator, bool> result = variables_.insert(VariablesMap::value_type(name, variable));
  assert(result.second);
}

template<typename Data>
void VariableContainer::Remove(const std::string& name)
{
  VariablesMap::iterator iter = variables_.find(name);
  if (variables_.end() == iter)
  {
    assert(false);
  }
  typename king::core::Variable<Data>::SharedPtr variable = Poco::RefAnyCast<typename king::core::Variable<Data>::SharedPtr>(iter->second);
  variables_.erase(iter);
}

template<typename Data>
typename king::core::Variable<Data>::SharedPtr VariableContainer::Get(const std::string &name) const
{
  VariablesMap::const_iterator iter = variables_.find(name);
  if (variables_.end() == iter)
  {
    assert(false);
  }
  typename king::core::Variable<Data>::SharedPtr variable = Poco::RefAnyCast<typename king::core::Variable<Data>::SharedPtr>(iter->second);
  return variable;
}

} // namespace core
} // namespace king

#endif  // KING_CORE_VARIABLE_CONTAINER_H
