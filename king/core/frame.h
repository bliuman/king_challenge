//
//  frame.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#ifndef KING_CORE_FRAME_H
#define KING_CORE_FRAME_H

#include <glm/glm.hpp>
#include <Poco/BasicEvent.h>
#include <Poco/EventArgs.h>

namespace king {
namespace core {

class GameObject;

} // namespace core
} // namespace king

namespace king {
namespace core {

class Frame {
  public:
    typedef Poco::BasicEvent<const Poco::EventArgs> FrameChangedEvent;
  
  public:
    Frame();
    Frame(const glm::vec2& position, const glm::vec2& dimension);
    Frame(const Frame& frame);
    ~Frame();
    Frame& operator = (const Frame& frame);
    Frame& operator += (const Frame& frame);
    Frame& operator -= (const Frame& frame);
    const Frame operator + (const Frame& frame);
    const Frame operator - (const Frame& frame);
    void SetGameObject(king::core::GameObject* game_object);
    king::core::GameObject* GetGameObject() const {return game_object_;}
    const glm::vec2& GetPosition() const {return position_;}
    const glm::vec2& GetDimension() const {return dimension_;}
    void SetPosition(const glm::vec2& position);
    void SetDimension(const glm::vec2& dimension);
    bool Contains(const glm::vec2& point) const;
    FrameChangedEvent OnFrameChangedEvent;
  
  private:
    glm::vec2 position_;
    glm::vec2 dimension_;
    king::core::GameObject* game_object_;
  
};

inline Frame& Frame::operator = (const Frame& frame)
{
  if (this != &frame)
  {
    position_ = frame.position_;
    dimension_ = frame.dimension_;
  }
  return *this;
}

inline Frame& Frame::operator += (const Frame &frame)
{
  position_ += frame.position_;
  dimension_ += frame.dimension_;
  return *this;
}

inline Frame& Frame::operator -= (const Frame &frame)
{
  position_ -= frame.position_;
  dimension_ -= frame.dimension_;
  return *this;
}

inline const Frame Frame::operator + (const Frame& frame)
{
  Frame copy(*this);
  copy += frame;
  return copy;
}

inline const Frame Frame::operator - (const Frame& frame)
{
  Frame copy(*this);
  copy -= frame;
  return copy;
}

inline bool Frame::Contains(const glm::vec2 &point) const
{
  return ((position_.x <= point.x) && (position_.y <= point.y) && ((position_.x + dimension_.x) > point.x) && ((position_.y + dimension_.y) > point.y));
}

} // namespace core
} // namespace king

#endif  // KING_CORE_FRAME_H
