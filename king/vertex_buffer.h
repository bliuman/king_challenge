//
//  vertex_buffer.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 25/10/13.
//
//

#ifndef VERTEX_BUFFER_H
#define VERTEX_BUFFER_H

#include <assert.h>
#include <Poco/Buffer.h>
#include "king/base_render_operation.h"
#include "king/gpu_program.h"
#include "king/opengl.h"

namespace king {

template<typename VertexDeclaration>
class VertexBuffer {
  private:
    static const int kDefaultBufferSize = 1024;
  
  public:
    typedef typename VertexDeclaration::BufferType DataBuffer;
    typedef typename VertexDeclaration::AttributeList BoundAttributeList;
  
  public:
    VertexBuffer();
    ~VertexBuffer();
    int GetVertexCount() const {return vertex_count_;}
    void BindVertexAttributes(const king::GPUProgram::SharedPtr& gpu_program);
    void UnbindVertexAttributes();
    void UpdateData(const king::core::GameObject::GameObjectList& game_objects);
  
  private:
    GLuint buffer_id_;
    DataBuffer data_buffer_;
    int vertex_count_;
    BoundAttributeList bound_attributes_;
    bool is_bound_;
    
};

template<typename VertexDeclaration>
VertexBuffer<VertexDeclaration>::VertexBuffer()
  :buffer_id_(0)
  ,data_buffer_(kDefaultBufferSize)
  ,vertex_count_(0)
  ,bound_attributes_()
  ,is_bound_(false)
{
  glGenBuffers(1, &buffer_id_);
  assert(buffer_id_ > 0);
}

template<typename VertexDeclaration>
VertexBuffer<VertexDeclaration>::~VertexBuffer()
{
  if (is_bound_)
  {
    UnbindVertexAttributes();
  }
  glDeleteBuffers(1, &buffer_id_);
}

template<typename VertexDeclaration>
void VertexBuffer<VertexDeclaration>::BindVertexAttributes(const king::GPUProgram::SharedPtr& gpu_program)
{
  assert(!is_bound_);
  assert(bound_attributes_.empty());
  glBindBuffer(GL_ARRAY_BUFFER, buffer_id_);
  VertexDeclaration::BindVertexAttributes(gpu_program, bound_attributes_);
  is_bound_ = true;
}

template<typename VertexDeclaration>
void VertexBuffer<VertexDeclaration>::UnbindVertexAttributes()
{
  assert(is_bound_);
  VertexDeclaration::UnbindVertexAttributes(bound_attributes_);
  bound_attributes_.clear();
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  is_bound_ = false;
}

template<typename VertexDeclaration>
void VertexBuffer<VertexDeclaration>::UpdateData(const king::BaseRenderOperation::RenderList& render_objects)
{
  vertex_count_ = VertexDeclaration::UpdateData(data_buffer_, render_objects);
  glBindBuffer(GL_ARRAY_BUFFER, buffer_id_);
  glBufferData(GL_ARRAY_BUFFER, data_buffer_.size() * sizeof(VertexDeclaration), data_buffer_.begin(), GL_DYNAMIC_DRAW);
}

} // namespace king

#endif  // VERTEX_BUFFER_H
