//
//  render_batch_factory.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/26/13.
//
//

#ifndef KING_RENDER_BATCH_FACTORY_H
#define KING_RENDER_BATCH_FACTORY_H

#include <Poco/SharedPtr.h>
#include "king/render_operation_factory.h"

namespace king {

class RenderBatch;
class RenderQueue;
class RenderState;

} // namespace king

namespace king {

class RenderBatchFactory {
  public:
    typedef Poco::SharedPtr<RenderBatchFactory> SharedPtr;
    
  public:
    RenderBatchFactory(const king::RenderOperationFactory::SharedPtr& render_operation_factory);
    ~RenderBatchFactory();
    king::RenderBatch* CreateRenderBatch(king::RenderQueue* queue, king::RenderState* state);
  
  private:
    king::RenderOperationFactory::SharedPtr render_operation_factory_;
  
};

} // namespace king

#endif  // KING_RENDER_BATCH_FACTORY_H
