//
//  render_queue.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 25/10/13.
//
//

#include "king/render_queue.h"
#include <assert.h>
#include <algorithm>
#include "king/core/game_object.h"
#include "king/render_state.h"

namespace king {

RenderQueue::RenderQueue(const king::RenderBatchFactory::SharedPtr& batch_factory)
  :batch_factory_(batch_factory)
  ,batch_list_()
  ,batch_id_map_()
  ,sort_(true)
{
}

RenderQueue::~RenderQueue()
{
}

void RenderQueue::AddGameObject(king::core::GameObject *game_object)
{
  king::RenderState* render_state = game_object->GetRenderState();
  const std::string& hash = render_state->GetObjectHash();
  if (!hash.empty())
  {
    RenderBatchIdMap::iterator batch_id_iter = batch_id_map_.find(hash);
    if (batch_id_map_.end() == batch_id_iter)
    {
      king::RenderBatch* batch = batch_factory_->CreateRenderBatch(this, render_state);
      assert(NULL != batch);
      std::pair<RenderBatchIdMap::iterator, bool> result = batch_id_map_.insert(RenderBatchIdMap::value_type(hash, batch));
      assert(result.second);
      batch_id_iter = result.first;
      batch_list_.push_back(batch);
      sort_ = true;
    }
    assert(batch_id_map_.end() != batch_id_iter);
    batch_id_iter->second->Add(game_object);
  }
}

void RenderQueue::RemoveGameObject(king::core::GameObject *game_object)
{
  assert(game_object);
  king::RenderState* render_state = game_object->GetRenderState();
  const std::string& hash = render_state->GetObjectHash();
  if (!hash.empty())
  {
    RemoveGameObject(game_object, hash);
  }
}

void RenderQueue::ReinsertGameObject(king::core::GameObject *game_object, const std::string &old_state_hash)
{
  if (!old_state_hash.empty())
  {
    RemoveGameObject(game_object, old_state_hash);
  }
  AddGameObject(game_object);
}

void RenderQueue::Render()
{
  if (sort_)
  {
    std::sort(batch_list_.begin(), batch_list_.end(), &RenderQueue::BatchCompareFunc);
    sort_ = false;
  }
  
  for (RenderBatchList::const_iterator iter = batch_list_.begin(); batch_list_.end() != iter; ++iter)
  {
    (*iter)->Render();
  }
}

bool RenderQueue::BatchCompareFunc(king::RenderBatch *lhs, king::RenderBatch *rhs)
{
  return lhs->GetPriority() < rhs->GetPriority();
}

void RenderQueue::RemoveGameObject(king::core::GameObject *game_object, const std::string &state_hash)
{
  RenderBatchIdMap::iterator batch_id_iter = batch_id_map_.find(state_hash);
  assert(batch_id_map_.end() != batch_id_iter);
  batch_id_iter->second->Remove(game_object);
}

} // namespace king
