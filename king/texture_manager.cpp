//
//  Texture_manager.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#include "king/texture_manager.h"
#include <assert.h>
#include "king/texture_unit.h"

namespace king {

TextureManager::TextureManager(const Poco::Path& path)
  :base_directory_(path)
  ,textures_()
{
}

TextureManager::~TextureManager()
{
}

king::Texture::SharedPtr TextureManager::GetTexture(const std::string &file_path)
{
  TextureMap::iterator iter = textures_.find(file_path);
  if (textures_.end() == iter)
  {
    Poco::Path full_path(base_directory_);
    full_path.append(file_path);
    assert(full_path.isFile());
    king::TextureUnit::SharedPtr texture_unit = new king::TextureUnit(full_path.toString());
    std::pair<TextureMap::iterator, bool> result = textures_.insert(TextureMap::value_type(file_path, new king::Texture(texture_unit)));
    assert(result.second);
    iter = result.first;
  }
  return iter->second;
}

} // namespace king
