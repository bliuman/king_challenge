//
//  render_state_switch.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#ifndef KING_RENDER_STATE_SWITCH_H
#define KING_RENDER_STATE_SWITCH_H

#include <Poco/SharedPtr.h>

namespace king {

class RenderStateSwitch {
  public:
    typedef Poco::SharedPtr<RenderStateSwitch> SharedPtr;
    
  public:
    virtual ~RenderStateSwitch() {}
    virtual void TurnOn() = 0;
    virtual void TurnOff() = 0;
  
  protected:
    RenderStateSwitch() {}
};

} // namespace king

#endif  // KING_RENDER_STATE_SWITCH_H
