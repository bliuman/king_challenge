//
//  gpu_program_param_info.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/24/13.
//
//

#ifndef GPU_PROGRAM_PARAM_INFO_H_
#define GPU_PROGRAM_PARAM_INFO_H_

#include <assert.h>
#include <Poco/Buffer.h>
#include "king/opengl.h"

namespace king {

template<typename Collector, typename Parameters>
class GPUProgramParamInfo {
  public:
    GPUProgramParamInfo();
    ~GPUProgramParamInfo();
    void CollectInfo(GLuint gpu_program_id_);
    const Parameters& GetParameters() const {return parameters_;}
  
  private:
    Collector collector_;
    Parameters parameters_;
  
};

template<typename Collector, typename Parameters>
GPUProgramParamInfo<Collector, Parameters>::GPUProgramParamInfo()
  :collector_()
  ,parameters_()
{
}

template<typename Collector, typename Parameters>
GPUProgramParamInfo<Collector, Parameters>::~GPUProgramParamInfo()
{
}

template<typename Collector, typename Parameters>
void GPUProgramParamInfo<Collector, Parameters>::CollectInfo(GLuint gpu_program_id_)
{
  assert(gpu_program_id_ > 0);
  GLenum param = 0;
  GLenum buffer_length = 0;
  GLint count = 0;
  collector_.GetTypes(param, buffer_length);
  glGetProgramiv(gpu_program_id_, param, &count);
  if (count > 0)
  {
    GLsizei buffer_size = 0;
    glGetProgramiv(gpu_program_id_, buffer_length, &buffer_size);
    assert(buffer_size > 0);
    Poco::Buffer<char> name_buffer(buffer_size);
    GLint size = 0;
    GLenum type = 0;
    GLint location = 0;
    for (GLint i = 0; i < count; ++i)
    {
      location = collector_.Collect(gpu_program_id_, i, buffer_size, &size, &type, name_buffer);
      assert(location >= 0);
      parameters_.insert(typename Parameters::value_type(std::string(name_buffer.begin()), location));
    }
  }
}

} // namespace king

#endif  // GPU_PROGRAM_PARAM_INFO_H_
