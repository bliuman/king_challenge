//
//  game_end_state.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 04/11/13.
//
//

#include "king/game/game_end_state.h"
#include <glm/glm.hpp>
#include <Rocket/Core/ElementDocument.h>
#include "king/game/game_end_ui.h"
#include "king/game/gui_animation_factory.h"
#include "king/game/gui_screen_factory.h"


namespace king {
namespace game {

GameEndState::GameEndState(const king::RenderQueue::SharedPtr& render_queue, const king::TextureManager::SharedPtr& texture_manager, const king::animation::AnimationSystem::SharedPtr& animation_system, const king::gui::GuiManager::SharedPtr& gui_manager)
  :king::core::State(render_queue, texture_manager, animation_system, gui_manager)
  ,gui_animation_factory_(NULL)
  ,gui_context_(NULL)
  ,game_end_ui_(NULL)
{
}

GameEndState::~GameEndState()
{
}

void GameEndState::Activate(king::core::StateContainer *state_container)
{
  king::core::State::Activate(state_container);
  gui_animation_factory_ = new king::game::GuiAnimationFactory(GetAnimationSystem());
}

void GameEndState::Enter()
{
  if (GetAnimationSystem()->IsAnimationsRunning())
  {
    return;
  }
  king::core::State::Enter();
  gui_context_ = GetGuiManager()->CreateContext(king::gui::ContextFactoryParam("game_end_menu", glm::ivec2(800, 600), new king::game::GuiScreenFactory()));
  game_end_ui_ = dynamic_cast<king::game::GameEndUi*>(gui_context_->CreateScreen("game_end_menu"));
  assert(NULL != game_end_ui_);
  game_end_ui_->SetStateContainer(GetStateContainer());
  gui_animation_factory_->GameEndDialogAppear(game_end_ui_->GetRocketDocument());
}

void GameEndState::Pause()
{
  king::core::State::Pause();
}

void GameEndState::ProcessEvents(king::event::EventQueue &event_queue)
{
  king::core::State::ProcessEvents(event_queue);
}

void GameEndState::Update(float delta_frame)
{
  king::core::State::Update(delta_frame);
}

void GameEndState::Resume()
{
  king::core::State::Resume();
}

void GameEndState::Leave()
{
  king::core::State::Leave();
  gui_animation_factory_->GameEndDialogDisappear(game_end_ui_->GetRocketDocument());
}

void GameEndState::Deactivate()
{
  if (GetAnimationSystem()->IsAnimationsRunning())
  {
    return;
  }
  gui_context_->DestroyScreen(game_end_ui_);
  GetGuiManager()->DestroyContext(gui_context_);
  delete gui_animation_factory_;
  king::core::State::Deactivate();
}

} // namespace game
} // namespace king