//
//  grid.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#include "king/game/grid.h"
#include <assert.h>
#include <deque>
#include <set>
#include <glm/glm.hpp>
#include "king/core/frame.h"
#include "king/game/animation_factory.h"
#include "king/game/game_object_factory.h"
#include "king/game/gem.h"

namespace king {
namespace game {

Grid::Grid(king::core::Frame* frame, king::game::GameObjectFactory* game_object_factory, king::game::AnimationFactory* animation_factory, const glm::ivec2 size)
  :king::core::GameObject(frame)
  ,game_object_factory_(game_object_factory)
  ,animation_factory_(animation_factory)
  ,gems_list_()
  ,size_(size)
  ,gem_size_(.0f, .0f)
  ,random_num_generator_()
  ,score_(0)
{
  assert(NULL != game_object_factory_);
  assert(NULL != animation_factory_);
  Initialize();
}

Grid::~Grid()
{
  DestroyGems(gems_list_);
}

void Grid::UpdateGridState()
{
  //
  // remove gems that were destroyed and had destroy animation running
  //
  if (!remove_list_.empty())
  {
    DestroyGems(remove_list_);
  }
  
  //
  // check all gems that have changed their position if they make chains
  //
  UpdateChains();
  
  //
  // if all animations are finished and there are no more possible chains in the current grid state
  // fill in empty grid gaps
  //
  if (remove_list_.empty())
  {
    for (int i = 0; i < size_.x; ++i)
    {
      RefillCollumn(i);
    }
  }
}

void Grid::AnimateClearGrid()
{
  for (GemsList::const_iterator iter = gems_list_.begin(); gems_list_.end() != iter; ++iter)
  {
    animation_factory_->GemDestroy((*iter), random_num_generator_.next(20));
  }
}

king::game::Gem* Grid::SelectGem(const glm::vec2 &screen_point) const
{
  king::core::Frame* frame = GetFrame();
  assert(frame->Contains(screen_point));
  const glm::vec2& position = frame->GetPosition();
  int index_x = static_cast<int>(screen_point.x - position.x) / static_cast<int>(gem_size_.x);
  int index_y = static_cast<int>(screen_point.y - position.y) / static_cast<int>(gem_size_.y);
  return gems_list_[index_x * size_.y + index_y];
}

bool Grid::SwitchGems(king::game::Gem *start_gem, king::game::Gem *end_gem)
{
  const glm::ivec2 distance = start_gem->GetGridPosition() - end_gem->GetGridPosition();
  const int abs_x = abs(distance.x);
  const int abs_y = abs(distance.y);
  if ((0 == abs_x && 1 == abs_y) || (1 == abs_x && 0 == abs_y))
  {
    SwapGems(start_gem, end_gem);
    GemsList chain;
    MakesChain(start_gem, chain);
    MakesChain(end_gem, chain);
    if (chain.empty())
    {
      SwapGems(start_gem, end_gem);
      animation_factory_->GemSwitch(start_gem, end_gem, false);
    }
    else
    {
      check_list_.push_back(start_gem);
      check_list_.push_back(end_gem);
      animation_factory_->GemSwitch(start_gem, end_gem, true);
    }
    return true;
  }
  else
  {
    return false;
  }
}

void Grid::SetVariableContainer(const king::core::VariableContainer::SharedPtr &variables_container)
{
  assert(!variables_container.isNull());
  score_ = variables_container->Get<int>("game_state_score");
  assert(!score_.isNull());
}

void Grid::Initialize()
{
  gems_list_.resize(size_.x * size_.y, NULL);
  const glm::vec2& dimensions = GetFrame()->GetDimension();
  gem_size_.x = dimensions.x / size_.x;
  gem_size_.y = dimensions.y / size_.y;
  for (int collumn = 0; collumn < size_.x; ++collumn)
  {
    RefillCollumn(collumn, true);
  }
}

bool Grid::MakesChain(king::game::Gem* gem, GemsList& chain) const
{
//  CheckForDirectionChain(gem, kUp, chain);
//  CheckForDirectionChain(gem, kDown, chain);
//  CheckForDirectionChain(gem, kRight, chain);
//  CheckForDirectionChain(gem, kLeft, chain);
  CheckForChain(gem, true, chain);
  CheckForChain(gem, false, chain);
  
  if (chain.empty())
  {
    return false;
  }
  else
  {
    chain.push_back(gem);
    return true;
  }
}

void Grid::CheckForDirectionChain(king::game::Gem *gem, king::game::Grid::Direction direction, GemsList &chain, int& counter) const
{
  king::game::Gem* temporary_gem = NULL;
  const glm::ivec2& position = gem->GetGridPosition();
  const king::game::Gem::Type type = gem->GetType();
  const int max_iterations = std::max(size_.x, size_.y);
  for (int i = 1; i < max_iterations + 1; ++i)
  {
    temporary_gem = GetGem(position, direction, i);
    if (temporary_gem && temporary_gem->GetType() == type)
    {
      chain.push_back(temporary_gem);
      ++counter;
    }
    else
    {
      break;
    }
  }
}

void Grid::CheckForChain(king::game::Gem *gem, bool vertical, GemsList &chain) const
{
  int added = 0;
  CheckForDirectionChain(gem, (vertical ? kUp : kRight), chain, added);
  CheckForDirectionChain(gem, (vertical ? kDown : kLeft), chain, added);
  if (added < 2)
  {
    for (int i = 0; i < added; ++i)
    {
      chain.pop_back();
    }
  }
}

king::game::Gem* Grid::GetGem(const glm::ivec2 &position, king::game::Grid::Direction direction, int distance) const
{
  glm::ivec2 pos = position;
  switch (direction)
  {
    case kUp:
    {
      pos.y -= distance;
    } break;
    
    case kDown:
    {
      pos.y += distance;
    } break;
    
    case kLeft:
    {
      pos.x -= distance;
    } break;
    
    case kRight:
    {
      pos.x += distance;
    } break;
  };
  if (pos.x >= 0 && pos.x < size_.x && pos.y >= 0 && pos.y < size_.y)
  {
    return gems_list_[((pos.x * size_.y) + pos.y)];
  }
  return NULL;
}

void Grid::RefillCollumn(int collumn, bool first_time)
{
  assert(collumn >= 0 && collumn < size_.x);
  std::deque<int> free_collumn_positions;
  king::game::Gem* temporary_gem = NULL;
  int row = 0;
  for (int i = size_.y - 1; i >= 0; --i)
  {
    temporary_gem = GetGem(glm::ivec2(collumn, i), kUp, 0);
    if (NULL == temporary_gem)
    {
      free_collumn_positions.push_back(i);
    }
    else if (!free_collumn_positions.empty())
    {
      row = free_collumn_positions.front();
      free_collumn_positions.pop_front();
      MoveDown(temporary_gem, glm::ivec2(collumn, row));
      free_collumn_positions.push_back(i);
    }
  }
  
  if (!free_collumn_positions.empty())
  {
    int index = 0;
    while (!free_collumn_positions.empty())
    {
      row = free_collumn_positions.front();
      DropIn(static_cast<king::game::Gem::Type>(random_num_generator_.next(king::game::Gem::kCount)), glm::ivec2(collumn, row), ++index, first_time);
      free_collumn_positions.pop_front();
    }
  }
}

void Grid::DropIn(king::game::Gem::Type type, const glm::ivec2 &grid_position, int drop_index, bool avoid_chains)
{
  king::game::Gem* gem = game_object_factory_->CreateGem(type, grid_position);
  if (avoid_chains)
  {
    GemsList chain;
    if (MakesChain(gem, chain))
    {
      king::game::Gem* left_gem = GetGem(gem->GetGridPosition(), kLeft, 1);
      king::game::Gem* down_gem = GetGem(gem->GetGridPosition(), kDown, 1);
      king::game::Gem* right_gem = GetGem(gem->GetGridPosition(), kRight, 1);
      king::game::Gem::Type not_available[3] = {
        (NULL != left_gem ? left_gem->GetType() : type),
        (NULL != down_gem ? down_gem->GetType() : type),
        (NULL != right_gem ? right_gem->GetType() : type)
      };
      do
      {
        type = static_cast<king::game::Gem::Type>(type + 1);
        if (king::game::Gem::kCount == type)
        {
          type = king::game::Gem::kBlue;
        }
      } while (not_available[0] == type && not_available[1] == type && not_available[2] == type);
      game_object_factory_->Destroy(gem);
      gem = game_object_factory_->CreateGem(type, grid_position);
    }
  }
  const glm::vec2& offset = GetFrame()->GetPosition();
  glm::vec2 start_position(offset.x + grid_position.x * gem_size_.x, offset.y - gem_size_.y * drop_index);
  glm::vec2 end_position(grid_position.x * gem_size_.x + offset.x, grid_position.y * gem_size_.y + offset.y);
  animation_factory_->GemDropIn(gem, start_position, end_position, GemMoveAnimationDuration(glm::ivec2(grid_position.x, -drop_index), grid_position));
  gem->GetFrame()->SetPosition(start_position);
  gem->GetFrame()->SetDimension(gem_size_);
  gems_list_[grid_position.x * size_.y + grid_position.y] = gem;
  check_list_.push_back(gem);
}

void Grid::MoveDown(king::game::Gem *gem, const glm::ivec2 &grid_position)
{
  glm::ivec2 current_grid_pos = gem->GetGridPosition();
  gems_list_[current_grid_pos.x * size_.y + current_grid_pos.y] = NULL;
  gem->SetGridPosition(grid_position);
  const glm::vec2& offset = GetFrame()->GetPosition();
  animation_factory_->GemDropIn(gem, gem->GetFrame()->GetPosition(), glm::vec2(grid_position.x * gem_size_.x + offset.x, grid_position.y * gem_size_.y + offset.y), GemMoveAnimationDuration(current_grid_pos, grid_position));
  gems_list_[grid_position.x * size_.y + grid_position.y] = gem;
  check_list_.push_back(gem);
}

void Grid::SwapGems(king::game::Gem *start_gem, king::game::Gem *end_gem)
{
  glm::ivec2 start_grid_pos = start_gem->GetGridPosition();
  gems_list_[start_grid_pos.x * size_.y + start_grid_pos.y] = end_gem;
  gems_list_[end_gem->GetGridPosition().x * size_.y + end_gem->GetGridPosition().y] = start_gem;
  start_gem->SetGridPosition(end_gem->GetGridPosition());
  end_gem->SetGridPosition(start_grid_pos);
}

void Grid::DestroyGems(GemsList &gems)
{
  for (GemsList::const_iterator iter = gems.begin(); gems.end() != iter; ++iter)
  {
    game_object_factory_->Destroy((*iter));
  }
  gems.clear();
}

void Grid::UpdateChains()
{
  GemsList chain;
  int score_value = score_->GetData();
  const glm::ivec2* grid_position = NULL;
  king::game::Gem* gem = NULL;
  for (GemsList::const_iterator iter = check_list_.begin(); check_list_.end() != iter; ++iter)
  {
    chain.clear();
    if (MakesChain((*iter), chain))
    {
      for (GemsList::const_iterator chain_iter = chain.begin(); chain.end() != chain_iter; ++chain_iter)
      {
        gem = (*chain_iter);
        if (gem->GetState() != king::game::Gem::kDestroyed)
        {
          gem->SetState(king::game::Gem::kDestroyed);
          grid_position = &gem->GetGridPosition();
          gems_list_[grid_position->x * size_.x + grid_position->y] = NULL;
          animation_factory_->GemDestroy(gem, 0);
          remove_list_.push_back(gem);
          score_value += kScorePerGem;
        }
      }
    }
  }
  score_->SetData(score_value);
  check_list_.clear();
}

} // namespace game
} // namespace king
