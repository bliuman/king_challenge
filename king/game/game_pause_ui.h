//
//  game_pause_ui.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 04/11/13.
//
//

#ifndef KING_GAME_PAUSE_UI_H
#define KING_GAME_PAUSE_UI_H

#include <map>
#include <string>
#include "king/gui/gui_screen.h"

namespace king {
namespace core {

class StateContainer;

} // namespace core
} // namespace king

namespace Rocket {
namespace Core {

class ElementDocument;
class Event;

} // namespace Core
} // namespace Rocket

namespace king {
namespace game {

class GamePauseUi: public king::gui::GuiScreen {
  public:
    static const std::string kUiFileName;
  
  private:
    typedef void (GamePauseUi::*ClickHandler)();
    typedef std::map<std::string, ClickHandler> ClickHandlers;
  
  public:
    GamePauseUi(Rocket::Core::ElementDocument* rocket_document);
    virtual ~GamePauseUi();
    virtual void ProcessEvent(Rocket::Core::Event& evt);
    void SetStateContainer(king::core::StateContainer* state_container);
  
  private:
    void ContinueButtonClicked();
    void MainMenuButtonClicked();
    
    ClickHandlers click_handlers_;
    king::core::StateContainer* state_container_;
    
};

} // namespace game
} // namespace king

#endif  // KING_GAME_PAUSE_UI_H
