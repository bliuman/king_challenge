//
//  game_object_factory.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#include "king/game/game_object_factory.h"
#include <assert.h>
#include <glm/glm.hpp>
#include "king/core/frame.h"
#include "king/game/grid.h"
#include "king/render_state.h"

namespace king {
namespace game {

GameObjectFactory::GameObjectFactory(const king::TextureManager::SharedPtr& texture_manager, const king::RenderQueue::SharedPtr& render_queue, const king::RenderStateFactory::SharedPtr& render_state_factory, king::game::AnimationFactory* animation_factory)
  :texture_manager_(texture_manager)
  ,render_queue_(render_queue)
  ,render_state_factory_(render_state_factory)
  ,animation_factory_(animation_factory)
{
  assert(!texture_manager_.isNull());
  assert(!render_queue_.isNull());
  assert(NULL != render_state_factory_);
  assert(NULL != animation_factory_);
}

GameObjectFactory::~GameObjectFactory()
{
}

void GameObjectFactory::Destroy(king::core::GameObject* game_object)
{
  render_queue_->RemoveGameObject(game_object);
  delete game_object;
}

king::core::GameObject* GameObjectFactory::CreateGameObject(const std::string& name)
{
  king::core::GameObject* game_object = NULL;
  if (name == "background")
  {
    king::core::Frame* frame = new king::core::Frame(glm::vec2(.0f, .0f), glm::vec2(800.f, 600.f));
    game_object = new king::core::GameObject(frame);
    game_object->AddRenderState("default", render_state_factory_->Create("background_default"));
    game_object->SwitchRenderState("default");
  }
  assert(NULL != game_object);
  render_queue_->AddGameObject(game_object);
  return game_object;
}

king::game::Grid* GameObjectFactory::CreateGrid()
{
  king::core::Frame* frame = new king::core::Frame(glm::vec2(355.0f, 95.0f), glm::vec2(344.f, 344.f));
  king::game::Grid* grid = new king::game::Grid(frame, this, animation_factory_, glm::ivec2(8, 8));
  grid->AddRenderState("default", render_state_factory_->Create("grid_default"));
  grid->SwitchRenderState("default");
  render_queue_->AddGameObject(grid);
  return grid;
}

king::game::Gem* GameObjectFactory::CreateGem(king::game::Gem::Type type, const glm::ivec2& grid_position)
{
  king::core::Frame* frame = new king::core::Frame(glm::vec2(.0f, .0f), glm::vec2(.0f, .0f));
  king::game::Gem* gem = new king::game::Gem(frame, type, grid_position);
  switch (type)
  {
    case king::game::Gem::kBlue:
    {
      gem->AddRenderState("default", render_state_factory_->Create("blue_gem_default"));
      gem->AddRenderState("select", render_state_factory_->Create("blue_gem_select"));
    } break;
    
    case king::game::Gem::kGreen:
    {
      gem->AddRenderState("default", render_state_factory_->Create("green_gem_default"));
      gem->AddRenderState("select", render_state_factory_->Create("green_gem_select"));
    } break;
    
    case king::game::Gem::kPurple:
    {
      gem->AddRenderState("default", render_state_factory_->Create("purple_gem_default"));
      gem->AddRenderState("select", render_state_factory_->Create("purple_gem_select"));
    } break;
    
    case king::game::Gem::kRed:
    {
      gem->AddRenderState("default", render_state_factory_->Create("red_gem_default"));
      gem->AddRenderState("select", render_state_factory_->Create("red_gem_select"));
    } break;
    
    case king::game::Gem::kYellow:
    {
      gem->AddRenderState("default", render_state_factory_->Create("yellow_gem_default"));
      gem->AddRenderState("select", render_state_factory_->Create("yellow_gem_select"));
    } break;
    
    default:
    {
      assert(false);
    } break;
  }
  gem->SwitchRenderState("default");
  render_queue_->AddGameObject(gem);
  return gem;
}

} // namespace game
} // namespace king
