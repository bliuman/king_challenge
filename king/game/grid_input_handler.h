//
//  grid_input_handler.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/28/13.
//
//

#ifndef KING_GRID_INPUT_HANDLER_H
#define KING_GRID_INPUT_HANDLER_H

#include <glm/glm.hpp>

struct SDL_MouseButtonEvent;
struct SDL_MouseMotionEvent;

namespace king {
namespace event {

class EventQueue;

} // namespace event

namespace game {

class Grid;
class Gem;

} // namespace game
} // namespace king

namespace king {
namespace game {

class GridInputHandler {
  private:
    static const int kMinDragDistance = 60;
  
  public:
    GridInputHandler(king::game::Grid* grid);
    ~GridInputHandler();
    bool OnMouseButtonDown(const SDL_MouseButtonEvent& evt);
    bool OnMouseButtonUp(const SDL_MouseButtonEvent& evt);
    bool OnMouseMotion(const SDL_MouseMotionEvent& evt);
  
  private:
    int SquaredLength(const glm::ivec2& vec) const;
  
    king::game::Grid* grid_;
    king::game::Gem* selected_gem_;
    glm::ivec2 drag_start_;
    glm::ivec2 drag_offset_;
    bool mouse_pressed_;
    bool dragging_;
  
};

} // namespace game
} // namespace king

#endif  // KING_GRID_INPUT_HANDLER_H
