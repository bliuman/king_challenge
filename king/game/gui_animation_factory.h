//
//  gui_animation_factory.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/3/13.
//
//

#ifndef KING_GAME_GUI_ANIMATION_FACTORY_H
#define KING_GAME_GUI_ANIMATION_FACTORY_H

#include <glm/glm.hpp>
#include <Poco/SharedPtr.h>
#include "king/animation/animation_system.h"

namespace Rocket {
namespace Core {

class Element;

} // namespace Core
} // namespace Rocket

namespace king {
namespace animation {

class Animation;
class BaseAnimation;

} // namespace animation
} // namespace king

namespace king {
namespace game {

class GuiAnimationFactory {
  public:
    typedef Poco::SharedPtr<GuiAnimationFactory> SharedPtr;
  
  public:
    GuiAnimationFactory(const king::animation::AnimationSystem::SharedPtr& animation_system);
    ~GuiAnimationFactory();
    king::animation::Animation* MainMenuAppear(Rocket::Core::Element* rocket_element);
    king::animation::Animation* MainMenuDisappear(Rocket::Core::Element* rocket_element);
    king::animation::Animation* GameHudAppear(Rocket::Core::Element* rocket_element);
    king::animation::Animation* GameHudDisappear(Rocket::Core::Element* rocket_element);
    king::animation::Animation* GameEndDialogAppear(Rocket::Core::Element* rocket_element);
    king::animation::Animation* GameEndDialogDisappear(Rocket::Core::Element* rocket_element);
    king::animation::Animation* PauseMenuAppear(Rocket::Core::Element* rocket_element);
    king::animation::Animation* PauseMenuDisappear(Rocket::Core::Element* rocket_element);
  
  private:
    king::animation::BaseAnimation* TranslationAnimation(Rocket::Core::Element* rocket_element, const glm::vec2& start_position, const glm::vec2& end_position, float duration);
  
    king::animation::AnimationSystem::SharedPtr animation_system_;
    
};

} // namespace game
} // namespace king

#endif  // KING_GAME_GUI_ANIMATION_SOURCE_H
