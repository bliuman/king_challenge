//
//  game_pause_state.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 04/11/13.
//
//

#ifndef KING_GAME_GAME_PAUSE_STATE_H
#define KING_GAME_GAME_PAUSE_STATE_H

#include "king/core/state.h"
#include "king/animation/animation_system.h"
#include "king/gui/gui_manager.h"
#include "king/render_queue.h"
#include "king/texture_manager.h"

namespace king {
namespace core {

class StateContainer;

} // namespace core

namespace game {

class GuiAnimationFactory;
class GamePauseUi;

} // namespace game
} // namespace king

namespace king {
namespace game {

class GamePauseState: public king::core::State {
  public:
    GamePauseState(const king::RenderQueue::SharedPtr& render_queue, const king::TextureManager::SharedPtr& texture_manager, const king::animation::AnimationSystem::SharedPtr& animation_system, const king::gui::GuiManager::SharedPtr& gui_manager);
    virtual ~GamePauseState();
    virtual void Activate(king::core::StateContainer* state_container);
    virtual void Enter();
    virtual void Pause();
    virtual void ProcessEvents(king::event::EventQueue& event_queue);
    virtual void Update(float delta_frame);
    virtual void Resume();
    virtual void Leave();
    virtual void Deactivate();
  
  private:
    king::game::GuiAnimationFactory* gui_animation_factory_;
    king::gui::Context* gui_context_;
    king::game::GamePauseUi* game_pause_ui_;
  
};

} // namespace game
} // namespace king

#endif  // KING_GAME_GAME_PAUSE_STATE_H
