//
//  game_hud_ui.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/3/13.
//
//

#include "king/game/game_hud_ui.h"
#include <assert.h>
#include <Poco/Delegate.h>
#include <Poco/Format.h>
#include <Rocket/Core.h>

namespace king {
namespace game {

const std::string GameHudUi::kUiFileName = "assets/game_hud.rml";

GameHudUi::GameHudUi(Rocket::Core::ElementDocument* rocket_document)
  :king::gui::GuiScreen(rocket_document)
{
}

GameHudUi::~GameHudUi()
{
  assert(!timer_.isNull());
  timer_->OnVariableChangedEvent -= Poco::delegate(this, &GameHudUi::OnTimerUpdate);
  assert(!score_.isNull());
  score_->OnVariableChangedEvent -= Poco::delegate(this, &GameHudUi::OnScoreUpdate);
}

void GameHudUi::ProcessEvent(Rocket::Core::Event &evt)
{
}

void GameHudUi::SetVariableContainer(const king::core::VariableContainer::SharedPtr& variable_container)
{
  timer_ = variable_container->Get<float>("game_state_timer");
  timer_->OnVariableChangedEvent += Poco::delegate(this, &GameHudUi::OnTimerUpdate);
  OnTimerUpdate(NULL, timer_->GetData());
  score_ = variable_container->Get<int>("game_state_score");
  score_->OnVariableChangedEvent += Poco::delegate(this, &GameHudUi::OnScoreUpdate);
  OnScoreUpdate(NULL, score_->GetData());
}

void GameHudUi::OnTimerUpdate(const void *sender, const float& value)
{
  Rocket::Core::Element* timer_element = GetRocketDocument()->GetElementById("timer");
  assert(NULL != timer_element);
  timer_element->SetInnerRML(Poco::format("Time left: %d:%02d", (int)value / 60, (int)value % 60).c_str());
}

void GameHudUi::OnScoreUpdate(const void *sender, const int& value)
{
  Rocket::Core::Element* score_element = GetRocketDocument()->GetElementById("score");
  assert(NULL != score_element);
  score_element->SetInnerRML(Poco::format("Score: %d", value).c_str());
}

} // namespace game
} // namespace king
