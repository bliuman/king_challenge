//
//  main_menu_ui.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/30/13.
//
//

#include "king/game/main_menu_ui.h"
#include <assert.h>
#include <Rocket/Core.h>
#include "king/core/state_container.h"

namespace king {
namespace game {

const std::string MainMenuUi::kUiFileName = "assets/main_menu.rml";

MainMenuUi::MainMenuUi(Rocket::Core::ElementDocument* rocket_document)
  :king::gui::GuiScreen(rocket_document)
  ,click_handlers_()
  ,state_container_(NULL)
{
  RegisterEvent("start_btn", "click");
  RegisterEvent("quit_btn", "click");
  click_handlers_.insert(ClickHandlers::value_type("start_btn", &MainMenuUi::StartButtonClicked));
  click_handlers_.insert(ClickHandlers::value_type("quit_btn", &MainMenuUi::QuitButtonClicked));
}

MainMenuUi::~MainMenuUi()
{
  UnregisterEvent("start_btn", "click");
  UnregisterEvent("quit_btn", "click");
}

void MainMenuUi::ProcessEvent(Rocket::Core::Event &evt)
{
  if (evt.GetType() == "click")
  {
    ClickHandlers::iterator iter = click_handlers_.find(evt.GetCurrentElement()->GetId().CString());
    if (click_handlers_.end() != iter)
    {
      (this->*iter->second)();
    }
  }
}

void MainMenuUi::SetStateContainer(king::core::StateContainer *state_container)
{
  assert(NULL != state_container);
  state_container_ = state_container;
}

void MainMenuUi::StartButtonClicked()
{
  state_container_->PushState("game_state");
}

void MainMenuUi::QuitButtonClicked()
{
  state_container_->PopState("main_menu_state");
}

} // namespace game
} // namespace king
