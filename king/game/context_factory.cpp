//
//  context_factory.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/31/13.
//
//

#include "king/game/context_factory.h"
#include <assert.h>
#include <Rocket/Core.h>
#include "king/gui/context.h"

namespace king {
namespace game {

ContextFactory::ContextFactory(const glm::ivec2& screen_size, const king::RenderStateFactory::SharedPtr& render_state_factory, const king::RenderQueue::SharedPtr& render_queue)
  :screen_size_(screen_size)
  ,render_state_factory_(render_state_factory)
  ,render_queue_(render_queue)
{
  assert(!render_state_factory_.isNull());
  assert(!render_queue_.isNull());
}

ContextFactory::~ContextFactory()
{
}

king::gui::Context* ContextFactory::Create(const king::gui::ContextFactoryParam& param)
{
  king::gui::Context* context = new king::gui::Context(Rocket::Core::CreateContext(param.name_.c_str(), Rocket::Core::Vector2i(screen_size_.x, screen_size_.y)), param.screen_factory_);
  if (param.name_ == "main_menu")
  {
    context->AddRenderState("default", render_state_factory_->Create("main_menu_context_default"));
  }
  else if (param.name_ == "game_menu")
  {
    context->AddRenderState("default", render_state_factory_->Create("game_menu_context_default"));
  }
  else if (param.name_ == "game_end_menu")
  {
    context->AddRenderState("default", render_state_factory_->Create("game_end_menu_context_default"));
  }
  else if (param.name_ == "game_pause_menu")
  {
    context->AddRenderState("default", render_state_factory_->Create("game_pause_menu_context_default"));
  }
  context->SwitchRenderState("default");
  render_queue_->AddGameObject(context);
  return context;
}

void ContextFactory::Destroy(king::gui::Context *context)
{
  assert(NULL != context);
  render_queue_->RemoveGameObject(context);
  delete context;
}

} // namespace game
} // namespace king