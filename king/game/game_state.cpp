//
//  game_state.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#include "king/game/game_state.h"
#include <SDL_events.h>
#include <Rocket/Core/ElementDocument.h>
#include "king/animation/animation_system.h"
#include "king/core/variable_container.h"
#include "king/event/event_queue.h"
#include "king/game/animation_factory.h"
#include "king/game/game_hud_ui.h"
#include "king/game/game_object_factory.h"
#include "king/game/grid.h"
#include "king/game/grid_input_handler.h"
#include "king/game/gui_animation_factory.h"
#include "king/game/gui_screen_factory.h"
#include "king/render_state_factory.h"
#include "king/core/state_container.h"

namespace king {
namespace game {

GameState::GameState(const king::RenderQueue::SharedPtr& render_queue, const king::TextureManager::SharedPtr& texture_manager, const king::animation::AnimationSystem::SharedPtr& animation_system, const king::gui::GuiManager::SharedPtr& gui_manager)
  :king::core::State(render_queue, texture_manager, animation_system, gui_manager)
  ,game_object_factory_(NULL)
  ,animation_factory_(NULL)
  ,gui_animation_factory_(NULL)
  ,grid_(NULL)
  ,grid_input_handler_(NULL)
  ,game_hud_ui_(NULL)
  ,timer_(new king::core::Variable<float>(60.0f))
  ,score_(new king::core::Variable<int>(0))
{
}

GameState::~GameState()
{
}

void GameState::Activate(king::core::StateContainer* state_container)
{
  king::core::State::Activate(state_container);
  animation_factory_ = new king::game::AnimationFactory(GetAnimationSystem());
  gui_animation_factory_ = new king::game::GuiAnimationFactory(GetAnimationSystem());
  game_object_factory_ = new king::game::GameObjectFactory(GetTextureManager(), GetRenderQueue(), new king::RenderStateFactory(GetTextureManager()), animation_factory_);
  king::core::VariableContainer::SharedPtr variable_container = state_container->GetVariableContainer();
  variable_container->Add<float>("game_state_timer", timer_);
  variable_container->Add<int>("game_state_score", score_);
  
}

void GameState::Enter()
{
  if (GetAnimationSystem()->IsAnimationsRunning())
  {
    return;
  }
  king::core::State::Enter();
  grid_ = game_object_factory_->CreateGrid();
  grid_->SetVariableContainer(GetStateContainer()->GetVariableContainer());
  grid_input_handler_ = new king::game::GridInputHandler(grid_);
  gui_context_ = GetGuiManager()->CreateContext(king::gui::ContextFactoryParam("game_menu", glm::ivec2(800, 600), new king::game::GuiScreenFactory()));
  game_hud_ui_ = dynamic_cast<king::game::GameHudUi*>(gui_context_->CreateScreen("game_hud"));
  assert(NULL != game_hud_ui_);
  game_hud_ui_->SetVariableContainer(GetStateContainer()->GetVariableContainer());
  gui_animation_factory_->GameHudAppear(game_hud_ui_->GetRocketDocument());
}

void GameState::Pause()
{
  king::core::State::Pause();
}

void GameState::ProcessEvents(king::event::EventQueue &event_queue)
{
//  gui_context_->Update(event_queue);
  king::core::State::ProcessEvents(event_queue);
  if (GetAnimationSystem()->IsAnimationsRunning())
  {
    return;
  }
  king::event::EventQueue::Events& events = event_queue.GetEvents();
  SDL_Event* evt = NULL;
  for (king::event::EventQueue::Events::iterator iter = events.begin(); events.end() != iter; ++iter)
  {
      if (iter->second)
      {
          continue;
      }
      evt = &iter->first;
    
      switch (evt->type)
      {
        case SDL_MOUSEBUTTONDOWN:
        {
          iter->second = grid_input_handler_->OnMouseButtonDown(evt->button);
        } break;
        
        case SDL_MOUSEBUTTONUP:
        {
          iter->second = grid_input_handler_->OnMouseButtonUp(evt->button);
        } break;
        
        case SDL_MOUSEMOTION:
        {
          iter->second = grid_input_handler_->OnMouseMotion(evt->motion);
        } break;
        
        case SDL_KEYUP:
        {
          if (SDLK_ESCAPE == evt->key.keysym.sym)
          {
            GetStateContainer()->PushState("game_pause_state");
          }
        } break;
        
      };
  }
}

void GameState::Update(float delta_frame)
{
  king::core::State::Update(delta_frame);
  if (!GetAnimationSystem()->IsAnimationsRunning())
  {
    grid_->UpdateGridState();
  }
  
  float value = timer_->GetData() - delta_frame;
  timer_->SetData(value);
  if (value <= 0.f)
  {
    timer_->SetData(.0f);
    if (!GetAnimationSystem()->IsAnimationsRunning())
    {
      GetStateContainer()->PushState("game_end_state");
    }
  }
}

void GameState::Resume()
{
  king::core::State::Resume();
  if (timer_->GetData() <= 0.f)
  {
    timer_->SetData(60.0f);
    score_->SetData(0);
    delete grid_input_handler_;
    game_object_factory_->Destroy(grid_);
    grid_ = game_object_factory_->CreateGrid();
    grid_->SetVariableContainer(GetStateContainer()->GetVariableContainer());
    grid_input_handler_ = new king::game::GridInputHandler(grid_);
  }
}

void GameState::Leave()
{
  king::core::State::Leave();
  gui_animation_factory_->GameHudDisappear(game_hud_ui_->GetRocketDocument());
  grid_->AnimateClearGrid();
}

void GameState::Deactivate()
{
  if (GetAnimationSystem()->IsAnimationsRunning())
  {
    return;
  }
  king::core::VariableContainer::SharedPtr& variables_container = GetStateContainer()->GetVariableContainer();
  variables_container->Remove<float>("game_state_timer");
  variables_container->Remove<int>("game_state_score");
  gui_context_->DestroyScreen(game_hud_ui_);
  GetGuiManager()->DestroyContext(gui_context_);
  delete grid_input_handler_;
  game_object_factory_->Destroy(grid_);
  delete game_object_factory_;
  delete gui_animation_factory_;
  delete animation_factory_;
  king::core::State::Deactivate();
}

} // namespace game
} // namespace king
