//
//  game_state.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#ifndef KING_GAME_GAME_STATE_H
#define KING_GAME_GAME_STATE_H

#include "king/animation/animation_system.h"
#include "king/core/variable.h"
#include "king/gui/gui_manager.h"
#include "king/render_queue.h"
#include "king/core/state.h"
#include "king/texture_manager.h"

namespace king {
namespace core {

class StateContainer;

} // namespace core

namespace game {

class AnimationFactory;
class GameHudUi;
class GameObject;
class GameObjectFactory;
class Grid;
class GridInputHandler;
class GuiAnimationFactory;

namespace gui {

class Context;

} // namespace gui
} // namespace game

namespace event {

class EventQueue;

} // namespace event
} // namespace king

namespace king {
namespace game {

class GameState: public king::core::State {
  public:
    GameState(const king::RenderQueue::SharedPtr& render_queue, const king::TextureManager::SharedPtr& texture_manager, const king::animation::AnimationSystem::SharedPtr& animation_system, const king::gui::GuiManager::SharedPtr& gui_manager);
    virtual ~GameState();
    virtual void Activate(king::core::StateContainer* state_container);
    virtual void Enter();
    virtual void Pause();
    virtual void ProcessEvents(king::event::EventQueue& event_queue);
    virtual void Update(float delta_frame);
    virtual void Resume();
    virtual void Leave();
    virtual void Deactivate();
  
  private:
    king::game::GameObjectFactory* game_object_factory_;
    king::game::AnimationFactory* animation_factory_;
    king::game::GuiAnimationFactory* gui_animation_factory_;
    king::game::Grid* grid_;
    king::game::GridInputHandler* grid_input_handler_;
    king::gui::Context* gui_context_;
    king::game::GameHudUi* game_hud_ui_;
    king::core::Variable<float>::SharedPtr timer_;
    king::core::Variable<int>::SharedPtr score_;
  
};

} // namespace game
} // namespace king

#endif  // KING_GAME_GAME_STATE_H
