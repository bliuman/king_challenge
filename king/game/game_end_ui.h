//
//  game_end_ui.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 04/11/13.
//
//

#ifndef KING_GAME_END_UI_H
#define KING_GAME_END_UI_H

#include <map>
#include <string>
#include "king/gui/gui_screen.h"

namespace king {
namespace core {

class StateContainer;

} // namespace core
} // namespace king

namespace Rocket {
namespace Core {

class ElementDocument;
class Event;

} // namespace Core
} // namespace Rocket

namespace king {
namespace game {

class GameEndUi: public king::gui::GuiScreen {
  public:
    static const std::string kUiFileName;
  
  private:
    typedef void (GameEndUi::*ClickHandler)();
    typedef std::map<std::string, ClickHandler> ClickHandlers;
  
  public:
    GameEndUi(Rocket::Core::ElementDocument* rocket_document);
    virtual ~GameEndUi();
    virtual void ProcessEvent(Rocket::Core::Event& evt);
    void SetStateContainer(king::core::StateContainer* state_container);
  
  private:
    void ReplayButtonClicked();
    void MainMenuButtonClicked();
    
    ClickHandlers click_handlers_;
    king::core::StateContainer* state_container_;
    
};

} // namespace game
} // namespace king

#endif  // KING_GAME_END_UI_H
