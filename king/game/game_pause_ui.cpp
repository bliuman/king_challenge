//
//  game_pause_ui.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 04/1/13.
//
//

#include "king/game/game_pause_ui.h"
#include <assert.h>
#include <Rocket/Core.h>
#include "king/core/state_container.h"

namespace king {
namespace game {

const std::string GamePauseUi::kUiFileName = "assets/game_pause_ui.rml";

GamePauseUi::GamePauseUi(Rocket::Core::ElementDocument* rocket_document)
  :king::gui::GuiScreen(rocket_document)
  ,click_handlers_()
  ,state_container_(NULL)
{
  RegisterEvent("continue_btn", "click");
  RegisterEvent("main_menu_btn", "click");
  click_handlers_.insert(ClickHandlers::value_type("continue_btn", &GamePauseUi::ContinueButtonClicked));
  click_handlers_.insert(ClickHandlers::value_type("main_menu_btn", &GamePauseUi::MainMenuButtonClicked));
}

GamePauseUi::~GamePauseUi()
{
  UnregisterEvent("continue_btn", "click");
  UnregisterEvent("main_menu_btn", "click");
}

void GamePauseUi::ProcessEvent(Rocket::Core::Event &evt)
{
  if (evt.GetType() == "click")
  {
    ClickHandlers::iterator iter = click_handlers_.find(evt.GetCurrentElement()->GetId().CString());
    if (click_handlers_.end() != iter)
    {
      (this->*iter->second)();
    }
  }
}

void GamePauseUi::SetStateContainer(king::core::StateContainer *state_container)
{
  assert(NULL != state_container);
  state_container_ = state_container;
}

void GamePauseUi::ContinueButtonClicked()
{
  state_container_->PopState("game_pause_state");
}

void GamePauseUi::MainMenuButtonClicked()
{
  state_container_->PopState("game_state");
}

} // namespace game
} // namespace king
