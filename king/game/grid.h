//
//  grid.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#ifndef KING_GAME_GRID_H
#define KING_GAME_GRID_H

#include <vector>
#include <glm/glm.hpp>
#include <Poco/Random.h>
#include "king/game/gem.h"
#include "king/core/game_object.h"
#include "king/core/variable.h"
#include "king/core/variable_container.h"

namespace king {

class Frame;

namespace game {

class AnimationFactory;
class GameObjectFactory;
class Gem;

} // namespace game
} // namespace frame

namespace king {
namespace game {

class Grid: public king::core::GameObject {
  public:
    typedef std::pair<int, int> GridPosition;
    typedef std::vector<king::game::Gem*> GemsList;
    enum Direction {
      kUp = 0,
      kDown,
      kLeft,
      kRight
    };
  
  private:
    static const int kScorePerGem = 5;
  
  public:
    Grid(king::core::Frame* frame, king::game::GameObjectFactory* game_object_factory, king::game::AnimationFactory* animation_factory, const glm::ivec2 size);
    virtual ~Grid();
    void UpdateGridState();
    void AnimateClearGrid();
    king::game::Gem* SelectGem(const glm::vec2& screen_point) const;
    bool SwitchGems(king::game::Gem* start_gem, king::game::Gem* end_gem);
    void SetVariableContainer(const king::core::VariableContainer::SharedPtr& variables_container);
  
  private:
    void Initialize();
    king::game::Gem* GetGem(const glm::ivec2& position, Direction direction, int distance) const;
    bool MakesChain(king::game::Gem* gem, GemsList& chain) const;
    void CheckForDirectionChain(king::game::Gem* gem, Direction direction, GemsList& chain, int& counter) const;
    void CheckForChain(king::game::Gem* gem, bool vertical, GemsList& chain) const;
    void RefillCollumn(int collumn, bool first_time = false);
    void DropIn(king::game::Gem::Type type, const glm::ivec2& grid_position, int drop_index, bool avoid_chains = false);
    void MoveDown(king::game::Gem* gem, const glm::ivec2& grid_position);
    float GemMoveAnimationDuration(const glm::ivec2& start_grid_pos, const glm::ivec2& end_grid_pos);
    void SwapGems(king::game::Gem* start_gem, king::game::Gem* end_gem);
    void DestroyGems(GemsList& gems);
    void UpdateChains();
  
    king::game::GameObjectFactory* game_object_factory_;
    king::game::AnimationFactory* animation_factory_;
    GemsList gems_list_;
    GemsList check_list_;
    GemsList remove_list_;
    glm::ivec2 size_;
    glm::vec2 gem_size_;
    Poco::Random random_num_generator_;
    king::core::Variable<int>::SharedPtr score_;
  
};

inline float Grid::GemMoveAnimationDuration(const glm::ivec2 &start_grid_pos, const glm::ivec2 &end_grid_pos)
{
  return end_grid_pos.x - start_grid_pos.x + end_grid_pos.y - start_grid_pos.y + random_num_generator_.next(100) / 100.0f;
}

} // namespace game
} // namespace king

#endif  // KING_GAME_GRID_H
