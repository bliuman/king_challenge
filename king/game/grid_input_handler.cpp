//
//  grid_input_handler.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/28/13.
//
//

#include "king/game/grid_input_handler.h"
#include <assert.h>
#include <glm/glm.hpp>
#include <SDL_events.h>
#include "king/event/event_queue.h"
#include "king/core/frame.h"
#include "king/game/grid.h"

namespace king {
namespace game {

GridInputHandler::GridInputHandler(king::game::Grid* grid)
  :grid_(grid)
  ,selected_gem_(NULL)
  ,drag_start_(0, 0)
  ,drag_offset_(0, 0)
  ,mouse_pressed_(false)
  ,dragging_(false)
{
  assert(NULL != grid_);
}

GridInputHandler::~GridInputHandler()
{
}

bool GridInputHandler::OnMouseMotion(const SDL_MouseMotionEvent& evt)
{
  glm::vec2 point(evt.x, evt.y);
  if (!grid_->GetFrame()->Contains(point))
  {
    return false;
  }
  
  if (mouse_pressed_)
  {
    drag_offset_.x += evt.xrel;
    drag_offset_.y += evt.yrel;
    if (!dragging_ && SquaredLength(drag_offset_) >= kMinDragDistance)
    {
      dragging_ = true;
    }
    
    if (dragging_)
    {
      king::game::Gem* another_gem = grid_->SelectGem(glm::vec2(drag_start_.x + drag_offset_.x, drag_start_.y + drag_offset_.y));
      if (selected_gem_ != another_gem)
      {
        selected_gem_->SwitchRenderState("default");
        grid_->SwitchGems(selected_gem_, another_gem);
        selected_gem_ = NULL;
        mouse_pressed_ = false;
      }
    }
  }
  return true;
}

bool GridInputHandler::OnMouseButtonDown(const SDL_MouseButtonEvent& evt)
{
  glm::vec2 point(evt.x, evt.y);
  if (!grid_->GetFrame()->Contains(point))
  {
    return false;
  }
  
  if (NULL == selected_gem_)
  {
    selected_gem_ = grid_->SelectGem(point);
    selected_gem_->SwitchRenderState("select");
  }
  drag_start_ = point;
  drag_offset_.x = 0;
  drag_offset_.y = 0;
  mouse_pressed_ = true;
  return true;
}

bool GridInputHandler::OnMouseButtonUp(const SDL_MouseButtonEvent& evt)
{
  glm::vec2 point(evt.x, evt.y);
  if (!grid_->GetFrame()->Contains(point))
  {
    return false;
  }
  
  if (mouse_pressed_ && !dragging_)
  {
    king::game::Gem* another_gem = grid_->SelectGem(point);
    if (selected_gem_ != another_gem)
    {
      selected_gem_->SwitchRenderState("default");
      if (grid_->SwitchGems(selected_gem_, another_gem))
      {
        selected_gem_ = NULL;
      }
      else
      {
        selected_gem_ = another_gem;
        selected_gem_->SwitchRenderState("select");
      }
    }
  }
  mouse_pressed_ = false;
  dragging_ = false;
  return true;
}

int GridInputHandler::SquaredLength(const glm::ivec2& vec) const
{
  return vec.x * vec.x + vec.y * vec.y;
}

} // namespace game
} // namespace king
