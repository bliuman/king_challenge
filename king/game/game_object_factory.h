//
//  game_object_factory.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#ifndef KING_GAME_OBJECT_FACTORY_H
#define KING_GAME_OBJECT_FACTORY_H

#include <string>
#include <glm/glm.hpp>
#include "king/game/gem.h"
#include "king/render_queue.h"
#include "king/render_state_factory.h"
#include "king/texture_manager.h"

namespace king {
namespace game {

class AnimationFactory;
class Grid;

} // namespace game
} // namespace king

namespace king {
namespace game {

class GameObjectFactory {
  public:
    GameObjectFactory(const king::TextureManager::SharedPtr& texture_manager, const king::RenderQueue::SharedPtr& render_queue, const king::RenderStateFactory::SharedPtr& render_state_factory, king::game::AnimationFactory* animation_factory);
    ~GameObjectFactory();
    void Destroy(king::core::GameObject* game_object);
    king::core::GameObject* CreateGameObject(const std::string& name);
    king::game::Grid* CreateGrid();
    king::game::Gem* CreateGem(king::game::Gem::Type type, const glm::ivec2& grid_position);
  
  private:
    king::TextureManager::SharedPtr texture_manager_;
    king::RenderQueue::SharedPtr render_queue_;
    king::RenderStateFactory::SharedPtr render_state_factory_;
    king::game::AnimationFactory* animation_factory_;
  
};

} // namespace game
} // namespace king

#endif  // KING_GAME_OBJECT_FACTORY_H
