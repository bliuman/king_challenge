//
//  gui_screen_factory.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 01/11/13.
//
//

#ifndef KING_GUI_GUI_SCREEN_FACTORY_H
#define KING_GUI_GUI_SCREEN_FACTORY_H

#include "king/gui/gui_screen.h"

namespace king {
namespace game {

class GuiScreenFactory: public king::gui::GuiScreen::Factory {
  public:
    GuiScreenFactory();
    virtual ~GuiScreenFactory();
    virtual king::gui::GuiScreen* Create(const king::gui::GuiScreen::FactoryParam& parameter);
    virtual void Destroy(king::gui::GuiScreen* screen);
    
};

} // namespace game
} // namespace king

#endif  // KING_GUI_GUI_SCREEN_FACTORY_H