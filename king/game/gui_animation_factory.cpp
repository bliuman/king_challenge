//
//  gui_animation_factory.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/3/13.
//
//

#include "king/game/gui_animation_factory.h"
#include <assert.h>
#include <cpgf/tween/gtween.h>
#include <cpgf/tween/easing/back.h>
#include <cpgf/tween/easing/circle.h>
#include <cpgf/tween/easing/cubic.h>
#include "king/animation/translation.h"
#include "king/gui/gui_object_animation_target.h"

namespace king {
namespace game {


GuiAnimationFactory::GuiAnimationFactory(const king::animation::AnimationSystem::SharedPtr& animation_system)
  :animation_system_(animation_system)
{
  assert(!animation_system_.isNull());
}

GuiAnimationFactory::~GuiAnimationFactory()
{
}

king::animation::Animation* GuiAnimationFactory::MainMenuAppear(Rocket::Core::Element* rocket_element)
{
  assert(NULL != rocket_element);
  king::animation::BaseAnimation* animation = TranslationAnimation(rocket_element, glm::vec2(250.0f, -300.0f), glm::vec2(250.0f, 200.0f), 0.375f);
  animation->GetInterpolationObject().ease(cpgf::BackEase::easeOut());
  animation_system_->AddAnimation("main_menu_appear", animation);
  return animation;
}

king::animation::Animation* GuiAnimationFactory::MainMenuDisappear(Rocket::Core::Element* rocket_element)
{
  assert(NULL != rocket_element);
  king::animation::BaseAnimation* animation = TranslationAnimation(rocket_element, glm::vec2(250.0f, 200.0f), glm::vec2(250.0f, 700.0f), 0.375f);
  animation->GetInterpolationObject().ease(cpgf::BackEase::easeIn());
  animation_system_->AddAnimation("main_menu_disappear", animation);
  return animation;
}

king::animation::Animation* GuiAnimationFactory::GameHudAppear(Rocket::Core::Element* rocket_element)
{
  assert(NULL != rocket_element);
  king::animation::BaseAnimation* animation = TranslationAnimation(rocket_element, glm::vec2(-300.0f, 100.0f), glm::vec2(5.0f, 100.0f), 0.375);
  animation->GetInterpolationObject().ease(cpgf::CubicEase::easeOut());
  animation_system_->AddAnimation("game_hud_appear", animation);
  return animation;
}

king::animation::Animation* GuiAnimationFactory::GameHudDisappear(Rocket::Core::Element* rocket_element)
{
  assert(NULL != rocket_element);
  king::animation::BaseAnimation* animation = TranslationAnimation(rocket_element, glm::vec2(5.0f, 100.0f), glm::vec2(-300.0f, 100.0f), 0.375);
  animation->GetInterpolationObject().ease(cpgf::CubicEase::easeIn());
  animation_system_->AddAnimation("game_hud_disappear", animation);
  return animation;
}

king::animation::Animation* GuiAnimationFactory::GameEndDialogAppear(Rocket::Core::Element *rocket_element)
{
  assert(NULL != rocket_element);
  king::animation::BaseAnimation* animation = TranslationAnimation(rocket_element, glm::vec2(850.0f, 260.0f), glm::vec2(200.0f, 260.0f), 0.375);
  animation->GetInterpolationObject().ease(cpgf::CircleEase::easeIn());
  animation_system_->AddAnimation("game_end_dialog_appear", animation);
  return animation;
}

king::animation::Animation* GuiAnimationFactory::GameEndDialogDisappear(Rocket::Core::Element *rocket_element)
{
  assert(NULL != rocket_element);
  king::animation::BaseAnimation* animation = TranslationAnimation(rocket_element, glm::vec2(200.0f, 260.0f), glm::vec2(850.0f, 260.0f), 0.375);
  animation->GetInterpolationObject().ease(cpgf::CircleEase::easeIn());
  animation_system_->AddAnimation("game_end_dialog_disappear", animation);
  return animation;
}

king::animation::Animation* GuiAnimationFactory::PauseMenuAppear(Rocket::Core::Element* rocket_element)
{
  assert(NULL != rocket_element);
  king::animation::BaseAnimation* animation = TranslationAnimation(rocket_element, glm::vec2(250.0f, -300.0f), glm::vec2(250.0f, 200.0f), 0.375f);
  animation->GetInterpolationObject().ease(cpgf::BackEase::easeOut());
  animation_system_->AddAnimation("pause_menu_appear", animation);
  return animation;
}

king::animation::Animation* GuiAnimationFactory::PauseMenuDisappear(Rocket::Core::Element* rocket_element)
{
  assert(NULL != rocket_element);
  king::animation::BaseAnimation* animation = TranslationAnimation(rocket_element, glm::vec2(250.0f, 200.0f), glm::vec2(250.0f, -300.0f), 0.375f);
  animation->GetInterpolationObject().ease(cpgf::BackEase::easeIn());
  animation_system_->AddAnimation("pause_menu_disappear", animation);
  return animation;
}

king::animation::BaseAnimation* GuiAnimationFactory::TranslationAnimation(Rocket::Core::Element *rocket_element, const glm::vec2 &start_position, const glm::vec2 &end_position, float duration)
{
  king::animation::AnimationTarget* target = new king::gui::GuiObjectAnimationTarget(rocket_element);
  king::animation::BaseAnimation* animation = new king::animation::Translation(target, start_position, end_position);
  animation->GetInterpolationObject().duration(duration);
  return animation;
}

} // namespace game
} // namespace king
