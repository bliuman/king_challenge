//
//  state_factory.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/3/13.
//
//

#ifndef KING_GAME_STATE_FACTORY_H
#define KING_GAME_STATE_FACTORY_H

#include <map>
#include <string>
#include "king/animation/animation_system.h"
#include "king/core/factory.h"
#include "king/gui/gui_manager.h"
#include "king/render_queue.h"
#include "king/core/state.h"
#include "king/texture_manager.h"

namespace king {
namespace game {

class StateFactory: public king::core::State::Factory {
  private:
    typedef king::core::State* (StateFactory::*FactoryFunction)();
    typedef std::map<std::string, FactoryFunction> FactoryFunctionMap;
  
  public:
    StateFactory(const king::RenderQueue::SharedPtr& render_queue, const king::TextureManager::SharedPtr& texture_manager, const king::animation::AnimationSystem::SharedPtr& animation_system, const king::gui::GuiManager::SharedPtr& gui_manager);
    virtual ~StateFactory();
    king::core::State* Create(const std::string& param);
    void Destroy(king::core::State* state);
  
  private:
    template<typename StateType> king::core::State* CreateState();
  
    king::RenderQueue::SharedPtr render_queue_;
    king::TextureManager::SharedPtr texture_manager_;
    king::animation::AnimationSystem::SharedPtr animation_system_;
    king::gui::GuiManager::SharedPtr gui_manager_;
    FactoryFunctionMap factory_functions_;
    
};

template<typename StateType>
king::core::State* StateFactory::CreateState()
{
  return new StateType(render_queue_, texture_manager_, animation_system_, gui_manager_);
}

} // namespace game
} // namespace king

#endif  // KING_GAME_STATE_FACTORY_H
