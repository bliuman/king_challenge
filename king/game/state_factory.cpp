//
//  state_factory.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/3/13.
//
//

#include "king/game/state_factory.h"
#include "king/game/game_end_state.h"
#include "king/game/game_pause_state.h"
#include "king/game/main_menu_state.h"
#include "king/game/game_state.h"

namespace king {
namespace game {

StateFactory::StateFactory(const king::RenderQueue::SharedPtr& render_queue, const king::TextureManager::SharedPtr& texture_manager, const king::animation::AnimationSystem::SharedPtr& animation_system, const king::gui::GuiManager::SharedPtr& gui_manager)
  :render_queue_(render_queue)
  ,texture_manager_(texture_manager)
  ,animation_system_(animation_system)
  ,gui_manager_(gui_manager)
{
  factory_functions_.insert(FactoryFunctionMap::value_type("main_menu_state", &StateFactory::CreateState<king::game::MainMenuState>));
  factory_functions_.insert(FactoryFunctionMap::value_type("game_state", &StateFactory::CreateState<king::game::GameState>));
  factory_functions_.insert(FactoryFunctionMap::value_type("game_end_state", &StateFactory::CreateState<king::game::GameEndState>));
  factory_functions_.insert(FactoryFunctionMap::value_type("game_pause_state", &StateFactory::CreateState<king::game::GamePauseState>));
}

StateFactory::~StateFactory()
{
}

king::core::State* StateFactory::Create(const std::string &param)
{
  FactoryFunctionMap::iterator iter = factory_functions_.find(param);
  assert(factory_functions_.end() != iter);
  king::core::State* state = (this->*iter->second)();
  return state;
}

void StateFactory::Destroy(king::core::State *state)
{
  delete state;
}

} // namespace game
} // namespace king
