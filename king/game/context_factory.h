//
//  context_factory.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/01/13.
//
//

#ifndef KING_GAME_CONTEXT_FACTORY_H
#define KING_GAME_CONTEXT_FACTORY_H

#include <string>
#include <glm/glm.hpp>
#include "king/gui/context.h"
#include "king/gui/gui_screen.h"
#include "king/render_queue.h"
#include "king/render_state_factory.h"

namespace king {
namespace gui {

class Context;

} // namespace gui
} // namespace king

namespace king {
namespace game {

class ContextFactory: public king::gui::Context::Factory {
  public:
    ContextFactory(const glm::ivec2& screen_size, const king::RenderStateFactory::SharedPtr& render_state_factory, const king::RenderQueue::SharedPtr& render_queue);
    virtual ~ContextFactory();
    virtual king::gui::Context* Create(const king::gui::ContextFactoryParam& param);
    virtual void Destroy(king::gui::Context* context);
    
  private:
    glm::ivec2 screen_size_;
    king::RenderStateFactory::SharedPtr render_state_factory_;
    king::RenderQueue::SharedPtr render_queue_;
    
};

} // namespace game
} // namespace king

#endif  // KING_GAME_CONTEXT_FACTORY_H