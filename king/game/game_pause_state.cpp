//
//  game_pause_state.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 04/11/13.
//
//

#include "king/game/game_pause_state.h"
#include <glm/glm.hpp>
#include <Rocket/Core/ElementDocument.h>
#include "king/event/event_queue.h"
#include "king/game/game_pause_ui.h"
#include "king/game/gui_animation_factory.h"
#include "king/game/gui_screen_factory.h"
#include "king/core/state_container.h"


namespace king {
namespace game {

GamePauseState::GamePauseState(const king::RenderQueue::SharedPtr& render_queue, const king::TextureManager::SharedPtr& texture_manager, const king::animation::AnimationSystem::SharedPtr& animation_system, const king::gui::GuiManager::SharedPtr& gui_manager)
  :king::core::State(render_queue, texture_manager, animation_system, gui_manager)
  ,gui_animation_factory_(NULL)
  ,gui_context_(NULL)
  ,game_pause_ui_(NULL)
{
}

GamePauseState::~GamePauseState()
{
}

void GamePauseState::Activate(king::core::StateContainer *state_container)
{
  king::core::State::Activate(state_container);
  gui_animation_factory_ = new king::game::GuiAnimationFactory(GetAnimationSystem());
}

void GamePauseState::Enter()
{
  if (GetAnimationSystem()->IsAnimationsRunning())
  {
    return;
  }
  king::core::State::Enter();
  gui_context_ = GetGuiManager()->CreateContext(king::gui::ContextFactoryParam("game_pause_menu", glm::ivec2(800, 600), new king::game::GuiScreenFactory()));
  game_pause_ui_ = dynamic_cast<king::game::GamePauseUi*>(gui_context_->CreateScreen("game_pause_menu"));
  assert(NULL != game_pause_ui_);
  game_pause_ui_->SetStateContainer(GetStateContainer());
  gui_animation_factory_->PauseMenuAppear(game_pause_ui_->GetRocketDocument());
}

void GamePauseState::Pause()
{
  king::core::State::Pause();
}

void GamePauseState::ProcessEvents(king::event::EventQueue &event_queue)
{
  king::core::State::ProcessEvents(event_queue);
  if (GetAnimationSystem()->IsAnimationsRunning())
  {
    return;
  }
  king::event::EventQueue::Events& events = event_queue.GetEvents();
  SDL_Event* evt = NULL;
  for (king::event::EventQueue::Events::iterator iter = events.begin(); events.end() != iter; ++iter)
  {
      if (iter->second)
      {
          continue;
      }
      evt = &iter->first;
    
      switch (evt->type)
      {
        
        case SDL_KEYUP:
        {
          if (SDLK_ESCAPE == evt->key.keysym.sym)
          {
            GetStateContainer()->PopState("game_pause_state");
          }
        } break;
        
      };
  }
}

void GamePauseState::Update(float delta_frame)
{
  king::core::State::Update(delta_frame);
}

void GamePauseState::Resume()
{
  king::core::State::Resume();
}

void GamePauseState::Leave()
{
  king::core::State::Leave();
  gui_animation_factory_->PauseMenuDisappear(game_pause_ui_->GetRocketDocument());
}

void GamePauseState::Deactivate()
{
  if (GetAnimationSystem()->IsAnimationsRunning())
  {
    return;
  }
  gui_context_->DestroyScreen(game_pause_ui_);
  GetGuiManager()->DestroyContext(gui_context_);
  delete gui_animation_factory_;
  king::core::State::Deactivate();
}

} // namespace game
} // namespace king