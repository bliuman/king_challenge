//
//  gem.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#include "king/game/gem.h"

namespace king {
namespace game {

Gem::Gem(king::core::Frame* frame, Type type, const glm::ivec2& grid_position)
  :king::core::GameObject(frame)
  ,type_(type)
  ,state_(kNormal)
  ,grid_position_(grid_position)
{
}

Gem::~Gem()
{
}

} // namespace game
} // namespace king
