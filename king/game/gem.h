//
//  gem.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#ifndef KING_GAME_GEM_H
#define KING_GAME_GEM_H

#include <glm/glm.hpp>
#include "king/core/game_object.h"

namespace king {

class Frame;
class RenderState;

} // namespace frame

namespace king {
namespace game {

class Gem: public king::core::GameObject {
  public:
    enum Type {
      kBlue = 0,
      kGreen,
      kPurple,
      kRed,
      kYellow,
      kCount
    };
  
    enum State {
      kNormal = 0,
      kDestroyed
    };
  
  public:
    Gem(king::core::Frame* frame, Type type, const glm::ivec2& grid_position);
    virtual ~Gem();
    Type GetType() const {return type_;}
    State GetState() const {return state_;}
    void SetState(State state) {state_ = state;}
    void SetGridPosition(const glm::ivec2& grid_position) {grid_position_ = grid_position;}
    const glm::ivec2& GetGridPosition() const {return grid_position_;}
  
  private:
    Type type_;
    State state_;
    glm::ivec2 grid_position_;
  
};

} // namespace game
} // namespace king

#endif  // KING_GAME_GEM_H
