//
//  main_menu_ui.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/30/13.
//
//

#ifndef KING_MAIN_MENU_UI_H
#define KING_MAIN_MENU_UI_H

#include <map>
#include <string>
#include "king/gui/gui_screen.h"

namespace king {
namespace core {

class StateContainer;

} // namespace core
} // namespace king

namespace Rocket {
namespace Core {

class ElementDocument;
class Event;

} // namespace Core
} // namespace Rocket

namespace king {
namespace game {

class MainMenuUi: public king::gui::GuiScreen {
  public:
    static const std::string kUiFileName;
  
  private:
    typedef void (MainMenuUi::*ClickHandler)();
    typedef std::map<std::string, ClickHandler> ClickHandlers;
  
  public:
    MainMenuUi(Rocket::Core::ElementDocument* rocket_document);
    virtual ~MainMenuUi();
    virtual void ProcessEvent(Rocket::Core::Event& evt);
    void SetStateContainer(king::core::StateContainer* state_container);
  
  private:
    void StartButtonClicked();
    void QuitButtonClicked();
    
    ClickHandlers click_handlers_;
    king::core::StateContainer* state_container_;
    
};

} // namespace game
} // namespace king

#endif  // KING_MAIN_MENU_UI_H
