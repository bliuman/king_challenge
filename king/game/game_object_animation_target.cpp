//
//  game_object_animation_target.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 29/10/13.
//
//

#include "king/game/game_object_animation_target.h"
#include <assert.h>
#include "king/core/game_object.h"
#include "king/core/frame.h"

namespace king {
namespace game {

GameObjectAnimationTarget::GameObjectAnimationTarget(king::core::GameObject* game_object)
  :game_object_(game_object)
{
  assert(NULL != game_object_);
}

GameObjectAnimationTarget::~GameObjectAnimationTarget()
{
}

void GameObjectAnimationTarget::SetPosition(const glm::vec2 &position)
{
  game_object_->GetFrame()->SetPosition(position);
}

const glm::vec2& GameObjectAnimationTarget::GetPosition() const
{
  return game_object_->GetFrame()->GetPosition();
}

void GameObjectAnimationTarget::SetSize(const glm::vec2 &size)
{
  game_object_->GetFrame()->SetDimension(size);
}

const glm::vec2& GameObjectAnimationTarget::GetSize() const
{
  return game_object_->GetFrame()->GetDimension();
}

} // namespace game
} // namespace king
