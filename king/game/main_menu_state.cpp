//
//  main_menu_state.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/3/13.
//
//

#include "king/game/main_menu_state.h"
#include <assert.h>
#include <Rocket/Core/ElementDocument.h>
#include "king/game/animation_factory.h"
#include "king/game/game_object_factory.h"
#include "king/game/gui_animation_factory.h"
#include "king/game/gui_screen_factory.h"
#include "king/game/main_menu_ui.h"
#include "king/render_state_factory.h"

namespace king {
namespace game {

MainMenuState::MainMenuState(const king::RenderQueue::SharedPtr& render_queue, const king::TextureManager::SharedPtr& texture_manager, const king::animation::AnimationSystem::SharedPtr& animation_system, const king::gui::GuiManager::SharedPtr& gui_manager)
  :king::core::State(render_queue, texture_manager, animation_system, gui_manager)
  ,game_object_factory_(NULL)
  ,animation_factory_(NULL)
  ,gui_animation_factory_(NULL)
  ,background_(NULL)
  ,gui_context_(NULL)
  ,main_menu_ui_(NULL)
{
}

MainMenuState::~MainMenuState()
{
}

void MainMenuState::Activate(king::core::StateContainer *state_container)
{
  king::core::State::Activate(state_container);
  king::RenderStateFactory::SharedPtr render_state_factory = new king::RenderStateFactory(GetTextureManager());
  animation_factory_ = new king::game::AnimationFactory(GetAnimationSystem());
  gui_animation_factory_ = new king::game::GuiAnimationFactory(GetAnimationSystem());
  game_object_factory_ = new king::game::GameObjectFactory(GetTextureManager(), GetRenderQueue(), render_state_factory, animation_factory_);
}

void MainMenuState::Enter()
{
  king::core::State::Enter();
  background_ = game_object_factory_->CreateGameObject("background");
  gui_context_ = GetGuiManager()->CreateContext(king::gui::ContextFactoryParam("main_menu", glm::ivec2(800, 600), new king::game::GuiScreenFactory()));
  main_menu_ui_ = dynamic_cast<king::game::MainMenuUi*>(gui_context_->CreateScreen("main_menu"));
  assert(NULL != main_menu_ui_);
  main_menu_ui_->SetStateContainer(GetStateContainer());
}

void MainMenuState::Pause()
{
  king::core::State::Pause();
  gui_animation_factory_->MainMenuDisappear(main_menu_ui_->GetRocketDocument());
}

void MainMenuState::ProcessEvents(king::event::EventQueue &event_queue)
{
//  gui_context_->Update(event_queue);
  king::core::State::ProcessEvents(event_queue);
}

void MainMenuState::Update(float delta_frame)
{
  king::core::State::Update(delta_frame);
}

void MainMenuState::Resume()
{
  king::core::State::Resume();
  gui_animation_factory_->MainMenuAppear(main_menu_ui_->GetRocketDocument());
}

void MainMenuState::Leave()
{
  king::core::State::Leave();
  game_object_factory_->Destroy(background_);
  gui_context_->DestroyScreen(main_menu_ui_);
  GetGuiManager()->DestroyContext(gui_context_);
}

void MainMenuState::Deactivate()
{
  delete game_object_factory_;
  delete gui_animation_factory_;
  delete animation_factory_;
  king::core::State::Deactivate();
}

} // namespace game
} // namespace king
