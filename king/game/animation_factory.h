//
//  animation_factory.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 29/10/13.
//
//

#ifndef KING_ANIMATION_FACTORY_H
#define KING_ANIMATION_FACTORY_H

#include <glm/glm.hpp>
#include "king/animation/animation.h"
#include "king/animation/animation_system.h"

namespace king {
namespace game {

class Gem;

} // namespace game
} // namespace king

namespace king {
namespace game {

class AnimationFactory {
  public:
    AnimationFactory(const king::animation::AnimationSystem::SharedPtr& animation_system);
    ~AnimationFactory();
    king::animation::Animation* GemDropIn(king::game::Gem* gem, const glm::vec2& from, const glm::vec2& to, float delay);
    king::animation::Animation* GemSwitch(king::game::Gem* lhs, king::game::Gem* rhs, bool success);
    king::animation::Animation* GemDestroy(king::game::Gem* gem, int delay);
    king::animation::Animation* TranslateGem(king::game::Gem* gem, const glm::vec2& from, const glm::vec2& to, float delay);
  
  private:
    king::animation::AnimationSystem::SharedPtr animation_system_;
  
};

} // namespace game
} // namespace king

#endif  // KING_ANIMATION_FACTORY_H
