//
//  game_hud_ui.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/30/13.
//
//

#ifndef KING_GAME_HUD_UI_H
#define KING_GAME_HUD_UI_H

#include "king/gui/gui_screen.h"
#include "king/core/variable.h"
#include "king/core/variable_container.h"

namespace Rocket {
namespace Core {

class ElementDocument;
class Event;

} // namespace Core
} // namespace Rocket

namespace king {
namespace game {

class GameHudUi: public king::gui::GuiScreen {
  public:
    static const std::string kUiFileName;
  
  public:
    GameHudUi(Rocket::Core::ElementDocument* rocket_document);
    virtual ~GameHudUi();
    virtual void ProcessEvent(Rocket::Core::Event& evt);
    void SetVariableContainer(const king::core::VariableContainer::SharedPtr& variable_container);
  
  private:
    void OnTimerUpdate(const void* sender, const float& value);
    void OnScoreUpdate(const void* sender, const int& value);
  
    king::core::Variable<float>::SharedPtr timer_;
    king::core::Variable<int>::SharedPtr score_;
    
};

} // namespace game
} // namespace king

#endif  // KING_GAME_HUD_UI_H
