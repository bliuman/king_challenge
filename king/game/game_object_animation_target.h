//
//  game_object_animation_target.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 29/10/13.
//
//

#ifndef KING_GAME_GAME_OBJECT_ANIMATION_TARGET_H
#define KING_GAME_GAME_OBJECT_ANIMATION_TARGET_H

#include <glm/glm.hpp>
#include "king/animation/animation_target.h"

namespace king {
namespace core {

class GameObject;

} // namespace core
} // namespace king

namespace king {
namespace game {

class GameObjectAnimationTarget: public king::animation::AnimationTarget {
  public:
    GameObjectAnimationTarget(king::core::GameObject* game_object);
    virtual ~GameObjectAnimationTarget();
    virtual void SetPosition(const glm::vec2& position);
    virtual const glm::vec2& GetPosition() const;
    virtual void SetSize(const glm::vec2& size);
    virtual const glm::vec2& GetSize() const;
  
  private:
    king::core::GameObject* game_object_;
  
};

} // namespace game
} // namespace king

#endif  // KING_GAME_GAME_OBJECT_ANIMATION_TARGET_H
