//
//  game_end_ui.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 04/1/13.
//
//

#include "king/game/game_end_ui.h"
#include <assert.h>
#include <Rocket/Core.h>
#include "king/core/state_container.h"

namespace king {
namespace game {

const std::string GameEndUi::kUiFileName = "assets/game_end_ui.rml";

GameEndUi::GameEndUi(Rocket::Core::ElementDocument* rocket_document)
  :king::gui::GuiScreen(rocket_document)
  ,click_handlers_()
  ,state_container_(NULL)
{
  RegisterEvent("replay_btn", "click");
  RegisterEvent("main_menu_btn", "click");
  click_handlers_.insert(ClickHandlers::value_type("replay_btn", &GameEndUi::ReplayButtonClicked));
  click_handlers_.insert(ClickHandlers::value_type("main_menu_btn", &GameEndUi::MainMenuButtonClicked));
}

GameEndUi::~GameEndUi()
{
  UnregisterEvent("replay_btn", "click");
  UnregisterEvent("main_menu_btn", "click");
}

void GameEndUi::ProcessEvent(Rocket::Core::Event &evt)
{
  if (evt.GetType() == "click")
  {
    ClickHandlers::iterator iter = click_handlers_.find(evt.GetCurrentElement()->GetId().CString());
    if (click_handlers_.end() != iter)
    {
      (this->*iter->second)();
    }
  }
}

void GameEndUi::SetStateContainer(king::core::StateContainer *state_container)
{
  assert(NULL != state_container);
  state_container_ = state_container;
}

void GameEndUi::ReplayButtonClicked()
{
  state_container_->PopState("game_end_state");
}

void GameEndUi::MainMenuButtonClicked()
{
  state_container_->PopState("game_state");
}

} // namespace game
} // namespace king
