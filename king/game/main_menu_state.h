//
//  main_menu_state.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/3/13.
//
//

#ifndef KING_GAME_MAIN_MENU_STATE_H
#define KING_GAME_MAIN_MENU_STATE_H

#include "king/animation/animation_system.h"
#include "king/gui/gui_manager.h"
#include "king/render_queue.h"
#include "king/core/state.h"
#include "king/texture_manager.h"

namespace king {
namespace core {

class GameObject;
class StateContainer;

} // namespace core

namespace game {

class AnimationFactory;
class GameObjectFactory;
class GuiAnimationFactory;
class MainMenuUi;

} // namespace game

namespace gui {

class Context;

} // namespace gui

namespace event {

class EventQueue;

} // namespace event
} // namespace king

namespace king {
namespace game {

class MainMenuState: public king::core::State {
  public:
    MainMenuState(const king::RenderQueue::SharedPtr& render_queue, const king::TextureManager::SharedPtr& texture_manager, const king::animation::AnimationSystem::SharedPtr& animation_system, const king::gui::GuiManager::SharedPtr& gui_manager);
    virtual ~MainMenuState();
    virtual void Activate(king::core::StateContainer* state_container);
    virtual void Enter();
    virtual void Pause();
    virtual void ProcessEvents(king::event::EventQueue& event_queue);
    virtual void Update(float delta_frame);
    virtual void Resume();
    virtual void Leave();
    virtual void Deactivate();
  
  private:
    king::game::GameObjectFactory* game_object_factory_;
    king::game::AnimationFactory* animation_factory_;
    king::game::GuiAnimationFactory* gui_animation_factory_;
    king::core::GameObject* background_;
    king::gui::Context* gui_context_;
    king::game::MainMenuUi* main_menu_ui_;
  
};

} // namespace game
} // namespace king

#endif  // KING_GAME_MAIN_MENU_STATE_H
