//
//  gui_screen_factory.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 30/10/13.
//
//

#include "king/game/gui_screen_factory.h"
#include <assert.h>
#include <Rocket/Core/Context.h>
#include "king/game/main_menu_ui.h"
#include "king/game/game_end_ui.h"
#include "king/game/game_hud_ui.h"
#include "king/game/game_pause_ui.h"

namespace king {
namespace game {

GuiScreenFactory::GuiScreenFactory()
{
}

GuiScreenFactory::~GuiScreenFactory()
{
}

king::gui::GuiScreen* GuiScreenFactory::Create(const king::gui::GuiScreen::FactoryParam& param)
{
  Rocket::Core::ElementDocument* rocket_document = NULL;
  if (param.name_ == "main_menu")
  {
    rocket_document = param.rocket_context_->LoadDocument(king::game::MainMenuUi::kUiFileName.c_str());
    return new king::game::MainMenuUi(rocket_document);
  }
  else if (param.name_ == "game_hud")
  {
    rocket_document = param.rocket_context_->LoadDocument(king::game::GameHudUi::kUiFileName.c_str());
    return new king::game::GameHudUi(rocket_document);
  }
  else if (param.name_ == "game_end_menu")
  {
    rocket_document = param.rocket_context_->LoadDocument(king::game::GameEndUi::kUiFileName.c_str());
    return new king::game::GameEndUi(rocket_document);
  }
  else if (param.name_ == "game_pause_menu")
  {
    rocket_document = param.rocket_context_->LoadDocument(king::game::GamePauseUi::kUiFileName.c_str());
    return new king::game::GamePauseUi(rocket_document);
  }
  assert(false);
  return NULL;
}

void GuiScreenFactory::Destroy(king::gui::GuiScreen *screen)
{
  assert(NULL != screen);
  delete screen;
}

} // namespace game
} // namespace king
