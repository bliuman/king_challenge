//
//  animation_factory.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 29/10/13.
//
//

#include "king/game/animation_factory.h"
#include <assert.h>
#include <cpgf/tween/gtween.h>
#include <cpgf/tween/easing/back.h>
#include <cpgf/tween/easing/bounce.h>
#include <cpgf/tween/easing/linear.h>
#include "king/animation/delay.h"
#include "king/animation/delta_size.h"
#include "king/animation/delta_translation.h"
#include "king/animation/group_animation.h"
#include "king/animation/sequence_animation.h"
#include "king/animation/translation.h"
#include "king/game/game_object_animation_target.h"
#include "king/game/gem.h"
#include "king/core/frame.h"

namespace king {
namespace game {

AnimationFactory::AnimationFactory(const king::animation::AnimationSystem::SharedPtr& animation_system)
  :animation_system_(animation_system)
{
  assert(!animation_system_.isNull());
}

AnimationFactory::~AnimationFactory()
{
}

king::animation::Animation* AnimationFactory::GemDropIn(king::game::Gem* gem, const glm::vec2& from ,const glm::vec2& to, float delay)
{
  king::animation::AnimationTarget* target = new king::game::GameObjectAnimationTarget(gem);
  king::animation::Translation* animation = new king::animation::Translation(target, from, to);
  animation->GetInterpolationObject().duration((float)(delay * 0.18f)).ease(cpgf::BounceEase::easeOut());
  animation_system_->AddAnimation("gem_drop_in", animation);
  return animation;
}

king::animation::Animation* AnimationFactory::GemSwitch(king::game::Gem *lhs, king::game::Gem *rhs, bool success)
{
  king::animation::SequnceAnimation* sequence = new king::animation::SequnceAnimation();
  king::animation::GroupAnimation* group = new king::animation::GroupAnimation();
  group->AddAnimation(TranslateGem(lhs, lhs->GetFrame()->GetPosition(), rhs->GetFrame()->GetPosition(), 0.12f));
  group->AddAnimation(TranslateGem(rhs, rhs->GetFrame()->GetPosition(), lhs->GetFrame()->GetPosition(), 0.12f));
  sequence->AddAnimation(group);
  if (!success)
  {
    group = new king::animation::GroupAnimation();
    group->AddAnimation(TranslateGem(lhs, rhs->GetFrame()->GetPosition(), lhs->GetFrame()->GetPosition(), 0.12f));
    group->AddAnimation(TranslateGem(rhs, lhs->GetFrame()->GetPosition(), rhs->GetFrame()->GetPosition(), 0.12f));
    sequence->AddAnimation(group);
  }
  animation_system_->AddAnimation("gem_switch", sequence);
  return sequence;
}

king::animation::Animation* AnimationFactory::GemDestroy(king::game::Gem* gem, int delay)
{
  king::animation::AnimationTarget::SharedPtr target = new king::game::GameObjectAnimationTarget(gem);
  const glm::vec2& size = target->GetSize();
  
  king::animation::SequnceAnimation* sequence = new king::animation::SequnceAnimation();
  king::animation::BaseAnimation* animation = NULL;
  for (int i = 0; i < delay; ++i)
  {
    animation = new king::animation::Delay(target);
    animation->GetInterpolationObject().duration(0.01f).ease(cpgf::LinearEase::easeOut());
    sequence->AddAnimation(animation);
  }
  
  king::animation::GroupAnimation* group = new king::animation::GroupAnimation();
  animation = new king::animation::DeltaSize(target, glm::vec2(size.x * -1, size.y * -1));
  animation->GetInterpolationObject().duration(0.2f).ease(cpgf::BackEase::easeIn());
  group->AddAnimation(animation);
  
  animation = new king::animation::DeltaTranslation(target, glm::vec2(size.x * 0.5f, size.y * 0.5f));
  animation->GetInterpolationObject().duration(0.2f).ease(cpgf::BackEase::easeIn());
  group->AddAnimation(animation);
  sequence->AddAnimation(group);
  
  animation_system_->AddAnimation("gem_destroy", sequence);
  return sequence;
}

king::animation::Animation* AnimationFactory::TranslateGem(king::game::Gem *gem, const glm::vec2 &from, const glm::vec2 &to, float delay)
{
  king::animation::AnimationTarget* target = new king::game::GameObjectAnimationTarget(gem);
  king::animation::Translation* animation = new king::animation::Translation(target, from, to);
  animation->GetInterpolationObject().duration(delay).ease(cpgf::LinearEase::easeIn());
  return animation;
}

} // namespace game
} // namespace king
