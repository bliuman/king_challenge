//
//  render_state_factory.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#ifndef KING_RENDER_STATE_FACTORY_H
#define KING_RENDER_STATE_FACTORY_H

#include <map>
#include <string>
#include <Poco/SharedPtr.h>
#include "king/texture_manager.h"

namespace king {

class RenderState;

} // namespace king

namespace king {

class RenderStateFactory {
  public:
    typedef Poco::SharedPtr<RenderStateFactory> SharedPtr;
  
  private:
    typedef king::RenderState* (RenderStateFactory::*FactoryFunction)();
    typedef std::map<std::string, FactoryFunction> FactoryFunctionMap;
  
  public:
    RenderStateFactory(const king::TextureManager::SharedPtr& texture_manager);
    ~RenderStateFactory();
    king::RenderState* Create(const std::string& name);
  
  private:
    king::RenderState* BackgroundDefault();
    king::RenderState* GridDefault();
    king::RenderState* GemDefault(const std::string& texture_name);
    king::RenderState* BlueGemDefault();
    king::RenderState* GreenGemDefault();
    king::RenderState* PurpleGemDefault();
    king::RenderState* RedGemDefault();
    king::RenderState* YellowGemDefault();
    king::RenderState* GemSelect(const std::string& texture_name);
    king::RenderState* BlueGemSelect();
    king::RenderState* GreenGemSelect();
    king::RenderState* PurpleGemSelect();
    king::RenderState* RedGemSelect();
    king::RenderState* YellowGemSelect();
    king::RenderState* MainMenuContextDefault();
    king::RenderState* GameMenuContextDefault();
    king::RenderState* GameEndMenuContextDefault();
    king::RenderState* GamePauseMenuContextDefault();
  
    king::TextureManager::SharedPtr texture_manager_;
    FactoryFunctionMap function_map_;
  
};

} // namespace king

#endif  // KING_RENDER_STATE_FACTORY_H
