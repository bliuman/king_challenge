//
//  render_batch_factory.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/26/13.
//
//

#include "king/render_batch_factory.h"
#include <assert.h>
#include <cstddef>
#include "king/blend_mode_switch.h"
#include "king/render_batch.h"
#include "king/render_state.h"
#include "king/render_state_switch.h"

namespace king {

RenderBatchFactory::RenderBatchFactory(const king::RenderOperationFactory::SharedPtr& render_operation_factory)
  :render_operation_factory_(render_operation_factory)
{
  assert(!render_operation_factory_.isNull());
}

RenderBatchFactory::~RenderBatchFactory()
{
}

king::RenderBatch* RenderBatchFactory::CreateRenderBatch(king::RenderQueue* queue, king::RenderState *state)
{
  king::BaseRenderOperation::TextureList textures;
  const king::RenderState::TextureList texture_names = state->GetTextures();
  for (king::RenderState::TextureList::const_iterator iter = texture_names.begin(); texture_names.end() != iter; ++iter)
  {
    textures.push_back((*iter)->GetTextureUnit());
  }
  
  king::BaseRenderOperation::RenderStateSwitchList render_state_switches;
  render_state_switches.push_back(new king::BlendModeSwitch(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
  
  return new king::RenderBatch(queue, render_operation_factory_->Create(state->GetRenderOperationName()), textures, render_state_switches, state->GetDepth());
}

} // namespace king
