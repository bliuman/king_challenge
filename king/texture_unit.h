//
//  texture_unit.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#ifndef KING_TEXTURE_H
#define KING_TEXTURE_H

#include <string>
#include <Poco/SharedPtr.h>
#include "king/opengl.h"

namespace king {

class TextureUnit {
  public:
    typedef Poco::SharedPtr<TextureUnit> SharedPtr;
  
  public:
    TextureUnit(const std::string& texture_unit_path);
    ~TextureUnit();
    int GetTextureUnitId() const {return texture_unit_id_;}
    int GetOriginalWidth() const {return original_width_;}
    int GetOriginalHeight() const {return original_height_;}
    int GetWidth() const {return width_;}
    int GetHeight() const {return height_;}
    int GetChannelCount() const {return channel_count_;}
  
  private:
    int GetNextPowerOfTwo(int dimension) const;
  
    GLuint texture_unit_id_;
    int original_width_;
    int original_height_;
    int width_;
    int height_;
    int channel_count_;
  
};

} // namespace king

#endif  // KING_TEXTURE_H
