//
//  gpu_program_uniforms_binder.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#include "king/gpu_program_uniforms_binder.h"
#include <assert.h>
#include "king/gpu_program_param_info.h"

namespace king {

GPUProgramUniformsBinder::GPUProgramUniformsBinder(const king::GPUProgram::SharedPtr& gpu_program, const king::UniformParamsContainer::SharedPtr& uniform_params_container)
  :uniforms_()
{
  assert(!gpu_program.isNull());
  assert(!uniform_params_container.isNull());
  const king::GPUProgram::Parameters uniform_mapping = gpu_program->GetUnitforms().GetParameters();
  for (king::GPUProgram::Parameters::const_iterator iter = uniform_mapping.begin(); uniform_mapping.end() != iter; ++iter)
  {
    king::BaseUniformParameter::SharedPtr uniform_value = uniform_params_container->Get(iter->first);
    uniforms_.push_back(UniformLocationPair(uniform_value, iter->second));
  }
}

GPUProgramUniformsBinder::~GPUProgramUniformsBinder()
{
}

void GPUProgramUniformsBinder::Bind() const
{
  for (UniformsList::const_iterator iter = uniforms_.begin(); uniforms_.end() != iter; ++iter)
  {
    iter->first->Bind(iter->second);
  }
}

} // namespace king
