//
//  gpu_program_uniforms_binder.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#ifndef KING_GPU_PROGRAM_UNIFORMS_BINDER_H
#define KING_GPU_PROGRAM_UNIFORMS_BINDER_H

#include <vector>
#include "king/base_uniform_parameter.h"
#include "king/gpu_program.h"
#include "king/uniform_params_container.h"

namespace king {

class GPUProgramUniformsBinder {
  public:
    typedef std::pair<king::BaseUniformParameter::SharedPtr, GLuint> UniformLocationPair;
    typedef std::vector<UniformLocationPair> UniformsList;
  
  public:
    GPUProgramUniformsBinder(const king::GPUProgram::SharedPtr& gpu_program, const king::UniformParamsContainer::SharedPtr& uniform_params_container);
    ~GPUProgramUniformsBinder();
    void Bind() const;
  
  private:
    UniformsList uniforms_;
  
};

} // namespace king

#endif  // KING_GPU_PROGRAM_UNIFORMS_BINDER_H
