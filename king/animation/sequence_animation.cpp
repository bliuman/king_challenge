//
//  sequence_animation.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/30/13.
//
//

#include "king/animation/sequence_animation.h"

#include <assert.h>

namespace king {
namespace animation {

SequnceAnimation::SequnceAnimation()
  :child_animations_()
{
}

SequnceAnimation::~SequnceAnimation()
{
  while (!child_animations_.empty())
  {
    delete child_animations_.front();
    child_animations_.pop();
  }
}

void SequnceAnimation::Update(float delta_time)
{
  if (child_animations_.empty())
  {
    SetState(king::animation::Animation::kFinished);
    return;
  }
  
  child_animations_.front()->Update(delta_time);
  if (king::animation::Animation::kFinished == child_animations_.front()->GetState())
  {
    delete child_animations_.front();
    child_animations_.pop();
  }
}

void SequnceAnimation::AddAnimation(king::animation::Animation *animation)
{
  assert(NULL != animation);
  child_animations_.push(animation);
  animation->Start();
}

} // namespace animation
} // namespace king
