//
//  group_animation.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/29/13.
//
//

#ifndef KING_ANIMATION_GROUP_ANIMATION_H
#define KING_ANIMATION_GROUP_ANIMATION_H

#include <vector>
#include "king/animation/animation.h"

namespace king {
namespace animation {

class GroupAnimation: public king::animation::Animation {
  private:
    typedef std::vector<king::animation::Animation*> AnimationList;
  
  public:
    GroupAnimation();
    virtual ~GroupAnimation();
    virtual void Update(float delta_time);
    void AddAnimation(king::animation::Animation* animation);
  
  private:
    AnimationList child_animations_;
    
};

} // namespace animation
} // namespace king

#endif  // KING_ANIMATION_GROUP_ANIMATION_H
