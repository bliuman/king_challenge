//
//  delay.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/5/13.
//
//

#ifndef KING_ANIMATION_DELAY_H
#define KING_ANIMATION_DELAY_H

#include "king/animation/base_animation.h"
#include "king/animation/animation_target.h"

namespace king {
namespace animation {

class Delay: public king::animation::BaseAnimation {
  public:
    Delay(const king::animation::AnimationTarget::SharedPtr& animation_target): king::animation::BaseAnimation(animation_target) {}
    virtual ~Delay() {}
  
  protected:
    virtual void UpdateAnimation(float interpolation_value) {}
  
};


} // namespace animation
} // namespace king

#endif  // KING_ANIMATION_DELAY_H
