//
//  delta_size.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 04/11/13.
//
//

#include "king/animation/delta_size.h"

namespace king {
namespace animation {

DeltaSize::DeltaSize(const king::animation::AnimationTarget::SharedPtr& animation_target, const glm::vec2& delta)
  :king::animation::BaseAnimation(animation_target)
  ,start_(.0f, .0f)
  ,end_(.0f, .0f)
{
  start_ = animation_target->GetSize();
  end_ = start_ + delta;
}

DeltaSize::~DeltaSize()
{
}

void DeltaSize::UpdateAnimation(float interpolation_value)
{
  const glm::vec2 size = start_ + (end_ - start_) * interpolation_value;
  GetTarget()->SetSize(size);
}

} // namespace animation
} // namespace king
