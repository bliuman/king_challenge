//
//  base_animation.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/29/13.
//
//

#ifndef KING_ANIMATION_BASE_ANIMATION_H
#define KING_ANIMATION_BASE_ANIMATION_H

#include "king/animation/animation.h"
#include "king/animation/animation_target.h"
#include <cpgf/tween/gtween.h>

namespace king {
namespace animation {

class BaseAnimation: public king::animation::Animation {
  public:
    virtual ~BaseAnimation();
    virtual void Update(float delta_time);
    virtual void OnInitialize();
    virtual void OnDestroy();
    virtual void OnComplete();
    virtual void OnRepeat();
    cpgf::GTween& GetInterpolationObject() {return tween_;}
  
  protected:
    BaseAnimation(const king::animation::AnimationTarget::SharedPtr& animation_target);
    virtual void UpdateAnimation(float interpolation_value) = 0;
    king::animation::AnimationTarget::SharedPtr& GetTarget() {return animation_target_;}
  
  private:
    cpgf::GTween tween_;
    king::animation::AnimationTarget::SharedPtr animation_target_;
  
};

} // namespace animation
} // namespace king

#endif  // KING_ANIMATION_BASE_ANIMATION_H
