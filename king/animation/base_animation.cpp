//
//  base_animation.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/29/13.
//
//

#include "king/animation/base_animation.h"
#include <assert.h>
#include <cpgf/tween/gtweenlist.h>
#include <cpgf/accessor/gaccessor.h>
#include <iostream>

namespace king {
namespace animation {

BaseAnimation::BaseAnimation(const king::animation::AnimationTarget::SharedPtr& animation_target)
  :tween_()
  ,animation_target_(animation_target)
{
  assert(!animation_target_.isNull());
  tween_.target(cpgf::createAccessor(this, 0, &BaseAnimation::UpdateAnimation), 0.0f, 1.0f)
    .onInitialize(cpgf::GTweenCallback(this, &BaseAnimation::OnInitialize))
    .onDestroy(cpgf::GTweenCallback(this, &BaseAnimation::OnDestroy))
    .onComplete(cpgf::GTweenCallback(this, &BaseAnimation::OnComplete))
    .onRepeat(cpgf::GTweenCallback(this, &BaseAnimation::OnRepeat));
}

BaseAnimation::~BaseAnimation()
{
}

void BaseAnimation::Update(float delta_time)
{
  tween_.tick(delta_time);
}

void BaseAnimation::OnInitialize()
{
}

void BaseAnimation::OnDestroy()
{
}

void BaseAnimation::OnComplete()
{
    SetState(king::animation::Animation::kFinished);
}

void BaseAnimation::OnRepeat()
{
}

} // namespace animation
} // namespace king
