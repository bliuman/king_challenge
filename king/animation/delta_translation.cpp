//
//  delta_translation.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 29/10/13.
//
//

#include "king/animation/delta_translation.h"

namespace king {
namespace animation {

DeltaTranslation::DeltaTranslation(const king::animation::AnimationTarget::SharedPtr& animation_target, const glm::vec2& delta_position)
  :king::animation::BaseAnimation(animation_target)
  ,start_(.0f, .0f)
  ,end_(.0f, .0f)
  ,delta_(delta_position)
{
  start_ = GetTarget()->GetPosition();
  end_ = start_ + delta_;
}

DeltaTranslation::~DeltaTranslation()
{
}

void DeltaTranslation::UpdateAnimation(float interpolation_value)
{
  const glm::vec2 position = start_ + (end_ - start_) * interpolation_value;
  GetTarget()->SetPosition(position);
}

} // namespace animation
} // namespace king
