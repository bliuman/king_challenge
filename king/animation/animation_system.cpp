//
//  animation_system.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/29/13.
//
//

#include "king/animation/animation_system.h"
#include <assert.h>
#include "king/animation/animation.h"

namespace king {
namespace animation {

AnimationSystem::AnimationSystem()
  :animations_()
{
}

AnimationSystem::~AnimationSystem()
{
  for (AnimationList::const_iterator iter = animations_.begin(); animations_.end() != iter; ++iter)
  {
    delete iter->first;
  }
  animations_.clear();
}

void AnimationSystem::AddAnimation(const std::string &name, king::animation::Animation *animation)
{
  assert(NULL != animation);
  animations_.push_back(AnimationEntry(animation, name));
  animation->Start();
}

void AnimationSystem::Update(float delta_time)
{
  king::animation::Animation* animation = NULL;
  for (AnimationList::iterator iter = animations_.begin(); animations_.end() != iter;)
  {
    animation = iter->first;
    if (king::animation::Animation::kFinished == animation->GetState())
    {
      delete animation;
      iter = animations_.erase(iter);
    }
    else
    {
      animation->Update(delta_time);
      ++iter;
    }
  }
}

} // namespace animation
} // namespace king
