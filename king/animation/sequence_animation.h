//
//  sequence_animation.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/30/13.
//
//

#ifndef KING_SEQUENCE_ANIMATION_H_
#define KING_SEQUENCE_ANIMATION_H_

#include <queue>
#include "king/animation/animation.h"

namespace king {
namespace animation {

class SequnceAnimation: public king::animation::Animation {
  private:
    typedef std::queue<king::animation::Animation*> AnimationList;
  
  public:
    SequnceAnimation();
    virtual ~SequnceAnimation();
    virtual void Update(float delta_time);
    void AddAnimation(king::animation::Animation* animation);
  
  private:
    AnimationList child_animations_;
    
};

} // namespace animation
} // namespace king

#endif  // KING_SEQUENCE_ANIMATION_H_
