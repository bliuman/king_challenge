//
//  animation_system.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/29/13.
//
//

#ifndef KING_ANIMATION_ANIMATION_SYSTEM_H
#define KING_ANIMATION_ANIMATION_SYSTEM_H

#include <string>
#include <vector>
#include <Poco/SharedPtr.h>

namespace king {
namespace animation {

class Animation;

} // namespace animation
} // namespace king

namespace king {
namespace animation {

class AnimationSystem {
  private:
    typedef std::pair<king::animation::Animation*, std::string> AnimationEntry;
    typedef std::vector<AnimationEntry> AnimationList;
  
  public:
    typedef Poco::SharedPtr<AnimationSystem> SharedPtr;
  
  public:
    AnimationSystem();
    ~AnimationSystem();
    void AddAnimation(const std::string& name, king::animation::Animation* animation);
    void Update(float delta_time);
    bool IsAnimationsRunning() const {return !animations_.empty();}
  
  private:
    AnimationList animations_;
    
};

} // namespace animation
} // namespace king

#endif  // KING_ANIMATION_ANIMATION_SYSTEM_H
