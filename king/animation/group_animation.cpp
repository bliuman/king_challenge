//
//  group_animation.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/29/13.
//
//

#include "king/animation/group_animation.h"
#include <assert.h>

namespace king {
namespace animation {

GroupAnimation::GroupAnimation()
  :child_animations_()
{
}

GroupAnimation::~GroupAnimation()
{
  for (AnimationList::const_iterator iter = child_animations_.begin(); child_animations_.end() != iter; ++iter)
  {
    delete (*iter);
  }
  child_animations_.clear();
}

void GroupAnimation::Update(float delta_time)
{
  if (child_animations_.empty())
  {
    SetState(king::animation::Animation::kFinished);
    return;
  }
  
  for (AnimationList::iterator iter = child_animations_.begin(); child_animations_.end() != iter;)
  {
    (*iter)->Update(delta_time);
    if (king::animation::Animation::kFinished == (*iter)->GetState())
    {
      delete (*iter);
      iter = child_animations_.erase(iter);
    }
    else
    {
      ++iter;
    }
  }
}

void GroupAnimation::AddAnimation(king::animation::Animation *animation)
{
  assert(NULL != animation);
  child_animations_.push_back(animation);
  animation->Start();
}

} // namespace animation
} // namespace king
