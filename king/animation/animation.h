//
//  animation.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 29/10/13.
//
//

#ifndef KING_ANIMATION_ANIMATION_H
#define KING_ANIMATION_ANIMATION_H

namespace king {
namespace animation {

class Animation {
  public:
    enum State {
      kNotStarted = 0,
      kRunning,
      kFinished
    };
  
  public:
    virtual ~Animation() {}
    virtual void Start() {state_ = kRunning;}
    virtual void Update(float delta_time) = 0;
    virtual void Stop() {state_ = kFinished;}
    State GetState() const {return state_;}
    void SetState(State state) {state_ = state;}
  
  protected:
    Animation() {}
  
  private:
    State state_;
  
};

} // namespace animation
} // namespace king

#endif  // KING_ANIMATION_ANIMATION_H
