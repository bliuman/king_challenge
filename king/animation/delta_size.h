//
//  delta_size.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 04/11/13.
//
//

#ifndef KING_ANIMATION_DELTA_SIZE_H
#define KING_ANIMATION_DELTA_SIZE_H

#include "king/animation/base_animation.h"
#include <glm/glm.hpp>
#include "king/animation/animation_target.h"

namespace king {
namespace animation {

class DeltaSize: public king::animation::BaseAnimation {
  public:
    DeltaSize(const king::animation::AnimationTarget::SharedPtr& animation_target, const glm::vec2& delta);
    virtual ~DeltaSize();
  
  protected:
    virtual void UpdateAnimation(float interpolation_value);
  
  private:
    glm::vec2 start_;
    glm::vec2 end_;
  
};

} // namespace animation
} // namespace king

#endif  // KING_ANIMATION_DELTA_SIZE_H
