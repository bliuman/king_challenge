//
//  translation.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 29/10/13.
//
//

#include "king/animation/translation.h"

namespace king {
namespace animation {

Translation::Translation(const king::animation::AnimationTarget::SharedPtr& animation_target, const glm::vec2& start_position, const glm::vec2& end_position)
  :king::animation::BaseAnimation(animation_target)
  ,start_(start_position)
  ,end_(end_position)
{
}

Translation::~Translation()
{
}

void Translation::UpdateAnimation(float interpolation_value)
{
  const glm::vec2 position = start_ + (end_ - start_) * interpolation_value;
  GetTarget()->SetPosition(position);
}

} // namespace animation
} // namespace king
