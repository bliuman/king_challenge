//
//  animation_target.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 29/10/13.
//
//

#ifndef KING_ANIMATION_ANIMATION_TARGET_H
#define KING_ANIMATION_ANIMATION_TARGET_H

#include <glm/glm.hpp>
#include <Poco/SharedPtr.h>

namespace king {
namespace animation {

class AnimationTarget {
  public:
    typedef Poco::SharedPtr<AnimationTarget> SharedPtr;
  
  public:
    virtual ~AnimationTarget() {}
    virtual void SetPosition(const glm::vec2& position) = 0;
    virtual const glm::vec2& GetPosition() const = 0;
    virtual void SetSize(const glm::vec2& size) = 0;
    virtual const glm::vec2& GetSize() const = 0;
  
  protected:
    AnimationTarget() {}
  
};

} // namespace animation
} // namespace king

#endif  // KING_ANIMATION_ANIMATION_TARGET_H
