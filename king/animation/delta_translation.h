//
//  delta_translation.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 29/10/13.
//
//

#ifndef KING_ANIMATION_DELTA_TRANSLATION_H
#define KING_ANIMATION_DELTA_TRANSLATION_H

#include "king/animation/animation_target.h"
#include "king/animation/base_animation.h"

namespace king {
namespace animation {

class DeltaTranslation: public king::animation::BaseAnimation {
  public:
    DeltaTranslation(const king::animation::AnimationTarget::SharedPtr& animation_target, const glm::vec2& delta_position);
    virtual ~DeltaTranslation();
  
  protected:
    virtual void UpdateAnimation(float interpolation_value);
  
  private:
    glm::vec2 start_;
    glm::vec2 end_;
    glm::vec2 delta_;
  
};

} // namespace animation
} // namespace king

#endif  // KING_ANIMATION_DELTA_TRANSLATION_H
