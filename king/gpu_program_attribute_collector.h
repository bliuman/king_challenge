//
//  gpu_program_attribute_collector.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/24/13.
//
//

#ifndef GPU_PROGRAM_ATTRIBUTE_COLLECTOR_H
#define GPU_PROGRAM_ATTRIBUTE_COLLECTOR_H

#include "king/opengl.h"

namespace Poco {

template<typename DataType>
class Buffer;

} // namespace Poco

namespace king {

class GPUProgramAttributeCollector {
  public:
    GPUProgramAttributeCollector();
    ~GPUProgramAttributeCollector();
    void GetTypes(GLenum& parameter, GLenum& buffer_length) const;
    GLint Collect(GLuint gpu_prgram_id_, GLint index, GLsizei buffer_size, GLint* size, GLenum* type, Poco::Buffer<char>& name_buffer) const;
  
};

} // namespace king

#endif  // GPU_PROGRAM_ATTRIBUTE_COLLECTOR_H
