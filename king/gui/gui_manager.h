//
//  gui_manager.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 30/10/13.
//
//

#ifndef KING_GUI_MANAGER_H
#define KING_GUI_MANAGER_H

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <Poco/SharedPtr.h>
#include "king/core/factory.h"
#include "king/gui/context.h"

namespace Rocket {
namespace Core {

class SystemInterface;
class RenderInterface;
class Context;

} // namespace Core
} // namespace Rocket

namespace king {
namespace event {

class EventQueue;

} // namespace event

namespace gui {

class GuiScreenFactory;

} // namespace gui
} // namespace king

namespace king {
namespace gui {

class GuiManager {
  public:
    typedef Poco::SharedPtr<GuiManager> SharedPtr;
  
  private:
    typedef std::vector<king::gui::Context*> ContextList;
    
  public:
    GuiManager(Rocket::Core::SystemInterface* system_interface, Rocket::Core::RenderInterface* render_interface, const king::gui::Context::Factory::SharedPtr& context_factory);
    ~GuiManager();
    king::gui::Context* CreateContext(const king::gui::ContextFactoryParam& param);
    void DestroyContext(king::gui::Context* context);
    void Update(king::event::EventQueue& event_queue);
  
  public:
    Rocket::Core::SystemInterface* system_interface_;
    Rocket::Core::RenderInterface* render_interface_;
    king::gui::Context::Factory::SharedPtr context_factory_;
    ContextList contexts_;
  
};

} // namespace gui
} // namespace king

#endif  // KING_GUI_MANAGER_H
