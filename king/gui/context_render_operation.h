//
//  context_render_operation.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/3/13.
//
//

#ifndef KING_GUI_CONTEXT_RENDER_OPERATION_H
#define KING_GUI_CONTEXT_RENDER_OPERATION_H

#include <vector>
#include "king/base_render_operation.h"

namespace king {
namespace gui {

class Context;

} // namespace gui
} // namespace gui

namespace king {
namespace gui {

class ContextRenderOperation: public king::BaseRenderOperation {
  private:
    typedef std::vector<king::gui::Context*> GuiContextList;

  public:
    ContextRenderOperation();
    virtual ~ContextRenderOperation();
    virtual void Begin(const king::BaseRenderOperation::TextureList& textures, king::BaseRenderOperation::RenderStateSwitchList& render_state_switches) {}
    virtual void Update(const king::BaseRenderOperation::RenderList& render_objects);
    virtual void Render();
    virtual void End(const king::BaseRenderOperation::TextureList& textures, king::BaseRenderOperation::RenderStateSwitchList& render_state_switches) {}
  
  private:
    GuiContextList gui_contexts_;
    
};

} // namespace gui
} // namespace king

#endif  // KING_GUI_CONTEXT_RENDER_OPERATION_H
