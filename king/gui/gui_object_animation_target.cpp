//
//  gui_object_animation_target.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 03/11/13.
//
//

#include "king/gui/gui_object_animation_target.h"
#include <assert.h>
#include <Rocket/Core.h>

namespace king {
namespace gui {

GuiObjectAnimationTarget::GuiObjectAnimationTarget(Rocket::Core::Element* gui_object)
  :gui_object_(gui_object)
{
  assert(NULL != gui_object_);
}

GuiObjectAnimationTarget::~GuiObjectAnimationTarget()
{
}

void GuiObjectAnimationTarget::SetPosition(const glm::vec2 &position)
{
  gui_object_->SetProperty("left", Rocket::Core::Property(position.x, Rocket::Core::Property::PX));
  gui_object_->SetProperty("top", Rocket::Core::Property(position.y, Rocket::Core::Property::PX));
}

const glm::vec2& GuiObjectAnimationTarget::GetPosition() const
{
  position_cache_ = glm::vec2(gui_object_->GetProperty<int>("left"), gui_object_->GetProperty<int>("top"));
  return position_cache_;
}

void GuiObjectAnimationTarget::SetSize(const glm::vec2 &size)
{
  size_cache_ = size;
  assert(false); // not supported yet
}

const glm::vec2& GuiObjectAnimationTarget::GetSize() const
{
  assert(false); // not supported yet
  return size_cache_;
}

} // namespace gui
} // namespace king
