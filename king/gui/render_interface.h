//
//  render_interface.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 30/10/13.
//
//

#ifndef KING_RENDER_INTERFACE_H
#define KING_RENDER_INTERFACE_H

#include <Rocket/Core.h>
#include "king/texture.h"
#include "king/texture_manager.h"

namespace king {
namespace gui {

class RenderInterface: public Rocket::Core::RenderInterface {
  public:
    RenderInterface(const king::TextureManager::SharedPtr& texture_manager);
    virtual ~RenderInterface();
    virtual void RenderGeometry(Rocket::Core::Vertex* vertices, int num_vertices, int* indices, int num_indices, Rocket::Core::TextureHandle texture, const Rocket::Core::Vector2f& translation);
    virtual Rocket::Core::CompiledGeometryHandle CompileGeometry(Rocket::Core::Vertex* vertices, int num_vertices, int* indices, int num_indices, Rocket::Core::TextureHandle texture);
    virtual void RenderCompiledGeometry(Rocket::Core::CompiledGeometryHandle geometry, const Rocket::Core::Vector2f& translation);
    virtual void ReleaseCompiledGeometry(Rocket::Core::CompiledGeometryHandle geometry);
    virtual void EnableScissorRegion(bool enable);
    virtual void SetScissorRegion(int x, int y, int width, int height);
    virtual bool LoadTexture(Rocket::Core::TextureHandle& texture_handle, Rocket::Core::Vector2i& texture_dimensions, const Rocket::Core::String& source);
    virtual bool GenerateTexture(Rocket::Core::TextureHandle& texture_handle, const Rocket::Core::byte* source, const Rocket::Core::Vector2i& source_dimensions);
    virtual void ReleaseTexture(Rocket::Core::TextureHandle texture);
  
  private:
    king::TextureManager::SharedPtr texture_manager_;
  
    
};

} // namespace gui
} // namespace king

#endif  // KING_RENDER_INTERFACE_H
