//
//  context_render_operation.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 11/3/13.
//
//

#include "king/gui/context_render_operation.h"
#include <assert.h>
#include "king/gui/context.h"

namespace king {
namespace gui {

ContextRenderOperation::ContextRenderOperation()
  :gui_contexts_()
{
}

ContextRenderOperation::~ContextRenderOperation()
{
}

void ContextRenderOperation::Update(const king::BaseRenderOperation::RenderList &render_objects)
{
  gui_contexts_.clear();
  gui_contexts_.reserve(render_objects.size());
  king::gui::Context* context = NULL;
  for (king::BaseRenderOperation::RenderList::const_iterator iter = render_objects.begin(); render_objects.end() != iter; ++iter)
  {
    context = dynamic_cast<king::gui::Context*>((*iter));
    assert(NULL != context);
    gui_contexts_.push_back(context);
  }
}

void ContextRenderOperation::Render()
{
  for (GuiContextList::const_iterator iter = gui_contexts_.begin(); gui_contexts_.end() != iter; ++iter)
  {
    (*iter)->Render();
  }
}

} // namespace gui
} // namespace king
