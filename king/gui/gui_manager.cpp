//
//  gui_manager.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 30/10/13.
//
//

#include "king/gui/gui_manager.h"
#include <assert.h>
#include <Rocket/Core.h>

namespace king {
namespace gui {

GuiManager::GuiManager(Rocket::Core::SystemInterface* system_interface, Rocket::Core::RenderInterface* render_interface, const king::gui::Context::Factory::SharedPtr& context_factory)
  :system_interface_(system_interface)
  ,render_interface_(render_interface)
  ,context_factory_(context_factory)
  ,contexts_()
{
  assert(NULL != system_interface_);
  assert(NULL != render_interface_);
  assert(!context_factory_.isNull());
  Rocket::Core::SetSystemInterface(system_interface_);
  Rocket::Core::SetRenderInterface(render_interface_);
  Rocket::Core::Initialise();
  Rocket::Core::FontDatabase::LoadFontFace("assets/Delicious-Bold.otf");
  Rocket::Core::FontDatabase::LoadFontFace("assets/Delicious-BoldItalic.otf");
  Rocket::Core::FontDatabase::LoadFontFace("assets/Delicious-Italic.otf");
  Rocket::Core::FontDatabase::LoadFontFace("assets/Delicious-Roman.otf");
}

GuiManager::~GuiManager()
{
  assert(!contexts_.size());
}

king::gui::Context* GuiManager::CreateContext(const king::gui::ContextFactoryParam& param)
{
  contexts_.push_back(context_factory_->Create(param));
  assert(NULL != contexts_.back());
  return contexts_.back();
}

void GuiManager::DestroyContext(king::gui::Context* context)
{
  assert(NULL != context);
  ContextList::iterator iter = std::find(contexts_.begin(), contexts_.end(), context);
  assert(contexts_.end() != iter);
  contexts_.erase(iter);
  context_factory_->Destroy(context);
}

void GuiManager::Update(king::event::EventQueue& event_queue)
{
  for (ContextList::const_iterator iter = contexts_.begin(); contexts_.end() != iter; ++iter)
  {
    king::gui::Context* context = (*iter);
    context->Update(event_queue);
  }
}

} // namespace gui
} // namespace king
