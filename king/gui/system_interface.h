//
//  system_interface.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 30/10/13.
//
//

#ifndef KING_SYSTEM_INTERFACE_H
#define KING_SYSTEM_INTERFACE_H

#include <Rocket/Core.h>

namespace king {
namespace gui {

class SystemInterface: public Rocket::Core::SystemInterface {
  public:
    SystemInterface(): time_(.0f) {}
    virtual ~SystemInterface() {}
    virtual float GetElapsedTime() {return time_;}
    void IncreaseElapsedTime(float delta_time) {time_ += delta_time;}
    
  private:
    float time_;
    
};

} // namespace gui
} // namespace king

#endif  // KING_SYSTEM_INTERFACE_H
