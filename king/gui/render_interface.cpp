//
//  render_interface.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 30/10/13.
//
//

#include "king/gui/render_interface.h"
#include <assert.h>
#include "king/opengl.h"
#include "king/texture_unit.h"

namespace king {
namespace gui {

RenderInterface::RenderInterface(const king::TextureManager::SharedPtr& texture_manager)
  :texture_manager_(texture_manager)
{
  assert(!texture_manager_.isNull());
}

RenderInterface::~RenderInterface()
{
}

void RenderInterface::RenderGeometry(Rocket::Core::Vertex* vertices, int num_vertices, int* indices, int num_indices, Rocket::Core::TextureHandle texture, const Rocket::Core::Vector2f& translation)
{
  glUseProgram(0);
  glPushMatrix();
  glTranslatef(translation.x, translation.y, 0);
  GLint blend_src = GL_SRC_ALPHA;
  GLint blend_dst = GL_ONE_MINUS_SRC_ALPHA;
  glGetIntegerv(GL_BLEND_SRC, &blend_src);
  glGetIntegerv(GL_BLEND_DST, &blend_dst);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  GLint texture_id = texture;
  if (texture_id > 0)
  {
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_id);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(2, GL_FLOAT, sizeof(Rocket::Core::Vertex), &vertices[0].tex_coord[0]);
  }
  else
  {
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  }
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glVertexPointer(2, GL_FLOAT, sizeof(Rocket::Core::Vertex), &vertices[0].position[0]);
  glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(Rocket::Core::Vertex), &vertices[0].colour[0]);
  glDrawElements(GL_TRIANGLES, num_indices, GL_UNSIGNED_INT, indices);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
  glPopMatrix();
}

Rocket::Core::CompiledGeometryHandle RenderInterface::CompileGeometry(Rocket::Core::Vertex* vertices, int num_vertices, int* indices, int num_indices, Rocket::Core::TextureHandle texture)
{
  return NULL;
}

void RenderInterface::RenderCompiledGeometry(Rocket::Core::CompiledGeometryHandle geometry, const Rocket::Core::Vector2f& translation)
{
}

void RenderInterface::ReleaseCompiledGeometry(Rocket::Core::CompiledGeometryHandle geometry)
{
}

void RenderInterface::EnableScissorRegion(bool enable)
{
  if (enable)
  {
    glEnable(GL_SCISSOR_TEST);
  }
  else
  {
    glDisable(GL_SCISSOR_TEST);
  }
}

void RenderInterface::SetScissorRegion(int x, int y, int width, int height)
{
  glScissor(x, y, width, height);
}

bool RenderInterface::LoadTexture(Rocket::Core::TextureHandle& texture_handle, Rocket::Core::Vector2i& texture_dimensions, const Rocket::Core::String& source)
{
  const king::Texture::SharedPtr& texture = texture_manager_->GetTexture(source.CString());
  assert(!texture.isNull());
  const king::TextureUnit::SharedPtr& texture_unit = texture->GetTextureUnit();
  texture_handle = texture_unit->GetTextureUnitId();
  texture_dimensions.x = texture_unit->GetWidth();
  texture_dimensions.y = texture_unit->GetHeight();
  return true;
}

bool RenderInterface::GenerateTexture(Rocket::Core::TextureHandle& texture_handle, const Rocket::Core::byte* source, const Rocket::Core::Vector2i& source_dimensions)
{
  GLuint texture_id = 0;
  glGenTextures(1, &texture_id);
  glBindTexture(GL_TEXTURE_2D, texture_id);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, source_dimensions.x, source_dimensions.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, source);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  texture_handle = (Rocket::Core::TextureHandle) texture_id;
  return true;
}

void RenderInterface::ReleaseTexture(Rocket::Core::TextureHandle texture)
{
  
}

} // namespace gui
} // namespace king
