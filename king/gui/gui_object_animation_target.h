//
//  gui_object_animation_target.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 03/11/13.
//
//

#ifndef KING_ANIMATION_GUI_OBJECT_ANIMATION_TARGET_H
#define KING_ANIMATION_GUI_OBJECT_ANIMATION_TARGET_H

#include <glm/glm.hpp>
#include "king/animation/animation_target.h"

namespace Rocket {
namespace Core {

class Element;

} // namespace Core
} // namespace Rocket

namespace king {
namespace gui {

class GuiObjectAnimationTarget: public king::animation::AnimationTarget {
  public:
    GuiObjectAnimationTarget(Rocket::Core::Element* gui_object);
    virtual ~GuiObjectAnimationTarget();
    virtual void SetPosition(const glm::vec2& position);
    virtual const glm::vec2& GetPosition() const;
    virtual void SetSize(const glm::vec2& size);
    virtual const glm::vec2& GetSize() const;
  
  private:
    Rocket::Core::Element* gui_object_;
    mutable glm::vec2 position_cache_;
    mutable glm::vec2 size_cache_;
  
};

} // namespace gui
} // namespace king

#endif  // KING_ANIMATION_GUI_OBJECT_ANIMATION_TARGET_H
