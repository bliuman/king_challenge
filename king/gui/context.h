//
//  context.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 31/10/13.
//
//

#ifndef KING_GUI_CONTEXT_H
#define KING_GUI_CONTEXT_H

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include "king/core/game_object.h"
#include "king/gui/gui_screen.h"
#include "king/core/factory.h"

namespace Rocket {
namespace Core {

class Context;

} // namespace Core
} // namespace Rocket

namespace king {

class Frame;

namespace event {

class EventQueue;

namespace gui {

class GuiScreen;

} // namespace gui
} // namespace event
} // namespace king

namespace king {
namespace gui {

struct ContextFactoryParam {
  ContextFactoryParam(const std::string& name, const glm::ivec2& size, const king::gui::GuiScreen::Factory::SharedPtr& screen_factory): name_(name), size_(size), screen_factory_(screen_factory) {}
  std::string name_;
  glm::ivec2 size_;
  king::gui::GuiScreen::Factory::SharedPtr screen_factory_;
};

class Context: public king::core::GameObject {
  public:
    typedef king::core::Factory<king::gui::Context, ContextFactoryParam> Factory;
  
  private:
    typedef std::vector<king::gui::GuiScreen*> GuiScreenList;
    enum MouseButtonId {
      kLeftButton = 0,
      kRightButton,
      kMiddleButton,
      kOtherButton
    };
  
  public:
    Context(Rocket::Core::Context* rocket_context, const king::gui::GuiScreen::Factory::SharedPtr& screen_factory);
    virtual ~Context();
    Rocket::Core::Context* GetRocketContext() const {return rocket_context_;}
    king::gui::GuiScreen* CreateScreen(const std::string& name);
    void DestroyScreen(king::gui::GuiScreen* gui_screen);
    
    void Update(king::event::EventQueue& event_queue);
    void Render();
  
  private:
    bool ConsumeMouseEvent() const;
    MouseButtonId SdlToRocketMouseButtonId(const char sdl_mouse_button) const;
  
    Rocket::Core::Context* rocket_context_;
    king::gui::GuiScreen::Factory::SharedPtr screen_factory_;
    GuiScreenList screen_list_;
  
};

} // namespace gui
} // namespace king

#endif  // KING_GUI_CONTEXT_H
