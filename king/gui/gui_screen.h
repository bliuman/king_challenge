//
//  gui_screen.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 30/10/13.
//
//

#ifndef KING_GUI_SCREEN_H
#define KING_GUI_SCREEN_H

#include <string>
#include <Rocket/Core/EventListener.h>
#include "king/core/factory.h"

namespace Rocket {
namespace Core {

class Context;
class ElementDocument;

} // namespace Core
} // namespace Rocket

namespace king {
namespace gui {

class GuiScreen: public Rocket::Core::EventListener {
  public:
    struct FactoryParam {
      FactoryParam(const std::string& name, Rocket::Core::Context* rocket_context): name_(name), rocket_context_(rocket_context) {}
      std::string name_;
      Rocket::Core::Context* rocket_context_;
    };
    typedef king::core::Factory<GuiScreen, GuiScreen::FactoryParam> Factory;

  public:
    GuiScreen(Rocket::Core::ElementDocument* rocket_document);
    virtual ~GuiScreen();
    Rocket::Core::ElementDocument* GetRocketDocument() const {return ui_document_;}
    
  protected:
    void RegisterEvent(const std::string& element_id, const std::string& event_id);
    void UnregisterEvent(const std::string& element_id, const std::string& event_id);
  
  private:
    Rocket::Core::ElementDocument* ui_document_;
  
};

} // namespace gui
} // namespace king

#endif  // KING_GUI_SCREEN_H
