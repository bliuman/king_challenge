//
//  context.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 31/10/13.
//
//

#include "king/gui/context.h"
#include <assert.h>
#include <Rocket/Core/Context.h>
#include "king/event/event_queue.h"
#include "king/core/frame.h"
#include "king/opengl.h"

namespace king {
namespace gui {

Context::Context(Rocket::Core::Context* rocket_context, const king::gui::GuiScreen::Factory::SharedPtr& screen_factory)
  :king::core::GameObject(new king::core::Frame())
  ,rocket_context_(rocket_context)
  ,screen_factory_(screen_factory)
  ,screen_list_()
{
  assert(NULL != rocket_context_);
  assert(!screen_factory_.isNull());
}

Context::~Context()
{
  assert(screen_list_.empty());
  rocket_context_->RemoveReference();
}

king::gui::GuiScreen* Context::CreateScreen(const std::string& name)
{
  assert(!name.empty());
  screen_list_.push_back(screen_factory_->Create(king::gui::GuiScreen::FactoryParam(name, rocket_context_)));
  return screen_list_.back();
}

void Context::DestroyScreen(king::gui::GuiScreen* gui_screen)
{
  GuiScreenList::iterator iter = std::find(screen_list_.begin(), screen_list_.end(), gui_screen);
  assert(screen_list_.end() != iter);
  rocket_context_->UnloadDocument(gui_screen->GetRocketDocument());
  screen_list_.erase(iter);
  screen_factory_->Destroy(gui_screen);
}

void Context::Update(king::event::EventQueue &event_queue)
{
  king::event::EventQueue::Events& events = event_queue.GetEvents();
  SDL_Event* evt = NULL;
  for (king::event::EventQueue::Events::iterator iter = events.begin(); events.end() != iter; ++iter)
  {
    evt = &iter->first;
    switch (evt->type)
    {
      case SDL_MOUSEMOTION:
      {
        rocket_context_->ProcessMouseMove(evt->motion.x, evt->motion.y, 0);
        iter->second = ConsumeMouseEvent();
      } break;
      
      case SDL_MOUSEBUTTONDOWN:
      {
        rocket_context_->ProcessMouseButtonDown(SdlToRocketMouseButtonId(evt->button.button), 0);
        iter->second = ConsumeMouseEvent();
      } break;
      
      case SDL_MOUSEBUTTONUP:
      {
        rocket_context_->ProcessMouseButtonUp(SdlToRocketMouseButtonId(evt->button.button), 0);
        iter->second = ConsumeMouseEvent();
      } break;
      
      case SDL_MOUSEWHEEL:
      {
        rocket_context_->ProcessMouseWheel(evt->wheel.x, 0);
        iter->second = ConsumeMouseEvent();
      } break;
      
    };
  }
  rocket_context_->Update();
}

void Context::Render()
{
  rocket_context_->Render();
}

bool Context::ConsumeMouseEvent() const
{
  Rocket::Core::Element* hover_element = rocket_context_->GetHoverElement();
  return (hover_element != NULL && hover_element != rocket_context_->GetRootElement());
}

Context::MouseButtonId Context::SdlToRocketMouseButtonId(const char sdl_mouse_button) const
{
  switch (sdl_mouse_button)
  {
    case 1:
    {
      return kLeftButton;
    } break;
    
    case 2:
    {
      return kMiddleButton;
    } break;
    
    case 3:
    {
      return kRightButton;
    } break;
    
    default:
    {
      return kOtherButton;
    } break;
  }
}

} // namespace gui
} // namespace king
