//
//  gui_screen.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 30/10/13.
//
//

#include "king/gui/gui_screen.h"
#include <assert.h>
#include <Rocket/Core.h>

namespace king {
namespace gui {

GuiScreen::GuiScreen(Rocket::Core::ElementDocument* rocket_document)
    :ui_document_(rocket_document)
{
  assert(NULL != ui_document_);
  ui_document_->Show();
}

GuiScreen::~GuiScreen()
{
}

void GuiScreen::RegisterEvent(const std::string &element_id, const std::string &event_id)
{
  assert(!element_id.empty());
  assert(!event_id.empty());
  Rocket::Core::Element* element = ui_document_->GetElementById(element_id.c_str());
  assert(NULL != element);
  element->AddEventListener(event_id.c_str(), this);
}

void GuiScreen::UnregisterEvent(const std::string &element_id, const std::string &event_id)
{
  assert(!element_id.empty());
  assert(!event_id.empty());
  Rocket::Core::Element* element = ui_document_->GetElementById(element_id.c_str());
  assert(NULL != element);
  element->RemoveEventListener(event_id.c_str(), this);
}

} // namespace gui
} // namespace king
