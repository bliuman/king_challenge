//
//  render_operation.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/25/13.
//
//

#ifndef KING_RENDER_OPERATION_H
#define KING_RENDER_OPERATION_H

#include <glm/glm.hpp>
#include "king/base_render_operation.h"
#include "king/core/game_object.h"
#include "king/gpu_program.h"
#include "king/gpu_program_uniforms_binder.h"
#include "king/render_state_switch.h"
#include "king/texture_unit.h"
#include "king/uniform_params_container.h"
#include "king/vertex_buffer.h"

namespace king {

template<typename VertexDeclaration>
class RenderOperation: public king::BaseRenderOperation {
  public:
    static const int kDefaultBufferSize = 1;
  
  public:
    RenderOperation(const king::GPUProgram::SharedPtr& gpu_program, const king::UniformParamsContainer::SharedPtr& uniform_params_container);
    virtual ~RenderOperation();
    virtual void Begin(const king::BaseRenderOperation::TextureList& textures, king::BaseRenderOperation::RenderStateSwitchList& render_state_switches);
    virtual void Update(const king::BaseRenderOperation::RenderList& render_objects);
    virtual void Render();
    virtual void End(const king::BaseRenderOperation::TextureList& textures, king::BaseRenderOperation::RenderStateSwitchList& render_state_switches);
  
  private:
    king::VertexBuffer<VertexDeclaration> buffer_;
    king::GPUProgram::SharedPtr gpu_program_;
    king::GPUProgramUniformsBinder gpu_uniforms_binder_;
  
};

template<typename VertexDeclaration>
RenderOperation<VertexDeclaration>::RenderOperation(const king::GPUProgram::SharedPtr& gpu_program, const king::UniformParamsContainer::SharedPtr& uniform_params_container)
  :buffer_()
  ,gpu_program_(gpu_program)
  ,gpu_uniforms_binder_(gpu_program, uniform_params_container)
{
}

template<typename VertexDeclaration>
RenderOperation<VertexDeclaration>::~RenderOperation()
{
}

template<typename VertexDeclaration>
void RenderOperation<VertexDeclaration>::Begin(const king::BaseRenderOperation::TextureList& textures, king::BaseRenderOperation::RenderStateSwitchList& render_state_switches)
{
  for (king::BaseRenderOperation::RenderStateSwitchList::iterator iter = render_state_switches.begin(); render_state_switches.end() != iter; ++iter)
  {
    (*iter)->TurnOn();
  }
  
  if (!textures.empty())
  {
    GLenum texture_id;
    for (int i = 0; i < textures.size(); ++i)
    {
      texture_id = GL_TEXTURE0 + i;
      glActiveTexture(texture_id);
      glBindTexture(GL_TEXTURE_2D, textures[i]->GetTextureUnitId());
    }
  }
  
  gpu_program_->Activate();
  buffer_.BindVertexAttributes(gpu_program_);
  gpu_uniforms_binder_.Bind();
}

template<typename VertexDeclaration>
void RenderOperation<VertexDeclaration>::Update(const king::BaseRenderOperation::RenderList& render_objects)
{
  buffer_.UpdateData(render_objects);
}

template<typename VertexDeclaration>
void RenderOperation<VertexDeclaration>::Render()
{
  glDrawArrays(GL_TRIANGLES, 0, buffer_.GetVertexCount());
}

template<typename VertexDeclaration>
void RenderOperation<VertexDeclaration>::End(const king::BaseRenderOperation::TextureList& textures, king::BaseRenderOperation::RenderStateSwitchList& render_state_switches)
{
  buffer_.UnbindVertexAttributes();
  gpu_program_->Deactivate();
  
  if (!textures.empty())
  {
    GLenum texture_id;
    for (int i = 0; i < textures.size(); ++i)
    {
      texture_id = GL_TEXTURE0 + i;
      glActiveTexture(texture_id);
      glBindTexture(GL_TEXTURE_2D, 0);
    }
  }
  
  for (king::BaseRenderOperation::RenderStateSwitchList::iterator iter = render_state_switches.begin(); render_state_switches.end() != iter; ++iter)
  {
    (*iter)->TurnOff();
  }
}

} // namespace king

#endif  // KING_RENDER_OPERATION_H
