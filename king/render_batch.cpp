//
//  render_batch.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/26/13.
//
//

#include "king/render_batch.h"
#include <assert.h>
#include <Poco/Delegate.h>
#include "king/core/frame.h"
#include "king/core/game_object.h"
#include "king/render_queue.h"
#include "king/render_state_switch.h"

namespace king {

RenderBatch::RenderBatch(king::RenderQueue* queue, const king::BaseRenderOperation::SharedPtr& operation, const king::BaseRenderOperation::TextureList& textures, const king::BaseRenderOperation::RenderStateSwitchList& render_state_switches, int priority)
  :queue_(queue)
  ,operation_(operation)
  ,textures_(textures)
  ,render_state_switches_(render_state_switches)
  ,priority_(priority)
  ,render_objects_()
  ,dirty_(true)
{
  assert(NULL != queue_);
  assert(!operation_.isNull());
}

RenderBatch::~RenderBatch()
{
  for (king::BaseRenderOperation::RenderStateSwitchList::const_iterator iter = render_state_switches_.begin(); render_state_switches_.end() != iter; ++iter)
  {
    delete (*iter);
  }
  render_state_switches_.clear();
}

void RenderBatch::Add(king::core::GameObject *game_object)
{
  assert(NULL != game_object);
  render_objects_.push_back(game_object);
  game_object->OnStateChangedEvent += Poco::delegate(this, &RenderBatch::OnSwitchRenderState);
  game_object->GetFrame()->OnFrameChangedEvent += Poco::delegate(this, &RenderBatch::OnUpdateGeometry);
  dirty_ = true;
}

void RenderBatch::Remove(king::core::GameObject *game_object)
{
  assert(NULL != game_object);
  game_object->GetFrame()->OnFrameChangedEvent -= Poco::delegate(this, &RenderBatch::OnUpdateGeometry);
  game_object->OnStateChangedEvent -= Poco::delegate(this, &RenderBatch::OnSwitchRenderState);
  render_objects_.erase(std::find(render_objects_.begin(), render_objects_.end(), game_object));
  dirty_ = true;
}

void RenderBatch::Render()
{
  if (dirty_)
  {
    operation_->Update(render_objects_);
    dirty_ = false;
  }
  operation_->Begin(textures_, render_state_switches_);
  operation_->Render();
  operation_->End(textures_, render_state_switches_);
}

void RenderBatch::OnUpdateGeometry(const void *sender, const Poco::EventArgs &args)
{
  dirty_ = true;
}

void RenderBatch::OnSwitchRenderState(const void *sender, const std::string &old_state_hash)
{
  king::core::GameObject* game_object = const_cast<king::core::GameObject*>(reinterpret_cast<const king::core::GameObject*>(sender));
  queue_->ReinsertGameObject(game_object, old_state_hash);
  dirty_ = true;
}

} // namespace king
