//
//  base_render_operation.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/25/13.
//
//

#ifndef KING_BASE_RENDER_OPERATION_H
#define KING_BASE_RENDER_OPERATION_H

#include <vector>
#include <Poco/SharedPtr.h>
#include "king/texture_unit.h"

namespace king {
namespace core {

class GameObject;

} // namespace core

class RenderStateSwitch;

}  // namespace king

namespace king {

class BaseRenderOperation {
  public:
    typedef Poco::SharedPtr<BaseRenderOperation> SharedPtr;
    typedef std::vector<king::core::GameObject*> RenderList;
    typedef std::vector<king::TextureUnit::SharedPtr> TextureList;
    typedef std::vector<king::RenderStateSwitch*> RenderStateSwitchList;
  
  public:
    BaseRenderOperation() {}
    virtual ~BaseRenderOperation() {}
    virtual void Begin(const TextureList& textures, RenderStateSwitchList& render_state_switches) = 0;
    virtual void Update(const RenderList& render_objects) = 0;
    virtual void Render() = 0;
    virtual void End(const TextureList& textures, RenderStateSwitchList& render_state_switches) = 0;
    
};

} // namespace king

#endif  // KING_BASE_RENDER_OPERATION_H
