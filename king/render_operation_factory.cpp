//
//  render_operation_factory.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#include "king/render_operation_factory.h"
#include <assert.h>
#include <fstream>
#include <Poco/Path.h>
#include <Poco/File.h>
#include "king/gui/context_render_operation.h"
#include "king/render_operation.h"
#include "king/shader.h"
#include "king/vertex_xyuv.h"
#include "king/vertex_xyrgba.h"
#include "king/vertex_xyuvrgba.h"

namespace king {

RenderOperationFactory::RenderOperationFactory(const king::UniformParamsContainer::SharedPtr& uniform_params_container)
  :uniform_params_container_(uniform_params_container)
  ,factory_functions_()
{
  assert(!uniform_params_container_.isNull());
  factory_functions_.insert(FactoryFunctionMap::value_type("basic_textured_quad", &RenderOperationFactory::BasicTexturedQuad));
  factory_functions_.insert(FactoryFunctionMap::value_type("basic_colored_quad", &RenderOperationFactory::BasicColoredQuad));
  factory_functions_.insert(FactoryFunctionMap::value_type("basic_xyuvrgba_quad", &RenderOperationFactory::QuadXYUVRGBA));
  factory_functions_.insert(FactoryFunctionMap::value_type("gui_context", &RenderOperationFactory::GuiContext));
}

RenderOperationFactory::~RenderOperationFactory()
{
}

king::BaseRenderOperation::SharedPtr RenderOperationFactory::Create(const std::string& name) const
{
  FactoryFunctionMap::const_iterator iter = factory_functions_.find(name);
  assert(factory_functions_.end() != iter);
  return (this->*iter->second)();
}

king::GPUProgram::SharedPtr RenderOperationFactory::LoadGpuProgram(const std::string& name) const
{
  king::Shader::SharedPtr vertex_shader = new king::Shader(king::Shader::kVertex);
  Poco::Path path(Poco::Path::current());
  path.pushDirectory("assets").setBaseName(name).setExtension("vert");
  Poco::File file(path);
  assert(file.exists());
  std::ifstream ifs(path.toString().c_str());
  std::string contents((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
  vertex_shader->Compile(contents);
  
  king::Shader::SharedPtr fragment_shader = new king::Shader(king::Shader::kFragment);
  path.setExtension("frag");
  file = path;
  assert(file.exists());
  ifs.close();
  ifs.open(path.toString().c_str());
  contents = std::string((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
  fragment_shader->Compile(contents);
  
  king::GPUProgram::SharedPtr gpu_program = new king::GPUProgram(vertex_shader, fragment_shader);
  gpu_program->Link();
  return gpu_program;
}

king::BaseRenderOperation::SharedPtr RenderOperationFactory::BasicTexturedQuad() const
{
  return new king::RenderOperation<king::VertexXYUV>(LoadGpuProgram("basic_textured_quad"), uniform_params_container_);
}

king::BaseRenderOperation::SharedPtr RenderOperationFactory::BasicColoredQuad() const
{
  return new king::RenderOperation<king::VertexXYRGBA>(LoadGpuProgram("basic_colored_quad"), uniform_params_container_);
}

king::BaseRenderOperation::SharedPtr RenderOperationFactory::QuadXYUVRGBA() const
{
  return new king::RenderOperation<king::VertexXYUVRGBA>(LoadGpuProgram("basic_xyuvrgba_quad"), uniform_params_container_);
}

king::BaseRenderOperation::SharedPtr RenderOperationFactory::GuiContext() const
{
  return new king::gui::ContextRenderOperation();
}

} // namespace king
