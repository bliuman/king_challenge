//
//  texture_unit.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#include "king/texture_unit.h"
#include <assert.h>
#include <math.h>
#include <SOIL.h>

namespace king {

TextureUnit::TextureUnit(const std::string& texture_path)
  :texture_unit_id_(0)
  ,original_width_(1)
  ,original_height_(1)
  ,width_(1)
  ,height_(1)
  ,channel_count_(0)
{
  unsigned char* texture_unit_data = SOIL_load_image(texture_path.c_str(), &original_width_, &original_height_, &channel_count_, SOIL_LOAD_AUTO);
  if (NULL != texture_unit_data)
  {
    texture_unit_id_ = SOIL_create_OGL_texture(texture_unit_data, original_width_, original_height_, channel_count_, SOIL_CREATE_NEW_ID, SOIL_FLAG_POWER_OF_TWO);
    assert(texture_unit_id_ > 0);
    SOIL_free_image_data(texture_unit_data);
    width_ = GetNextPowerOfTwo(original_width_);
    height_ = GetNextPowerOfTwo(original_height_);
  }
  assert(texture_unit_id_ > 0);
}

TextureUnit::~TextureUnit()
{
  glDeleteTextures(1, &texture_unit_id_);
}

int TextureUnit::GetNextPowerOfTwo(int dimension) const
{
  int result = 1;
  while (result < dimension)
  {
    result *= 2;
  }
  return result;
}

} // namespace king
