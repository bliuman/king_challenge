//
//  vertex_xyuv.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/25/13.
//
//

#ifndef VERTEX_XYUV_H
#define VERTEX_XYUV_H

#include <vector>
#include <Poco/Buffer.h>
#include "king/base_render_operation.h"
#include "king/gpu_program.h"
#include "king/opengl.h"

namespace king {

struct VertexXYUV {
  typedef Poco::Buffer<VertexXYUV> BufferType;
  typedef std::vector<GLuint> AttributeList;
  struct AttribBindInfo {
    std::string name_;
    GLint size_;
    GLenum type_;
    GLboolean normalize_;
    const GLvoid* offset_;
  };
  
  static void BindVertexAttributes(const king::GPUProgram::SharedPtr& gpu_program, AttributeList& attribute_list);
  static void UnbindVertexAttributes(const AttributeList& attribute_list);
  static int UpdateData(BufferType& buffer, const king::BaseRenderOperation::RenderList& game_objects);
  
  float position[2];
  float uv[2];
};

} // namespace king

#endif  // VERTEX_XYUV_H
