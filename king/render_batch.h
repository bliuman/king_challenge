//
//  render_batch.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/26/13.
//
//

#ifndef KING_RENDER_BATCH_H
#define KING_RENDER_BATCH_H

#include <vector>
#include <Poco/EventArgs.h>
#include "king/base_render_operation.h"

namespace king {
namespace core {

class GameObject;

} // namespace core

class RenderQueue;

} // namespace king

namespace king {

class RenderBatch {
  public:
    RenderBatch(king::RenderQueue* queue, const king::BaseRenderOperation::SharedPtr& operation, const king::BaseRenderOperation::TextureList& textures, const king::BaseRenderOperation::RenderStateSwitchList& render_state_switches, int priority);
    ~RenderBatch();
    int GetPriority() const {return priority_;}
    void Add(king::core::GameObject* game_object);
    void Remove(king::core::GameObject* game_object);
    void Render();
    void OnUpdateGeometry(const void* sender, const Poco::EventArgs& args);
    void OnSwitchRenderState(const void* sender, const std::string& old_state_hash);
  
  private:
    king::RenderQueue* queue_;
    king::BaseRenderOperation::SharedPtr operation_;
    king::BaseRenderOperation::TextureList textures_;
    king::BaseRenderOperation::RenderStateSwitchList render_state_switches_;
    int priority_;
    king::BaseRenderOperation::RenderList render_objects_;
    bool dirty_;
  
};

} // namespace king

#endif  // KING_RENDER_BATCH_H
