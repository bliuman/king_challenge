//
//  gpu_program.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/24/13.
//
//

#include "king/gpu_program.h"
#include <assert.h>

namespace king {

GPUProgram::GPUProgram(const king::Shader::SharedPtr& vertex_shader, const king::Shader::SharedPtr& fragment_shader)
  :id_(glCreateProgram())
  ,vertex_shader_(vertex_shader)
  ,fragment_shader_(fragment_shader)
  ,attributes_()
  ,uniforms_()
{
  assert(id_ > 0);
  glAttachShader(id_, vertex_shader_->GetID());
  glAttachShader(id_, fragment_shader_->GetID());
}

GPUProgram::~GPUProgram()
{
  if (id_ > 0)
  {
    glDeleteProgram(id_);
  }
}

void GPUProgram::Link()
{
  glLinkProgram(id_);
  GLint status = 0;
  glGetProgramiv(id_, GL_LINK_STATUS, &status);
  if (0 == status)
  {
    //TODO: properly handle failed to link gpu_program error
    assert(false);
  }
  attributes_.CollectInfo(id_);
  uniforms_.CollectInfo(id_);
}

void GPUProgram::Activate()
{
  glUseProgram(id_);
}

void GPUProgram::Deactivate()
{
  
}

} // namespace king
