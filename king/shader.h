//
//  shader.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/24/13.
//
//

#ifndef KING_SHADER_H
#define KING_SHADER_H

#include <string>
#include <Poco/SharedPtr.h>
#include "king/opengl.h"

namespace king {

class Shader {
  public:
    enum ShaderType {
      kVertex = GL_VERTEX_SHADER,
      kFragment = GL_FRAGMENT_SHADER
    };
    typedef Poco::SharedPtr<Shader> SharedPtr;
  
  public:
    Shader(ShaderType type);
    ~Shader();
    ShaderType GetType() const {return type_;}
    GLuint GetID() const {return id_;}
    void Compile(const std::string& source);
  
  private:
    ShaderType type_;
    GLuint id_;
  
};

} // namespace king

#endif  // KING_SHADER_H
