//
//  uniform_params_container.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 25/10/13.
//
//

#ifndef KING_UNIFORM_PARAMS_CONTAINER_H
#define KING_UNIFORM_PARAMS_CONTAINER_H

#include <string>
#include <map>
#include <assert.h>
#include <Poco/SharedPtr.h>
#include "king/base_uniform_parameter.h"
#include "king/uniform_parameter.h"

namespace king {

class UniformParamsContainer {
  private:
    typedef std::map<std::string, king::BaseUniformParameter::SharedPtr> UniformsMap;
  
  public:
    typedef Poco::SharedPtr<UniformParamsContainer> SharedPtr;
  
  public:
    UniformParamsContainer();
    ~UniformParamsContainer();
  
    template<typename Data>
    void Add(const std::string& name, const typename king::UniformParameter<Data>::SharedPtr& parameter);
  
    template<typename Data>
    typename king::UniformParameter<Data>::SharedPtr Get(const std::string& name) const;
  
    king::BaseUniformParameter::SharedPtr Get(const std::string& name) const;
  
  private:
    UniformsMap uniforms_;
  
};

template<typename Data>
void UniformParamsContainer::Add(const std::string& name, const typename king::UniformParameter<Data>::SharedPtr& parameter)
{
  assert(!name.empty());
  std::pair<UniformsMap::iterator, bool> result = uniforms_.insert(UniformsMap::value_type(name, parameter));
  assert(result.second);
}

template<typename Data>
typename king::UniformParameter<Data>::SharedPtr UniformParamsContainer::Get(const std::string &name) const
{
  UniformsMap::const_iterator iter = uniforms_.find(name);
  if (uniforms_.end() == iter)
  {
    assert(false);
  }
  return iter->second.cast<king::UniformParameter<Data> >();
}

} // namespace king

#endif  // KING_UNIFORM_PARAMS_CONTAINER_H
