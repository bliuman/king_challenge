//
//  shader.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/24/13.
//
//

#include "king/shader.h"
#include <assert.h>
#include <iostream>

namespace king {

Shader::Shader(ShaderType type)
  :type_(type)
  ,id_(0)
{
}

Shader::~Shader()
{
  if (id_ > 0)
  {
    glDeleteShader(id_);
  }
}

void Shader::Compile(const std::string& source)
{
  assert(!source.empty());
  id_ = glCreateShader(type_);
  const char* source_data = source.c_str();
  glShaderSource(id_, 1, &source_data, NULL);
  glCompileShader(id_);
  
  GLint status = 0;
  glGetShaderiv(id_, GL_COMPILE_STATUS, &status);
  if (0 == status)
  {
    GLint info_length = 0;
    glGetShaderiv(id_, GL_INFO_LOG_LENGTH, &info_length);
    if (info_length > 1)
    {
      char* info_log = new char[sizeof(char) * info_length];
      glGetShaderInfoLog(id_, info_length, NULL, info_log);
      // TODO: log error from shader compilation step
      std::cout << std::string(info_log, info_length) << std::endl;
      delete[] info_log;
    }
    assert(false); // TODO: properly manage failed shader compilation
  }
}

} // namespace king
