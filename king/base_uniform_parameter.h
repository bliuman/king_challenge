//
//  base_uniform_parameter.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#ifndef KING_BASE_UNIFORM_PARAMETER_H
#define KING_BASE_UNIFORM_PARAMETER_H

#include <Poco/SharedPtr.h>
#include "king/opengl.h"

namespace king {

class BaseUniformParameter {
  public:
    typedef Poco::SharedPtr<BaseUniformParameter> SharedPtr;
  
  public:
    virtual ~BaseUniformParameter() {}
    virtual void Bind(GLuint location) const = 0;
  
  protected:
    BaseUniformParameter() {}
  
};

} // namespace king

#endif  // KING_BASE_UNIFORM_PARAMETER_H
