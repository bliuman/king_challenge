//
//  vertex_xyuv.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/25/13.
//
//

#include "king/core/frame.h"
#include "king/vertex_xyuv.h"
#include <assert.h>
#include <glm/glm.hpp>
#include "king/core/game_object.h"
#include "king/gpu_program_param_info.h"
#include "king/render_state.h"

namespace king {

void VertexXYUV::BindVertexAttributes(const king::GPUProgram::SharedPtr &gpu_program, AttributeList &attribute_list)
{
  const int attrib_count = 2;
  AttribBindInfo attrib_bind_info[attrib_count] = {
    {"vPosition", 2, GL_FLOAT, GL_FALSE, 0},
    {"vTexCoord0", 2, GL_FLOAT, GL_FALSE, (const GLvoid*)(2 * sizeof(GL_FLOAT))}
  };
  
  GLsizei stride = sizeof(VertexXYUV);
  const king::GPUProgram::Parameters& attrib_parameters = gpu_program->GetAttributes().GetParameters();
  king::GPUProgram::Parameters::const_iterator iter = attrib_parameters.end();
  for (int i = 0; i < attrib_count; ++i)
  {
    const AttribBindInfo& info = attrib_bind_info[i];
    iter = attrib_parameters.find(info.name_);
    assert(attrib_parameters.end() != iter);
    glVertexAttribPointer(iter->second, info.size_, info.type_, info.normalize_, stride, info.offset_);
    glEnableVertexAttribArray(iter->second);
    attribute_list.push_back(iter->second);
  }
}

void VertexXYUV::UnbindVertexAttributes(const AttributeList &attribute_list)
{
  for (AttributeList::const_iterator iter = attribute_list.begin(); attribute_list.end() != iter; ++iter)
  {
    glDisableVertexAttribArray((*iter));
  }
}

int VertexXYUV::UpdateData(BufferType &buffer, const king::BaseRenderOperation::RenderList& render_objects)
{
  int vertex_count_ = 0;
  int vertex_per_object = 6;
  if (buffer.size() < render_objects.size() * vertex_per_object)
  {
    buffer.resize(buffer.size() * 2, false);
  }
  
  for (king::BaseRenderOperation::RenderList::const_iterator iter = render_objects.begin(); render_objects.end() != iter; ++iter)
  {
    const king::core::Frame* frame = (*iter)->GetFrame();
    const glm::vec2& position = frame->GetPosition();
    const glm::vec2& dimension = frame->GetDimension();
    const king::RenderState* render_state = (*iter)->GetRenderState();
    const king::RenderState::TextureList& textures = render_state->GetTextures();
    assert(1 == textures.size());
    const king::Texture::SharedPtr& texture = textures.front();
    const glm::vec2& min_uv = texture->GetMinUV();
    const glm::vec2& max_uv = texture->GetMaxUV();
    
    // top left vertex
    buffer[vertex_count_].position[0] = position.x;
    buffer[vertex_count_].position[1] = position.y;
    buffer[vertex_count_].uv[0] = min_uv[0];
    buffer[vertex_count_].uv[1] = min_uv[1];
    
    // bottom left vertex
    buffer[vertex_count_ + 1].position[0] = position.x;
    buffer[vertex_count_ + 1].position[1] = position.y + dimension.y;
    buffer[vertex_count_ + 1].uv[0] = min_uv[0];
    buffer[vertex_count_ + 1].uv[1] = max_uv[1];
    
    // bottom right vertex
    buffer[vertex_count_ + 2].position[0] = position.x + dimension.x;
    buffer[vertex_count_ + 2].position[1] = position.y + dimension.y;
    buffer[vertex_count_ + 2].uv[0] = max_uv[0];
    buffer[vertex_count_ + 2].uv[1] = max_uv[1];
    
    // bottom right vertex
    buffer[vertex_count_ + 3].position[0] = position.x + dimension.x;
    buffer[vertex_count_ + 3].position[1] = position.y + dimension.y;
    buffer[vertex_count_ + 3].uv[0] = max_uv[0];
    buffer[vertex_count_ + 3].uv[1] = max_uv[1];
    
    // top right vertex
    buffer[vertex_count_ + 4].position[0] = position.x + dimension.x;
    buffer[vertex_count_ + 4].position[1] = position.y;
    buffer[vertex_count_ + 4].uv[0] = max_uv[0];
    buffer[vertex_count_ + 4].uv[1] = min_uv[1];
    
    // top left vertex
    buffer[vertex_count_ + 5].position[0] = position.x;
    buffer[vertex_count_ + 5].position[1] = position.y;
    buffer[vertex_count_ + 5].uv[0] = min_uv[0];
    buffer[vertex_count_ + 5].uv[1] = min_uv[1];
    
    vertex_count_ += vertex_per_object;
  }
  return vertex_count_;
}

} // namespace king
