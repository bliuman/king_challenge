//
//  gpu_program_uniform_collector.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/25/13.
//
//

#include "king/gpu_program_uniform_collector.h"
#include <Poco/Buffer.h>

namespace king {

GPUProgramUniformCollector::GPUProgramUniformCollector()
{
}

GPUProgramUniformCollector::~GPUProgramUniformCollector()
{
}

void GPUProgramUniformCollector::GetTypes(GLenum& parameter, GLenum& buffer_length) const
{
  parameter = GL_ACTIVE_UNIFORMS;
  buffer_length = GL_ACTIVE_UNIFORM_MAX_LENGTH;
}

GLint GPUProgramUniformCollector::Collect(GLuint gpu_prgram_id_, GLint index, GLsizei buffer_size, GLint* size, GLenum* type, Poco::Buffer<char>& name_buffer) const
{
  glGetActiveUniform(gpu_prgram_id_, index, buffer_size, static_cast<GLsizei*>(0), size, type, name_buffer.begin());
  return glGetUniformLocation(gpu_prgram_id_, name_buffer.begin());
}

} // namespace king
