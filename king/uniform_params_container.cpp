//
//  uniform_params_container.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 25/10/13.
//
//

#include "king/uniform_params_container.h"

namespace king {

UniformParamsContainer::UniformParamsContainer()
{
}

UniformParamsContainer::~UniformParamsContainer()
{
}

king::BaseUniformParameter::SharedPtr UniformParamsContainer::Get(const std::string &name) const
{
  UniformsMap::const_iterator iter = uniforms_.find(name);
  if (uniforms_.end() == iter)
  {
    assert(false);
  }
  return iter->second;
}

} // namespace king
