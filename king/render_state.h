//
//  render_state.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#ifndef KING_RENDER_STATE_H
#define KING_RENDER_STATE_H

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include "king/texture.h"

namespace king {

class RenderState {
  public:
    typedef std::vector<king::Texture::SharedPtr> TextureList;
  
  public:
    RenderState();
    RenderState(const std::string& render_op_name, int depth = 0, const TextureList& texture_list = TextureList(), const glm::ivec4& color = glm::ivec4(255, 255, 255, 255));
    ~RenderState();

    const std::string& GetRenderOperationName() const {return render_op_name_;}
    int GetDepth() const {return depth_;}
    const TextureList& GetTextures() const {return texture_list_;}
    const glm::ivec4& GetColor() const {return color_;}
    const std::string& GetObjectHash() const {return hash_;}
  
  private:
    void CalculateObjectHash();
  
    std::string render_op_name_;
    int depth_;
    TextureList texture_list_;
    glm::ivec4 color_;
    std::string hash_;
  
};

} // namespace king

#endif  // KING_RENDER_STATE_H
