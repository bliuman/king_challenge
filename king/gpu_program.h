//
//  gpu_program.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/24/13.
//
//

#ifndef KING_GPU_PROGRAM_H
#define KING_GPU_PROGRAM_H

#include <string>
#include <map>
#include <Poco/SharedPtr.h>
#include "king/shader.h"
#include "king/gpu_program_param_info.h"
#include "king/gpu_program_attribute_collector.h"
#include "king/gpu_program_uniform_collector.h"
#include "king/opengl.h"

namespace king {

class GPUProgram {
  public:
    typedef Poco::SharedPtr<GPUProgram> SharedPtr;
    typedef std::map<std::string, GLuint> Parameters;
    typedef king::GPUProgramParamInfo<king::GPUProgramAttributeCollector, Parameters> Attributes;
    typedef king::GPUProgramParamInfo<king::GPUProgramUniformCollector, Parameters> Uniforms;
  
  public:
    GPUProgram(const king::Shader::SharedPtr& vertex_shader, const king::Shader::SharedPtr& fragment_shader);
    ~GPUProgram();
    GLuint GetID() const {return id_;}
    const Attributes& GetAttributes() const {return attributes_;}
    const Uniforms& GetUnitforms() const {return uniforms_;}
    void Link();
    void Activate();
    void Deactivate();
  
  private:
    GLuint id_;
    king::Shader::SharedPtr vertex_shader_;
    king::Shader::SharedPtr fragment_shader_;
    Attributes attributes_;
    Uniforms uniforms_;
  
};

} // namespace king

#endif  // KING_GPU_PROGRAM_H
