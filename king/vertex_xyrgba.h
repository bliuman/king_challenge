//
//  vertex_xyrgba.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/25/13.
//
//

#ifndef VERTEX_XYRGBA_H
#define VERTEX_XYRGBA_H

#include <vector>
#include <Poco/Buffer.h>
#include "king/base_render_operation.h"
#include "king/gpu_program.h"
#include "king/opengl.h"

namespace king {

struct VertexXYRGBA {
  typedef Poco::Buffer<VertexXYRGBA> BufferType;
  typedef std::vector<GLuint> AttributeList;
  struct AttribBindInfo {
    std::string name_;
    GLint size_;
    GLenum type_;
    GLboolean normalize_;
    const GLvoid* offset_;
  };
  
  static void BindVertexAttributes(const king::GPUProgram::SharedPtr& gpu_program, AttributeList& attribute_list);
  static void UnbindVertexAttributes(const AttributeList& attribute_list);
  static int UpdateData(BufferType& buffer, const king::BaseRenderOperation::RenderList& game_objects);
  
  float position[2];
  unsigned char color[4];
};

} // namespace king

#endif  // VERTEX_XYRGBA_H
