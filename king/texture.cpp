//
//  texture.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#include "king/texture.h"
#include <assert.h>

namespace king {

Texture::Texture(const king::TextureUnit::SharedPtr& texture_unit)
  :texture_unit_(texture_unit)
  ,uv_min_(.0f, .0f)
  ,uv_max_(1.f, 1.f)
{
  assert(!texture_unit_.isNull());
//  CalculateUV(glm::vec2(0, 0), glm::vec2(texture_unit_->GetOriginalWidth(), texture_unit_->GetOriginalHeight()));
}

Texture::Texture(const king::TextureUnit::SharedPtr& texture_unit, const glm::vec2& min, const glm::vec2& max)
  :texture_unit_(texture_unit)
  ,uv_min_(.0f, .0f)
  ,uv_max_(.0f, .0f)
{
  assert(!texture_unit_.isNull());
  CalculateUV(min, max);
}

Texture::~Texture()
{
}

void Texture::CalculateUV(const glm::vec2& min, const glm::vec2& max)
{
  float texture_unit_width = (float)texture_unit_->GetWidth();
  float texture_unit_height = (float)texture_unit_->GetHeight();
  uv_min_.x = min.x / texture_unit_width;
  uv_min_.y = min.y / texture_unit_height;
  uv_max_.x = uv_min_.x + ((max.x - min.x) / texture_unit_width);
  uv_max_.y = uv_min_.y + ((max.y - min.y) / texture_unit_height);
}

} // namespace king
