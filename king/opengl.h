#ifndef KING_OPENGL_H
#define KING_OPENGL_H

#if defined(TARGET_PLATFORM_WINDOWS)
#	include <GL/glew.h>
#endif
#include <SDL_opengl.h>

#endif	// KING_OPENGL_H