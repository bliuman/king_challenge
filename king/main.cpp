#include <iostream>
#include <SDL.h>
#if defined (TARGET_PLATFORM_WINDOWS)
#	undef main
#endif
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "king/animation/animation_system.h"
#include "king/event/event_queue.h"
#include "king/game/context_factory.h"
#include "king/game/state_factory.h"
#include "king/gui/gui_manager.h"
#include "king/gui/render_interface.h"
#include "king/gui/system_interface.h"
#include "king/opengl.h"
#include "king/render_batch_factory.h"
#include "king/render_operation_factory.h"
#include "king/render_queue.h"
#include "king/render_state_factory.h"
#include "king/core/state_container.h"
#include "king/texture_manager.h"
#include "king/uniform_params_container.h"
#include "king/uniform_mat4.h"
#include "king/uniform_texture.h"

int main()
{
  if (-1 == SDL_Init(SDL_INIT_EVERYTHING))
  {
      std::cout << SDL_GetError() << std::endl;
      return -1;
  }
  
  SDL_Window* window = NULL;
  window = SDL_CreateWindow("Game", 100, 100, 800, 600, SDL_WINDOW_OPENGL);
  if (NULL == window)
  {
      std::cout << SDL_GetError() << std::endl;
      return -1;
  }
  
  SDL_GLContext glContext = SDL_GL_CreateContext(window);
  if (NULL == glContext)
  {
	  std::cout << SDL_GetError() << std::endl;
	  return -1;
  }
  
#if defined(TARGET_PLATFORM_WINDOWS)
  glewInit();
#endif
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, 800, 600, 0, -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  king::UniformParamsContainer::SharedPtr uniforms = new king::UniformParamsContainer();
  uniforms->Add<glm::mat4>("ModelView", new king::UniformMat4(glm::mat4()));
  uniforms->Add<glm::mat4>("Projection", new king::UniformMat4(glm::ortho(0.0f, 800.0f, 600.0f, 0.0f, -1.0f, 1.0f)));
  uniforms->Add<GLuint>("texture0", new king::UniformTexture(0));

  king::TextureManager::SharedPtr texture_manager = new king::TextureManager(Poco::Path::current());
  king::RenderOperationFactory::SharedPtr render_op_factory = new king::RenderOperationFactory(uniforms);
  king::RenderBatchFactory::SharedPtr batch_factory = new king::RenderBatchFactory(render_op_factory);
  king::RenderQueue::SharedPtr render_queue = new king::RenderQueue(batch_factory);
  
  king::gui::SystemInterface gui_system_interface;
  king::gui::RenderInterface gui_render_interface(texture_manager);
  king::gui::GuiManager::SharedPtr gui_manager = new king::gui::GuiManager(&gui_system_interface , &gui_render_interface, new king::game::ContextFactory(glm::ivec2(800, 600), new king::RenderStateFactory(texture_manager), render_queue));

  SDL_Event evt;
  king::event::EventQueue event_queue;
  king::animation::AnimationSystem::SharedPtr animation_system = new king::animation::AnimationSystem();
  king::core::StateContainer state_container(new king::game::StateFactory(render_queue, texture_manager, animation_system, gui_manager));
  state_container.PushState("main_menu_state");
  state_container.Update(.0f);
  
  int old_time = SDL_GetTicks();
  int current_time = 0;
  float delta_time = .0f;
  while (!state_container.IsEmpty() || animation_system->IsAnimationsRunning())
  {
    glClearColor(0.123f, 0.123f, 0.123f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    event_queue.FlushQueue();
    while (SDL_PollEvent(&evt) != 0)
    {
      event_queue.PushEvent(evt);
    }
    
    current_time = SDL_GetTicks();
    delta_time = (current_time - old_time) / 1000.0f;
    old_time = current_time;
    animation_system->Update(delta_time);
    gui_system_interface.IncreaseElapsedTime(delta_time);
    gui_manager->Update(event_queue);
    state_container.ProcessEvents(event_queue);
    state_container.Update(delta_time);
    render_queue->Render();
    SDL_GL_SwapWindow(window);
  }

  SDL_GL_DeleteContext(glContext);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}
