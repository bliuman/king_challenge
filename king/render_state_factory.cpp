//
//  render_state_factory.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#include "king/render_state_factory.h"
#include <assert.h>
#include "king/render_state.h"

namespace king {

RenderStateFactory::RenderStateFactory(const king::TextureManager::SharedPtr& texture_manager)
  :texture_manager_(texture_manager)
  ,function_map_()
{
  assert(!texture_manager_.isNull());
  function_map_.insert(FactoryFunctionMap::value_type("background_default", &RenderStateFactory::BackgroundDefault));
  function_map_.insert(FactoryFunctionMap::value_type("grid_default", &RenderStateFactory::GridDefault));
  function_map_.insert(FactoryFunctionMap::value_type("blue_gem_default", &RenderStateFactory::BlueGemDefault));
  function_map_.insert(FactoryFunctionMap::value_type("green_gem_default", &RenderStateFactory::GreenGemDefault));
  function_map_.insert(FactoryFunctionMap::value_type("purple_gem_default", &RenderStateFactory::PurpleGemDefault));
  function_map_.insert(FactoryFunctionMap::value_type("red_gem_default", &RenderStateFactory::RedGemDefault));
  function_map_.insert(FactoryFunctionMap::value_type("yellow_gem_default", &RenderStateFactory::YellowGemDefault));
  function_map_.insert(FactoryFunctionMap::value_type("blue_gem_select", &RenderStateFactory::BlueGemSelect));
  function_map_.insert(FactoryFunctionMap::value_type("green_gem_select", &RenderStateFactory::GreenGemSelect));
  function_map_.insert(FactoryFunctionMap::value_type("purple_gem_select", &RenderStateFactory::PurpleGemSelect));
  function_map_.insert(FactoryFunctionMap::value_type("red_gem_select", &RenderStateFactory::RedGemSelect));
  function_map_.insert(FactoryFunctionMap::value_type("yellow_gem_select", &RenderStateFactory::YellowGemSelect));
  function_map_.insert(FactoryFunctionMap::value_type("main_menu_context_default", &RenderStateFactory::MainMenuContextDefault));
  function_map_.insert(FactoryFunctionMap::value_type("game_menu_context_default", &RenderStateFactory::GameMenuContextDefault));
  function_map_.insert(FactoryFunctionMap::value_type("game_end_menu_context_default", &RenderStateFactory::GameEndMenuContextDefault));
  function_map_.insert(FactoryFunctionMap::value_type("game_pause_menu_context_default", &RenderStateFactory::GamePauseMenuContextDefault));
}

RenderStateFactory::~RenderStateFactory()
{
}

king::RenderState* RenderStateFactory::Create(const std::string &name)
{
  FactoryFunctionMap::const_iterator iter = function_map_.find(name);
  assert(function_map_.end() != iter);
  FactoryFunction func = iter->second;
  return (this->*func)();
}

king::RenderState* RenderStateFactory::BackgroundDefault()
{
  king::RenderState::TextureList textures;
  textures.push_back(texture_manager_->GetTexture("assets/BackGround.jpg"));
  return new king::RenderState("basic_textured_quad", 10, textures);
}

king::RenderState* RenderStateFactory::GridDefault()
{
  return new king::RenderState();
}

king::RenderState* RenderStateFactory::GemDefault(const std::string& texture_name)
{
  king::RenderState::TextureList textures;
  textures.push_back(texture_manager_->GetTexture(texture_name));
  return new king::RenderState("basic_xyuvrgba_quad", 13, textures);
}

king::RenderState* RenderStateFactory::BlueGemDefault()
{
  return GemDefault("assets/Blue.png");
}

king::RenderState* RenderStateFactory::GreenGemDefault()
{
  return GemDefault("assets/Green.png");
}

king::RenderState* RenderStateFactory::PurpleGemDefault()
{
  return GemDefault("assets/Purple.png");
}

king::RenderState* RenderStateFactory::RedGemDefault()
{
  return GemDefault("assets/Red.png");
}

king::RenderState* RenderStateFactory::YellowGemDefault()
{
  return GemDefault("assets/Yellow.png");
}

king::RenderState* RenderStateFactory::GemSelect(const std::string& texture_name)
{
  king::RenderState::TextureList textures;
  textures.push_back(texture_manager_->GetTexture(texture_name));
  return new king::RenderState("basic_xyuvrgba_quad", 13, textures, glm::ivec4(255, 100, 100, 150));
}

king::RenderState* RenderStateFactory::BlueGemSelect()
{
  return GemSelect("assets/Blue.png");
}

king::RenderState* RenderStateFactory::GreenGemSelect()
{
  return GemSelect("assets/Green.png");
}

king::RenderState* RenderStateFactory::PurpleGemSelect()
{
  return GemSelect("assets/Purple.png");
}

king::RenderState* RenderStateFactory::RedGemSelect()
{
  return GemSelect("assets/Red.png");
}

king::RenderState* RenderStateFactory::YellowGemSelect()
{
  return GemSelect("assets/Yellow.png");
}

king::RenderState* RenderStateFactory::MainMenuContextDefault()
{
  king::RenderState::TextureList textures;
  return new king::RenderState("gui_context", 11, textures);
}

king::RenderState* RenderStateFactory::GameMenuContextDefault()
{
  king::RenderState::TextureList textures;
  return new king::RenderState("gui_context", 15, textures);
}

king::RenderState* RenderStateFactory::GameEndMenuContextDefault()
{
  king::RenderState::TextureList textures;
  return new king::RenderState("gui_context", 16, textures);
}

king::RenderState* RenderStateFactory::GamePauseMenuContextDefault()
{
  king::RenderState::TextureList textures;
  return new king::RenderState("gui_context", 16, textures);
}

} // namespace king
