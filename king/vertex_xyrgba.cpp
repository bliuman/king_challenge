//
//  vertex_xyrgba.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/25/13.
//
//

#include "king/core/frame.h"
#include "king/vertex_xyrgba.h"
#include <assert.h>
#include "king/core/game_object.h"
#include "gpu_program_param_info.h"

namespace king {

void VertexXYRGBA::BindVertexAttributes(const king::GPUProgram::SharedPtr &gpu_program, AttributeList &attribute_list)
{
  const int attrib_count = 2;
  AttribBindInfo attrib_bind_info[attrib_count] = {
    {"vPosition", 2, GL_FLOAT, GL_FALSE, 0},
    {"vColor", 4, GL_UNSIGNED_BYTE, GL_FALSE, (const GLvoid*)(2 * sizeof(GL_FLOAT))}
  };
  
  GLsizei stride = sizeof(VertexXYRGBA);
  const king::GPUProgram::Parameters& attrib_parameters = gpu_program->GetAttributes().GetParameters();
  king::GPUProgram::Parameters::const_iterator iter = attrib_parameters.end();
  for (int i = 0; i < attrib_count; ++i)
  {
    const AttribBindInfo& info = attrib_bind_info[i];
    iter = attrib_parameters.find(info.name_);
    assert(attrib_parameters.end() != iter);
    glVertexAttribPointer(iter->second, info.size_, info.type_, info.normalize_, stride, info.offset_);
    glEnableVertexAttribArray(iter->second);
    attribute_list.push_back(iter->second);
  }
}

void VertexXYRGBA::UnbindVertexAttributes(const AttributeList &attribute_list)
{
  for (AttributeList::const_iterator iter = attribute_list.begin(); attribute_list.end() != iter; ++iter)
  {
    glDisableVertexAttribArray((*iter));
  }
}

int VertexXYRGBA::UpdateData(BufferType &buffer, const king::BaseRenderOperation::RenderList& render_objects)
{
  int vertex_count_ = 0;
  int vertex_per_object = 6;
  if (buffer.size() < render_objects.size() * vertex_per_object)
  {
    buffer.resize(buffer.size() * 2, false);
  }
  
  for (king::BaseRenderOperation::RenderList::const_iterator iter = render_objects.begin(); render_objects.end() != iter; ++iter)
  {
    const king::core::Frame* frame = (*iter)->GetFrame();
    const glm::vec2& position = frame->GetPosition();
    const glm::vec2& dimension = frame->GetDimension();
    
    // top left vertex
    buffer[vertex_count_].position[0] = position.x;
    buffer[vertex_count_].position[1] = position.y;
    buffer[vertex_count_].color[0] = 255;
    buffer[vertex_count_].color[1] = 255;
    buffer[vertex_count_].color[2] = 255;
    buffer[vertex_count_].color[3] = 255;
    
    // bottom left vertex
    buffer[vertex_count_ + 1].position[0] = position.x;
    buffer[vertex_count_ + 1].position[1] = position.y + dimension.y;
    buffer[vertex_count_ + 1].color[0] = 255;
    buffer[vertex_count_ + 1].color[1] = 255;
    buffer[vertex_count_ + 1].color[2] = 255;
    buffer[vertex_count_ + 1].color[3] = 255;
    
    // bottom right vertex
    buffer[vertex_count_ + 2].position[0] = position.x + dimension.x;
    buffer[vertex_count_ + 2].position[1] = position.y + dimension.y;
    buffer[vertex_count_ + 2].color[0] = 255;
    buffer[vertex_count_ + 2].color[1] = 255;
    buffer[vertex_count_ + 2].color[2] = 255;
    buffer[vertex_count_ + 2].color[3] = 255;
    
    // bottom right vertex
    buffer[vertex_count_ + 3].position[0] = position.x + dimension.x;
    buffer[vertex_count_ + 3].position[1] = position.y + dimension.y;
    buffer[vertex_count_ + 3].color[0] = 255;
    buffer[vertex_count_ + 3].color[1] = 255;
    buffer[vertex_count_ + 3].color[2] = 255;
    buffer[vertex_count_ + 3].color[3] = 255;
    
    // top right vertex
    buffer[vertex_count_ + 4].position[0] = position.x + dimension.x;
    buffer[vertex_count_ + 4].position[1] = position.y;
    buffer[vertex_count_ + 4].color[0] = 255;
    buffer[vertex_count_ + 4].color[1] = 255;
    buffer[vertex_count_ + 4].color[2] = 255;
    buffer[vertex_count_ + 4].color[3] = 255;
    
    // top left vertex
    buffer[vertex_count_ + 5].position[0] = position.x;
    buffer[vertex_count_ + 5].position[1] = position.y;
    buffer[vertex_count_ + 5].color[0] = 255;
    buffer[vertex_count_ + 5].color[1] = 255;
    buffer[vertex_count_ + 5].color[2] = 255;
    buffer[vertex_count_ + 5].color[3] = 255;
    
    vertex_count_ += vertex_per_object;
  }
  return vertex_count_;
}

} // namespace king
