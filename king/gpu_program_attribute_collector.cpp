//
//  gpu_program_attribute_collector.cpp
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/24/13.
//
//

#include "king/gpu_program_attribute_collector.h"
#include <Poco/Buffer.h>

namespace king {

GPUProgramAttributeCollector::GPUProgramAttributeCollector()
{
}

GPUProgramAttributeCollector::~GPUProgramAttributeCollector()
{
}

void GPUProgramAttributeCollector::GetTypes(GLenum& parameter, GLenum& buffer_length) const
{
  parameter = GL_ACTIVE_ATTRIBUTES;
  buffer_length = GL_ACTIVE_ATTRIBUTE_MAX_LENGTH;
}

GLint GPUProgramAttributeCollector::Collect(GLuint gpu_prgram_id_, GLint index, GLsizei buffer_size, GLint* size, GLenum* type, Poco::Buffer<char>& name_buffer) const
{
  glGetActiveAttrib(gpu_prgram_id_, index, buffer_size, static_cast<GLsizei*>(0), size, type, name_buffer.begin());
  return glGetAttribLocation(gpu_prgram_id_, name_buffer.begin());
}

} // namespace king
