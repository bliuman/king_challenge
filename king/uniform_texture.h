//
//  uniform_texture.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/27/13.
//
//

#ifndef KING_UNIFORM_TEXTURE_H
#define KING_UNIFORM_TEXTURE_H

#include "king/opengl.h"
#include "king/uniform_parameter.h"

namespace king {

class UniformTexture: public king::UniformParameter<GLuint> {
  public:
    UniformTexture(GLuint data);
    virtual ~UniformTexture();
    virtual void Bind(GLuint location) const;
    
};

} // namespace king

#endif  // KING_UNIFORM_TEXTURE_H
