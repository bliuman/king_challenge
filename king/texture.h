//
//  texture.h
//  king_challenge
//
//  Created by Mantas Bliudzius on 10/23/13.
//
//

#ifndef KING_TEXTURE_UNIT_H
#define KING_TEXTURE_UNIT_H

#include <Poco/SharedPtr.h>
#include <glm/glm.hpp>
#include "king/texture_unit.h"

namespace king {

class Texture {
  public:
    typedef Poco::SharedPtr<Texture> SharedPtr;
  
  public:
    Texture(const king::TextureUnit::SharedPtr& texture_unit);
    Texture(const king::TextureUnit::SharedPtr& texture_unit, const glm::vec2& min, const glm::vec2& max);
    ~Texture();
    const glm::vec2& GetMinUV() const {return uv_min_;}
    const glm::vec2& GetMaxUV() const {return uv_max_;}
    const king::TextureUnit::SharedPtr& GetTextureUnit() const {return texture_unit_;}
  
  private:
    void CalculateUV(const glm::vec2& min, const glm::vec2& max);
  
    king::TextureUnit::SharedPtr texture_unit_;
    glm::vec2 uv_min_;
    glm::vec2 uv_max_;
    
};

} // namespace king

#endif  // KING_TEXTURE_UNIT_H
