set(png_export_include_dirs ${png_SOURCE_DIR}/include)
set(png_export_library_dirs "")
set(png_export_definitions "")
if (TARGET_PLATFORM_MACOSX)
	set(png_export_link_targets debug ${png_SOURCE_DIR}/lib/macosx_x64/Debug/libpng16d.a optimized ${png_SOURCE_DIR}/lib/macosx_x64/Release/libpng16.a)
elseif (TARGET_PLATFORM_WINDOWS)
	set(png_export_link_targets debug ${png_SOURCE_DIR}/lib/win_x86/Debug/libpng16_staticd.lib optimized ${png_SOURCE_DIR}/lib/win_x86/Release/libpng16_static.lib)
endif (TARGET_PLATFORM_MACOSX)