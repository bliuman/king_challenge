set(freetype_export_include_dirs ${freetype_SOURCE_DIR}/include)
set(freetype_export_library_dirs "")
set(freetype_export_definitions "")
if (TARGET_PLATFORM_MACOSX)
	set(freetype_export_link_targets debug ${freetype_SOURCE_DIR}/lib/macosx_x64/Debug/libfreetype.a optimized ${freetype_SOURCE_DIR}/lib/macosx_x64/Release/libfreetype.a)
elseif (TARGET_PLATFORM_WINDOWS)
	set(freetype_export_link_targets debug ${freetype_SOURCE_DIR}/lib/win_x86/Debug/freetype250_D.lib optimized ${freetype_SOURCE_DIR}/lib/win_x86/Release/freetype250.lib)
endif (TARGET_PLATFORM_MACOSX)